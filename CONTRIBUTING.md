# Contributing

When contributing to this repository, please first annotate the change you wish 
to make via issue before making a change. 

## Merge Request Process

1. Create an issue ticket of the change. Create a specific branch for your
   solution that includes the ticket number in the name. Use the [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) 
   workflow guidelines:
2. Update the README.md with details of changes to the interface, this includes 
   new environment variables, exposed ports, useful file locations and container
   parameters.
3. Document the added or modified functions and methods with docstrings, and 
   update the generated documentation.
4. Increase the version numbers. The versioning scheme we use is
   [SemVer](http://semver.org/).
5. Run all the tests.
