# RISE Project - SLiM from Structure

## Version 0.0.1

## Table of Contents

- [RISE Project - SLiM from Structure](#rise-project---slim-from-structure)
    - [Table of Contents](#table-of-contents)
    - [SLiM From Structure](#slim-from-structure)
        - [About this project](#about-this-project)
    - [Getting Started](#getting-started)
        - [Prerequisites](#prerequisites)
            - [Prerequisites for the virtual enviroment on a linux OS](
               #prerequisites-for-the-virtual-enviroment-on-a-linux-os)
            - [Prerequisites for docker](#prerequisites-for-docker)
        - [Installing](#installing)
        - [Set up a virtual enviroment](#set-up-a-virtual-enviroment)
        - [Running](#running)
            - [How do I run it locally?](#how-do-i-run-it-locally)
                - [Run the docker image](#run-the-docker-image)
                - [Running as a REST server](#running-as-a-rest-server)
            - [Examples](#examples)
            - [How do I run it online?](#how-do-i-run-it-online)
            - [What are the input and outputs?](#what-are-the-input-and-outputs)
            - [Running the tests](#running-the-tests)
    - [Benchmark for ELM database](#benchmark-for-elm-database)
        - [Benchmark specifictions](#benchmark-specifictions)
        - [Running the benchmark](#running-the-benchmark)
        - [Benchmark analysis results](#benchmark-analysis-results)
        - [Benchmark summary](#benchmark-summary)
    - [Documentation](#documentation)
    - [License](#license)
    - [Contributors](#contributors)
        - [Who worked on this project?](#who-worked-on-this-project)
    - [Acknowledgments](#acknowledgments)

## SLiM From Structure 

### About this project

SLiM from structures objective is to search for IDR mediated interactions 
analyzing PDB files.

## Getting Started

### Prerequisites

There is two ways to run this code:

- In a python virtual enviroment on a linux based OS.
- As a docker container.

Each ways has a different set of prerequisites.

#### Prerequisites for the virtual enviroment on a linux OS

- Python 2.7.x
- Linux OS

#### Prerequisites for docker

- Docker, for linux and mac.
- Docker Tools for windows.

### Installing

The code is available as a docker image and can be run as described below.
The code is in python so it's possible to run it locally without docker too if 
you set the paths correctly in the _config.ini_ file.

1. First grab the repository:  

```bash
    $ git clone https@gitlab.com/idpfun/rise-find-interfaces.git
```

2. Enter the repository directory:  

```bash
    $ cd rise-find-interfaces/
```

3. Build the docker image:  
   Make sure you have ftp access.

```bash
    $ docker build -t rise-find-interfaces ./docker_file
```

### Set up a virtual enviroment

For develpment, a virtual enviroment can be used instead of running a docker
image:

```bash
    # Install python virtualenv
    $ pip install virtualenv
    # Create the virtual enviroment
    $ python -m virtualenv venv
    # Enter to the virtual enviroment
    $ source venv/bin/activate 
    # or 
    $ source venv/Scripts/activate # on windows
    # Install dependencies
    (venv) $ pip install -r requeriments.txt
```

After this, _PYTHONPATH_ enviromental variable should be set to 'src/scripts'
full path. 

The configuration file (_config.ini_) contains the path to the data folder and
to the dssp executables as full paths inside the docker image. Therefore, these
paths must be changed. Use always full paths.

To run the Flask development server, _FLASK\_APP_ and  _FLASK\_ENV_ enviromental
variables should be set.

- **FLASK\_APP**: apps:create_app
- **FLASK\_ENV**: development

Then, just run:

```bash
   flask run --port 80 --host 127.0.0.1
```

### Running

#### How do I run it locally?

##### Run the docker image

Run the docker image from host:

```bash
    # {rise-find-interfaces-folder} is the full path to the project's root 
    # folder
    $ docker run -t -i \
    -v {rise-find-interfaces-folder}/src/scripts/:/home/scripts/ \
    -v {rise-find-interfaces-folder}/data/:/home/data/ \
    rise-find-interfaces /bin/bash
```

There are two shared folder with the host:

- **script**: contains the source code. It is kept outside the image to make
  development easier.
- **data**: contains downloaded data outside the docker image itself for
  persistence.

Running the container should result in a bash prompt from an ubuntu image.
There are three scripts that can be used to run, attach and stop a docker 
container.
These scripts assumes that:

- There is only one running container for this project.
- The _scripts_ and _data_ folders are in the current working directory
  or that the _RISE\_PATH_ enviromental variable is set to the directory
  containg those folders.

```bash 
    # Starts a detached container.
    source run_container.sh
    # Attach a console to a running container.
    source attach_container.sh
    # Stops the container
    source stop_container.sh
```

##### Running as a REST server

The docker image can run as a web server itself:

```bash
    # Running the development server.
    # The server run with flask framework.
    $ source run_development_server.sh
    # Running the production server.
    # The server run with flask+uwsgi+nginx frameworks.
    $ source run_production_server.sh
```

By default the server binds to the host port 80. The port can be changed to a
different one. If host is a Windows OS and it is running Docker Toolbox, then
the ports intended to be used should be configured in the virtual-box settings.
Go to "Settings" -> "Network", choose th NAT adapter a then "Advanced", finally
open the "Port forwarding" options. Add a new rule, and set the guest and host
port to the desired port (the same in both places), leave IP options blank,
and set protocol to TCP. Here, host is the Windows OS, and guest is the virtual
machine, not the docker container that is running.

```bash
    # Running the development server.
    # The server run with flask framework accessible at port 8080 on the host.
    $ source run_development_server.sh -p 8080
```

To stop the server just run:

```bash
    $ source stop_container.sh
```

To test the server, just go to localhost on your browser, a simple help page
should appear.
You can also use curl or a similar tool:
```bash 
    # Run find interfaces for PDB 2AST.
    $ curl localhost/pdb/2ast
```

#### Examples

```bash 
    $ python /home/scripts/structure/interface_finder.py \
    --pdb_id 2AST --remake True --debug True --outfmt tdt
```

#### How do I run it online?

We hope to run a [REST](https://disorder.ucd.ie/webservices/ 
"Visit the REST server!") server.

#### What are the input and outputs?

- **Input**: A *PDB identifier*: PDB idenfier of the structure to analyze.

- **Outputs**:
  - Output options:
    - *Full JSON* Detailed information of motifs, proteins, etc.
    - *JSON*: Detailed motif interface information
    - *Small JSON*: Simple motif interface information
    - *TDT*: Simple motif interface information

#### Running the tests

To run the test suite, run the docker image and execute the these commands:

```bash 
    $ cd /home/scripts
    $ pytest tests/
```

## Benchmark for ELM database

### Benchmark specifictions

1. Iterate through each ELM instance structure
2. Find motifs and get
    1. Motif-containing protein UniProt accession
    2. Motif start
    3. Motif end
    4. Motif-binding protein UniProt accession
    5. Motif-binding domain Pfam accession
3. Compare structure parsed data with the ELM data
4. If it doesn't match find out why and fix
5. Keep a list of problem ids or ELM curation mistakes
6. Calculate some basic statistics

### Running the benchmark

Currently, there is one benchmark. It consist of running find_interfaces script
with all PDB with one or more annotated ELM instances to check if those 
instances are recovered.
How to run the benchmarks.
```bash
    cd /home/scripts
    pytest benchmarks/test_elm_data.py::test_benchmark_elm
    # Run find_interfaces script for all PDB on elm.
    # Takes many hours to run.
    # Results of this benchmark are precomputed and stored in 
    # src/scripts/benchmarks/results/

    pytest benchmarks/test_elm_data.py::test_benchmark_elm_verification
    # Makes the analysis the previous results.
```

### Benchmark analysis results 

The results of the analysis are in several files in the
_src/scripts/benchmarks/_ folder.

- **errors.txt**  PDB with known errors.  
- **no_results.txt**  PDB with no results.
- **no_accession.txt**  Motifs found that lack uniprot accessions for key chains
and therefore can not be directly mapped to an ELM instance.
- **unmatched.txt**  Motifs whose uniprot accession do not match with ELM.
- **similars.txt**  Motifs found that matches or closely resembles an ELM 
instance.
- **strange.txt**  Motifs with results that can not be mapped to an ELM 
instance. Requires further analysis of each individual case.
- **strange.annotated.txt**  Review of each individual case in strange.txt
- **summary_pdb.txt**  Summary of the results found for each PDB.


### Benchmark summary
A collection of 448 PDB containing Elm instances was constructed querying ELM.
Also, another larger collection was constructed consisintg of 762 PDB containing
all previous ELM pdbs and more (listed in _elm_pdb.txt_).
All both collections were run and finished properly without breaking.

Futher analysis from the results of the small collection gives:

*  39 of them returned an error status:
    * 3 Entry is marked as obsolete.
    * 13 Multimer but only one entity in structure [skip_homomultimers set].
    * 23 Only one entity in structure. Not a complex.
* 20 of them contains no motif results. In all cases, chains have a high number
  of intramolecular contacts.
* The motif found for the remaining PDBs were extracted.
    * In total there are 582 (Motif, Elm instance) pairs to compare (Most of 
      them are 1 to 1).
    * 61 Corresponds to Motifs with an unmatched uniprot acc in ELM.
    * 510 Corresponds to Motifs that are similar to and ELM instance.
    * 11 Corresponds to Motifs that are not similar to ELM.
    * Motif region do not overlaps with Elm instance.

## Documentation

Documentation is generated automatically with pydoc.
It's found in _/home/scripts/docs_. 

## License

This project source code is licensed under the MIT License - see the 
[LICENSE.md](LICENSE.md) file for details.

## Contributors

### Who worked on this project?

| Name          | Email                 | Role           |
| ------------- | --------------------- | -------------- |
| Norman Davey  | normandavey@gmail.com | Lead developer |
| Javier Iserte | jiserte@leloir.org.ar | Developer      |

## Acknowledgments

This software was produced as part of the 
**IDPfun, Research and Innovation Staff Exchange project** 
([IDPfun](http://idpfun.eu/)) 
funded by **Marie Skłodowska-Curie - Horizon 2020 programme**
of the **European Commission**.