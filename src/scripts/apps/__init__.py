import os
from os.path import dirname
from os.path import join
from flask import Flask
from flask import request
from flask import send_from_directory
from flask import Response
import json
import optparse
from structure.custom_json_encoder import CjsonEncoder
from structure.find_interfaces import findInterfaces

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='MSNVRVSNGSPSLERMDARQAEHPKPSACRNLFGPVDHEEL',
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route('/')
    def main_slash():
        return send_from_directory(dirname(__file__), 'index.html')

    @app.route('/pdb/<pdb_id>')
    def pdb(pdb_id):
        findInterfacesObj = findInterfaces()
        findInterfacesObj.options.update({
            'skip_large_multimers': True,
            'url_root': 'http://localhost:6543',
            'coiled_coil_helix_helix_contact_proportion_cutoff': 0.75,
            'intramolecular_cutoff': 10.0,
            'remake_age': 180.0,
            'disorder_domain_length': 15.0,
            'max_peptide_length': 25.0,
            'pdb_id': pdb_id,
            'max_intramolecular_interactions': 10.0,
            'chain_id': '',
            'wait_time': 0.1,
            'peptide_length': 30.0,
            'inter_chain_distance': 8,
            'surface_accessibility_cutoff': 0.35,
            'domain_contacts_cutoff': 3.0,
            'task': 'find_motifs',
            'skip_homomultimers': True,
            'sidechain_interactions_only': 'SideToAny',
            'remake': False,
            'debug': False,
            'outfmt': 'tdt',
            'coiled_coil_helix_helix_contact_length_cutoff': 7.0,
            'user_email': 'normandavey@gmail.com'})

        motifs = findInterfacesObj.find_motifs()
        if motifs["status"] == "Error":
            return Response(json.dumps(motifs, cls=CjsonEncoder, indent=4, 
                sort_keys=True), mimetype='application/json')
        else:
            outfmt = request.args.get("outfmt")
            outfmt = outfmt if outfmt in ["full_json", "json", "small_json"] else "json"
            motif_details = motifs['data']['motif_details']            
            out = ""
            if outfmt == "full_json":
                out = {k: motifs[k] for k in motifs}
            elif outfmt == "json":
                out = {chain: motif_details[chain] for chain in motif_details}
            elif outfmt == "small_json":
                headers = ['chain', 'accession', 'start', 'end', 'gene_name', 
                           "protein_name", 'module_name', u'module_type', 
                           'fragment_description', 'polymer_description', 
                           "sequence", 'contact_residues', "ss", 'ss_type']
                out = {chain:{h: motif_details[chain][h] for h in headers[1:]} 
                       for chain in motif_details}
            return Response(json.dumps(out, cls=CjsonEncoder, indent=4, 
                sort_keys=True), mimetype='application/json')
    return app

