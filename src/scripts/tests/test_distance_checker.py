import pytest
from Bio.PDB.Atom import Atom
from numpy import array

from structure.pdb_utils.distance_checkers import NitrogenAlphaDistanceLessThan

@pytest.fixture(scope="module")
def resA():
    return {"N":Atom("", array([0, 0, 1]), 0, 0, 0, "", 1, element="N")}

@pytest.fixture(scope="module")
def resB():
    return {"N":Atom("", array([0, 0, 10]), 0, 0, 0, "", 1, element="N")}

@pytest.fixture(scope="module")
def resC():
    return {"N":Atom("", array([5, 5, 5]), 0, 0, 0, "", 1, element="N")}

@pytest.fixture(scope="module")
def resD():
    return {"N":Atom("", array([6, 6, 6]), 0, 0, 0, "", 1, element="N")}

@pytest.fixture(scope="module")
def resE():
    return {"N":Atom("", array([1, 1, 1]), 0, 0, 0, "", 1, element="N")}

@pytest.fixture(scope="module")
def resF():
    return {"C":Atom("", array([1, 1, 1]), 0, 0, 0, "", 1, element="N")}

def test_NitrogenAlphaDistanceLessThan_distance(resA, resB):
    dc = NitrogenAlphaDistanceLessThan(10)
    assert dc.distance(resA, resB) == 9
    assert not dc.distance(resA, resB) == 10

def test_NitrogenAlphaDistanceLessThan_in_contacts(resC, resD, resE):
    dc = NitrogenAlphaDistanceLessThan(10)
    assert dc.in_contact(resC, resD)
    assert dc.in_contact(resC, resE)

def test_NitrogenAlphaDistanceLessThan_in_contacts_atom_without_nitrogen(resC, resF):
    dc = NitrogenAlphaDistanceLessThan(10)
    assert not dc.in_contact(resC, resF)

def test_NitrogenAlphaDistanceLessThan_distance_atom_without_nitrogen(resC, resF):
    dc = NitrogenAlphaDistanceLessThan(10)
    assert dc.distance(resC, resF) is None
