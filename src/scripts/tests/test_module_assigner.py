import pytest
from structure.module_assigner import ModuleAssigner
from structure.dssp.secondary_structure import SecondaryStructureRegion
from structure.dssp.dssp_data import DsspPdbChain, DsspDataPdb

@pytest.fixture(scope='module')
def chain_pair_01():
    """
    Simple coiled coiled chains:

    sequence    A:  11  KKK        13
    sec. struc. A:  11  HHH        13
    contacts            |||
    sequence    B: 100 -KKK------ 110
    sec. struc. B: 100 HHHHHHHHHH 110

    """
    contacts = {
        'PDB1.A':{'PDB1.B': {11: {101: "dummy_contact"},
                             12: {102: "dummy_contact"},
                             13: {103: "dummy_contact"}}},
        'PDB1.B':{'PDB1.A': {101: {11: "dummy_contact"},
                             102: {12: "dummy_contact"},
                             103: {13: "dummy_contact"}}},
    }

    helices = {
        'PDB1.A': [SecondaryStructureRegion('H', 11, 13)],
        'PDB1.B': [SecondaryStructureRegion('H', 100, 110)]
    }

    dssp = DsspDataPdb()

    dssp_chain_a = DsspPdbChain('PDB1.A')
    for i in range(11, 13+1):
        dssp_chain_a.add_residue(i, {'SecondaryStructure':'H'})

    dssp.add_chain('PDB1.A', dssp_chain_a)

    dssp_chain_b = DsspPdbChain('PDB1.B')
    for i, s in zip(range(100, 110+1), "HHHHHHHHHHH"):
        dssp_chain_b.add_residue(i, {'SecondaryStructure': s})

    dssp.add_chain('PDB1.B', dssp_chain_b)

    interfaces = {'PDB1.A': {'PDB1.B':{'interactor_residues_pdb':[101, 102, 103]}},
                  'PDB1.B': {'PDB1.A':{'interactor_residues_pdb':[11, 12, 13]}}}

    return contacts, helices, dssp, interfaces

@pytest.fixture(scope='module')
def chain_pair_02():
    """
    Simple coiled coiled chains:

    sequence    A:  11  KKKP       14
    sec. struc. A:  11  HHHT       14
    contacts            ||||
    sequence    B: 100 -KKKK----- 110
    sec. struc. B: 100 HHHHHHHHHH 110

    """
    contacts = {
        'PDB1.A':{'PDB1.B': {11: {101: "dummy_contact"},
                             12: {102: "dummy_contact"},
                             13: {103: "dummy_contact"},
                             14: {104: "dummy_contact"}}},
        'PDB1.B':{'PDB1.A': {101: {11: "dummy_contact"},
                             102: {12: "dummy_contact"},
                             103: {13: "dummy_contact"},
                             104: {14: "dummy_contact"}}},
    }

    helices = {
        'PDB1.A': [SecondaryStructureRegion('H', 11, 13)],
        'PDB1.B': [SecondaryStructureRegion('H', 100, 110)]
    }

    dssp = DsspDataPdb()

    dssp_chain_a = DsspPdbChain('PDB1.A')
    for i in range(11, 13+1):
        dssp_chain_a.add_residue(i, {'SecondaryStructure':'H'})
    dssp_chain_a.add_residue(14, {'SecondaryStructure':'T'})

    dssp.add_chain('PDB1.A', dssp_chain_a)

    dssp_chain_b = DsspPdbChain('PDB1.B')
    for i, s in zip(range(100, 110+1), "HHHHHHHHHHH"):
        dssp_chain_b.add_residue(i, {'SecondaryStructure': s})

    dssp.add_chain('PDB1.B', dssp_chain_b)

    interfaces = {'PDB1.A': {'PDB1.B':{'interactor_residues_pdb':[101, 102, 103, 104]}},
                  'PDB1.B': {'PDB1.A':{'interactor_residues_pdb':[11, 12, 13, 14]}}}

    return contacts, helices, dssp, interfaces

@pytest.fixture(scope='module')
def chain_pair_03():
    """
    Simple coiled coiled chains:

    sequence    A:  11  KKKP       14
    sec. struc. A:  11  HHH        14
    contacts            ||||
    sequence    B: 100 -KKKK----- 110
    sec. struc. B: 100 HHHHHHHHHH 110

    """
    contacts = {
        'PDB1.A':{'PDB1.B': {11: {101: "dummy_contact"},
                             12: {102: "dummy_contact"},
                             13: {103: "dummy_contact"},
                             14: {104: "dummy_contact"}}},
        'PDB1.B':{'PDB1.A': {101: {11: "dummy_contact"},
                             102: {12: "dummy_contact"},
                             103: {13: "dummy_contact"},
                             104: {14: "dummy_contact"}}},
    }

    helices = {
        'PDB1.A': [SecondaryStructureRegion('H', 11, 13)],
        'PDB1.B': [SecondaryStructureRegion('H', 100, 110)]
    }

    dssp = DsspDataPdb()

    dssp_chain_a = DsspPdbChain('PDB1.A')
    for i in range(11, 13+1):
        dssp_chain_a.add_residue(i, {'SecondaryStructure':'H'})
    dssp_chain_a.add_residue(14, {'SecondaryStructure':''})

    dssp.add_chain('PDB1.A', dssp_chain_a)

    dssp_chain_b = DsspPdbChain('PDB1.B')
    for i, s in zip(range(100, 110+1), "HHHHHHHHHHH"):
        dssp_chain_b.add_residue(i, {'SecondaryStructure': s})

    dssp.add_chain('PDB1.B', dssp_chain_b)

    interfaces = {'PDB1.A': {'PDB1.B':{'interactor_residues_pdb':[101, 102, 103, 104]}},
                  'PDB1.B': {'PDB1.A':{'interactor_residues_pdb':[11, 12, 13, 14]}}}

    return contacts, helices, dssp, interfaces

@pytest.fixture(scope='module')
def chain_pair_04():
    """
    For testing module assignment .

    sequence    A:  11  KKK        13
    sec. struc. A:  11  HHH        13
    sequence    B: 100 -KKK------ 110
    sec. struc. B: 100 HHHHHHHHHH 110
    sequence    C: 200 -KKK------ 210
    sec. struc. C: 200 HHHHHHHHHH 210
    sequence    D: 300 -KKK------ 310
    sec. struc. D: 300 HHHHHHHHHH 310
    sequence    D: 300 -KKK------ 310
    sec. struc. D: 300 HHHHHHHHHH 310
    """
    contacts = {}

    helices = {
        'PDB1.A': [SecondaryStructureRegion('H', 11, 13)],
        'PDB1.B': [SecondaryStructureRegion('H', 100, 110)],
        'PDB1.C': [SecondaryStructureRegion('H', 200, 210)],
        'PDB1.D': [SecondaryStructureRegion('H', 300, 310)],
        'PDB1.E': [SecondaryStructureRegion('H', 400, 410)]
    }

    dssp = DsspDataPdb()

    dssp_chain_a = DsspPdbChain('PDB1.A')
    for i in range(11, 13+1):
        dssp_chain_a.add_residue(i, {'SecondaryStructure':'H'})

    dssp.add_chain('PDB1.A', dssp_chain_a)

    dssp_chain_b = DsspPdbChain('PDB1.B')
    for i, s in zip(range(100, 110+1), "HHHHHHHHHHH"):
        dssp_chain_b.add_residue(i, {'SecondaryStructure': s})

    dssp.add_chain('PDB1.B', dssp_chain_b)

    dssp_chain_c = DsspPdbChain('PDB1.C')
    for i, s in zip(range(200, 210+1), "HHHHHHHHHHH"):
        dssp_chain_c.add_residue(i, {'SecondaryStructure': s})

    dssp.add_chain('PDB1.C', dssp_chain_c)

    dssp_chain_d = DsspPdbChain('PDB1.D')
    for i, s in zip(range(300, 310+1), "HHHHHHHHHHH"):
        dssp_chain_d.add_residue(i, {'SecondaryStructure': s})

    dssp.add_chain('PDB1.D', dssp_chain_d)

    dssp_chain_e = DsspPdbChain('PDB1.E')
    for i, s in zip(range(400, 410+1), "HHHHHHHHHHH"):
        dssp_chain_e.add_residue(i, {'SecondaryStructure': s})

    dssp.add_chain('PDB1.E', dssp_chain_e)

    interface_positions = [300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310]
    interfaces = {'PDB1.A': {'PDB1.D':{'interactor_residues_pdb': interface_positions}}}

    return contacts, helices, dssp, interfaces

def test_module_assigner_01_coiled_coil(chain_pair_01):

    contacts, helices, dssp, interfaces = chain_pair_01

    ma = ModuleAssigner()
    ma.set_intramolecular_cutoff(10)
    ma.set_disorder_domain_length(10)
    ma.set_surface_accessibility_cutoff(5)
    ma.set_antigen_presenting([])
    ma.set_antigen_binding([])
    ma.set_coiled_coil_length_cutoff(2)
    ma.set_coiled_coil_proportion_cutoff(0.8)
    ma.set_contacts(contacts)
    ma.set_helices(helices)
    ma.set_dssp(dssp)

    ma.add_chain_data('PDB1.A', 'protein', 15, 1, interfaces, "Extended", [])
    ma.add_chain_data('PDB1.B', 'protein', 14, 1, interfaces, "Extended", [])

    types = ma.get_types()

    assert types['PDB1.A'] == ModuleAssigner.MODULE_TYPE_COILED_COIL
    assert types['PDB1.B'] == ModuleAssigner.MODULE_TYPE_COILED_COIL

def test_module_assigner_01_coiled_coil_high_length_cutoff(chain_pair_01):

    contacts, helices, dssp, interfaces = chain_pair_01

    ma = ModuleAssigner()
    ma.set_intramolecular_cutoff(10)
    ma.set_disorder_domain_length(10)
    ma.set_surface_accessibility_cutoff(5)
    ma.set_antigen_presenting([])
    ma.set_antigen_binding([])
    ma.set_coiled_coil_proportion_cutoff(0.8)
    ma.set_contacts(contacts)
    ma.set_helices(helices)
    ma.set_dssp(dssp)

    # helix region should be larger than the cutoff (equal is excluded)
    ma.set_coiled_coil_length_cutoff(3)

    ma.add_chain_data('PDB1.A', 'protein', 15, 1, interfaces, "Extended", [])
    ma.add_chain_data('PDB1.B', 'protein', 14, 1, interfaces, "Extended", [])

    types = ma.get_types()

    assert not types['PDB1.A'] == ModuleAssigner.MODULE_TYPE_COILED_COIL
    assert not types['PDB1.B'] == ModuleAssigner.MODULE_TYPE_COILED_COIL

def test_module_assigner_02_coiled_coil(chain_pair_02):
    contacts, helices, dssp, interfaces = chain_pair_02

    ma = ModuleAssigner()
    ma.set_intramolecular_cutoff(10)
    ma.set_disorder_domain_length(10)
    ma.set_surface_accessibility_cutoff(5)
    ma.set_antigen_presenting([])
    ma.set_antigen_binding([])
    ma.set_coiled_coil_proportion_cutoff(0.8)
    ma.set_contacts(contacts)
    ma.set_helices(helices)
    ma.set_dssp(dssp)
    ma.set_coiled_coil_length_cutoff(2)

    ma.add_chain_data('PDB1.A', 'protein', 15, 1, interfaces, "Extended", [])
    ma.add_chain_data('PDB1.B', 'protein', 14, 1, interfaces, "Extended", [])

    types = ma.get_types()

    assert types['PDB1.A'] == ModuleAssigner.MODULE_TYPE_COILED_COIL
    assert not types['PDB1.B'] == ModuleAssigner.MODULE_TYPE_COILED_COIL

def test_module_assigner_02_coiled_coil_lower_proportion(chain_pair_02):
    contacts, helices, dssp, interfaces = chain_pair_02

    ma = ModuleAssigner()
    ma.set_intramolecular_cutoff(10)
    ma.set_disorder_domain_length(10)
    ma.set_surface_accessibility_cutoff(5)
    ma.set_antigen_presenting([])
    ma.set_antigen_binding([])
    ma.set_contacts(contacts)
    ma.set_helices(helices)
    ma.set_dssp(dssp)
    ma.set_coiled_coil_length_cutoff(2)

    # A helix region should have a minumum proportion of helix contacts
    ma.set_coiled_coil_proportion_cutoff(0.7)

    ma.add_chain_data('PDB1.A', 'protein', 15, 1, interfaces, "Extended", [])
    ma.add_chain_data('PDB1.B', 'protein', 14, 1, interfaces, "Extended", [])

    types = ma.get_types()

    assert types['PDB1.A'] == ModuleAssigner.MODULE_TYPE_COILED_COIL
    assert types['PDB1.B'] == ModuleAssigner.MODULE_TYPE_COILED_COIL


def test_module_assigner_03_coiled_coil(chain_pair_03):
    contacts, helices, dssp, interfaces = chain_pair_03

    ma = ModuleAssigner()
    ma.set_intramolecular_cutoff(10)
    ma.set_disorder_domain_length(10)
    ma.set_surface_accessibility_cutoff(5)
    ma.set_antigen_presenting([])
    ma.set_antigen_binding([])
    ma.set_contacts(contacts)
    ma.set_helices(helices)
    ma.set_dssp(dssp)
    ma.set_coiled_coil_length_cutoff(2)

    # Residue with no secondary structure does not count when calculating
    # the proportion of helix contacts
    ma.set_coiled_coil_proportion_cutoff(0.8)

    ma.add_chain_data('PDB1.A', 'protein', 15, 1, interfaces, "Extended", [])
    ma.add_chain_data('PDB1.B', 'protein', 14, 1, interfaces, "Extended", [])

    types = ma.get_types()

    assert types['PDB1.A'] == ModuleAssigner.MODULE_TYPE_COILED_COIL
    assert types['PDB1.B'] == ModuleAssigner.MODULE_TYPE_COILED_COIL

def test_module_assigner_04(chain_pair_04):
    contacts, helices, dssp, interfaces = chain_pair_04

    ma = ModuleAssigner()
    ma.set_intramolecular_cutoff(10)
    ma.set_disorder_domain_length(7)
    ma.set_surface_accessibility_cutoff(5)
    ma.set_antigen_presenting(['PFX0001'])
    ma.set_antigen_binding([])
    ma.set_contacts(contacts)
    ma.set_helices(helices)
    ma.set_dssp(dssp)
    ma.set_coiled_coil_length_cutoff(2)

    # Residue with no secondary structure does not count when calculating
    # the proportion of helix contacts
    ma.set_coiled_coil_proportion_cutoff(0.8)

    # Chain A has high solvent accesibility and high intramolecular contacts
    ma.add_chain_data('PDB1.A', 'protein', 15, 20, interfaces, "Extended", [])

    # Chain B has high solvent accesibility and low intramolecular contacts
    # and small interface length
    ma.add_chain_data('PDB1.B', 'protein', 14, 1, interfaces, "Extended", [])

    # Chain C has low solvent accesibility and low intramolecular contacts
    ma.add_chain_data('PDB1.C', 'protein', 1, 1, interfaces, "Extended", [])

    # Chain D has high solvent accesibility and low intramolecular contacts
    # and large interface length
    ma.add_chain_data('PDB1.D', 'protein', 20, 1, interfaces, "Extended", [])

    # Chain D is an known antigen pfam domain
    ma.add_chain_data('PDB1.E', 'protein', 20, 1, interfaces, "Extended", ['PFX0001'])

    types = ma.get_types()
    assert types['PDB1.A'] == ModuleAssigner.MODULE_TYPE_GLOBULAR
    assert types['PDB1.B'] == ModuleAssigner.MODULE_TYPE_SLIM
    assert types['PDB1.C'] == ModuleAssigner.MODULE_TYPE_UNCERTAIN
    assert types['PDB1.D'] == ModuleAssigner.MODULE_TYPE_DISORDERED_DOMAIN
    assert types['PDB1.E'] == ModuleAssigner.MODULE_TYPE_ANTIGEN
