import json
import inspect
from os.path import join
from os.path import dirname
import pytest
import Bio.PDB

from structure.pdb_utils.construct_sequence_builder  import  PDBSeqBuilder

@pytest.fixture(scope="module")
def sequences_2AST_static(test_data_folder):
    seq_file = join(test_data_folder, "pdb", "2AST.seq.json")
    return json.loads(open(seq_file, "r").read())

@pytest.fixture(scope="module")
def structure_2AST_static(test_data_folder):
    structure_pdb = join(test_data_folder, "pdb", "2AST.static.pdb")
    struct = None
    struct = Bio.PDB.PDBParser().get_structure("2AST", structure_pdb)
    return struct

@pytest.fixture(scope="module")
def structure_XXX1(test_data_folder):
    structure_pdb = join(test_data_folder, "pdb", "XXX1.pdb")
    return Bio.PDB.PDBParser().get_structure("XXX1", structure_pdb)

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
def test_PDBSeqBuilder_2AST(structure_2AST_static, sequences_2AST_static):
    builder = PDBSeqBuilder()
    data = builder.build_all_chains(structure_2AST_static, "2AST")
    chain_data_keys = ['modifications', 'construct_sequences',
                       'construct_sequences_offsets', 'construct_pdb_offsets',
                       'construct_pdb_ranges']
    # each element of data should have four keys
    assert all([len(data[k]) == 4 for k in chain_data_keys])
    # each element keys should be "2AST.A", "2AST.B", "2AST.C" and "2AST.D"
    assert all([all([ck in data[k] for ck in ["2AST.A", "2AST.B", "2AST.C", "2AST.D"]])
                for k in chain_data_keys])

    # pdb sequences should be:
    assert data['construct_sequences']["2AST.A"] == sequences_2AST_static["A"]
    assert data['construct_sequences']["2AST.B"] == sequences_2AST_static["B"]
    assert data['construct_sequences']["2AST.C"] == sequences_2AST_static["C"]
    assert data['construct_sequences']["2AST.D"] == sequences_2AST_static["D"]

    # data pdb range values should be:
    assert data['construct_pdb_ranges']["2AST.A"] == [1002, 1160]
    assert data['construct_pdb_ranges']["2AST.B"] == [2095, 2419]
    assert data['construct_pdb_ranges']["2AST.C"] == [3005, 3073]
    assert data['construct_pdb_ranges']["2AST.D"] == [4181, 4190]

    assert data['modifications']["2AST.A"] == {}
    assert data['modifications']["2AST.B"] == {}
    assert data['modifications']["2AST.C"] == {}
    assert data['modifications']["2AST.D"][4187]['aa'] == "t"
    assert data['modifications']["2AST.D"][4187]['modification'] == "PHOSPHOTHREONINE"

def test_PDBSeqBuilder_XXX1_all_chains(structure_XXX1):
    builder = PDBSeqBuilder()
    data = builder.build_all_chains(structure_XXX1, "XXX1")
    # data should have five keys
    assert len(data) == 5
    # Elements in data should be construct_sequences,
    # construct_sequences_offsets, construct_pdb_offsets, construct_pdb_ranges
    # and modifications

    chain_data_keys = ['modifications', 'construct_sequences',
                       'construct_sequences_offsets', 'construct_pdb_offsets',
                       'construct_pdb_ranges']
    assert  all([k in data for k in chain_data_keys])
    # each element of data should have two keys
    assert all([len(data[k]) == 2 for k in chain_data_keys])
    # each element keys should be "XXX1.A" and "XXX1.B"
    assert all([all([ck in data[k] for ck in ["XXX1.A", "XXX1.B"]]) for k in chain_data_keys])


def test_PDBSeqBuilder_XXX1_single_chains(structure_XXX1):
    builder = PDBSeqBuilder()
    data = builder.build_chain(structure_XXX1, "A", "XXX1")
    # data should have five keys
    assert len(data) == 5
    construct_sequences,\
    construct_sequences_offsets,\
    construct_pdb_offsets,\
    construct_pdb_ranges,\
    modifications = data
    # construct_sequence for chain "XXX1.A" should be ASi
    assert construct_sequences == "ASi"
    # pdb range for chain "XXX1.A" should be [1002, 1004]
    assert construct_pdb_ranges == [1002, 1004]
    # construct_sequences_offsets should have three keys
    assert len(construct_sequences_offsets) == 3
    # construct_sequences_offsets keys should be 0, 1 and 2
    assert all([p in construct_sequences_offsets for p in [0, 1, 2]])
    # construct_sequences_offsets values should be:
    assert construct_sequences_offsets[0]["position"] == 1002
    assert construct_sequences_offsets[1]["position"] == 1003
    assert construct_sequences_offsets[2]["position"] == 1004
    assert construct_sequences_offsets[0]["residue"] == "A"
    assert construct_sequences_offsets[1]["residue"] == "S"
    assert construct_sequences_offsets[2]["residue"] == "i"
    # construct_pdb_offsets should have three keys
    assert len(construct_pdb_offsets) == 3
    # construct_pdb_offsets keys should be 0, 1 and 2
    assert all([p in construct_pdb_offsets for p in [1002, 1003, 1004]])
    # construct_pdb_offsets values should be:
    assert construct_pdb_offsets[1002]["offset"] == 0
    assert construct_pdb_offsets[1003]["offset"] == 1
    assert construct_pdb_offsets[1004]["offset"] == 2
    assert construct_pdb_offsets[1002]["residue"] == "A"
    assert construct_pdb_offsets[1003]["residue"] == "S"
    assert construct_pdb_offsets[1004]["residue"] == "i"
    # modifications should have only one element
    assert len(modifications) == 1
    # modifications element should be 1004
    assert 1004 in modifications
    # modifications values should be
    assert modifications[1004]["modification"] == '"ISOLEUCINE BORONIC ACID"'
    assert modifications[1004]["aa"] == 'i'

    data = builder.build_chain(structure_XXX1, "B", "XXX1")
    # data should have five keys
    assert len(data) == 5
    construct_sequences,\
    construct_sequences_offsets,\
    construct_pdb_offsets,\
    construct_pdb_ranges,\
    modifications = data
    # TODO: construct_sequence for chain "XXX1.B" should be "V?W"
    assert construct_sequences == "VW"
    # pdb range for chain "XXX1.B" should be [2095, 2097]
    assert construct_pdb_ranges == [2095, 2097]
    # construct_sequences_offsets should have three keys
    assert len(construct_sequences_offsets) == 2
    # construct_sequences_offsets keys should be 0, 1 and 2
    assert all([p in construct_sequences_offsets for p in [0, 1]])
    # construct_sequences_offsets values should be:
    assert construct_sequences_offsets[0]["position"] == 2095
    assert construct_sequences_offsets[1]["position"] == 2097
    assert construct_sequences_offsets[0]["residue"] == "V"
    assert construct_sequences_offsets[1]["residue"] == "W"
    # construct_pdb_offsets should have three keys
    assert len(construct_pdb_offsets) == 2
    # construct_pdb_offsets keys should be 0, 1 and 2
    assert all([p in construct_pdb_offsets for p in [2095, 2097]])
    # construct_pdb_offsets values should be:
    assert construct_pdb_offsets[2095]["offset"] == 0
    assert construct_pdb_offsets[2097]["offset"] == 1
    assert construct_pdb_offsets[2095]["residue"] == "V"
    assert construct_pdb_offsets[2097]["residue"] == "W"
    # modifications should have only one element
    assert not modifications
