import inspect
from os.path import dirname
from os.path import join
import pytest

@pytest.fixture(scope="session")
def test_data_folder():
    return join(dirname(inspect.stack()[0][1]), "test_data")
