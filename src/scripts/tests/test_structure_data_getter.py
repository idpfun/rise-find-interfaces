from os.path import dirname
from os.path import join
from os.path import exists
from os import remove
import inspect
import pytest
from structure.pdb_utils.structureDataGetter import StructureDataGetter

def removeExistentFiles(fs):
    for f in fs:
        if exists(f):
            remove(f)

@pytest.fixture(scope="module")
def files_2AST(test_data_folder):
    data_path = test_data_folder
    result_folder = join(data_path, "pdb")
    structure_pdb_file = join(result_folder, "2AST.pdb")
    structure_pfam_file = join(result_folder, "2AST.pfam.xml")
    structure_dssp_file = join(result_folder, "2AST.dssp")
    removeExistentFiles([structure_pdb_file, structure_pfam_file, structure_dssp_file])
    files = (data_path, structure_pdb_file, structure_pfam_file,
             structure_dssp_file)
    yield files  # provide the fixture value
    removeExistentFiles([structure_pdb_file, structure_pfam_file, structure_dssp_file])

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
@pytest.mark.requiresConnection()
def test_get_2AST(files_2AST):
    """
    get method should download files for 2AST and compute dssp if the files do
    not exists.
    """
    sdg = StructureDataGetter()
    data_path, structure_pdb_file, structure_pfam_file, structure_dssp_file = files_2AST
    # test assumes that dssp is in the PATH
    dssp_executable_path = "dssp"
    structure = sdg.get(data_path, "2AST", dssp_executable_path, structure_pdb_file)

    assert exists(structure_pdb_file)
    assert exists(structure_pfam_file)
    assert structure.id == "2AST"

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
def test_get_XXXX(test_data_folder):
    """
    get should return a Bio.PDB.Structure.Structure object if pdb contains more than one chain
    """
    sdg = StructureDataGetter()
    data_path = test_data_folder
    structure_pdb = join(test_data_folder, "pdb", "XXXX.pdb")
    dssp_path = "dssp"
    structure = sdg.get(data_path, "XXXX", dssp_path, structure_pdb)
    assert structure.id == "XXXX"

def test_get_YYYY(test_data_folder):
    """
    get should return a Bio.PDB.Structure.Structure object if pdb contains only one chain
    """
    sdg = StructureDataGetter()
    data_path = test_data_folder
    structure_pdb = join(test_data_folder, "pdb", "YYYY.pdb")
    dssp_path = "dssp"
    structure = sdg.get(data_path, "YYYY", dssp_path, structure_pdb)
    assert structure.id is "YYYY"
