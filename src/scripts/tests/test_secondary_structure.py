from os.path import dirname
from os.path import join
import inspect
import pytest
from structure.dssp.secondary_structure import DsspSecondaryStructure
from structure.dssp.secondary_structure import SecondaryStructureRegion
from structure.dssp.parser import Parser

def test_secondary_structure_region():
    ssr = SecondaryStructureRegion('H', 10, 20)
    assert ssr.ss_char == 'H'
    assert ssr.start == 10
    assert ssr.end == 20
    assert len(ssr) == 11
    assert ssr.toArray() == ['H', 11, 10, 20]

@pytest.fixture(scope='module')
def dssp_data_pdb_01(test_data_folder):
    dssp_file = join(test_data_folder, 'dssp_03.dssp')
    return Parser().dssp_parse(dssp_file, ['XXX5.A'], 'XXX5')

@pytest.fixture(scope='module')
def dssp_data_pdb_02(test_data_folder):
    dssp_file = join(test_data_folder, 'dssp_04.dssp')
    return Parser().dssp_parse(dssp_file, ['XXX5.A'], 'XXX5')

@pytest.fixture(scope='module')
def dssp_data_pdb_03(test_data_folder):
    dssp_file = join(test_data_folder, 'dssp_05.dssp')
    return Parser().dssp_parse(dssp_file, ['XXX5.A'], 'XXX5')

@pytest.fixture(scope='module')
def dssp_data_pdb_04(test_data_folder):
    dssp_file = join(test_data_folder, 'dssp_06.dssp')
    return Parser().dssp_parse(dssp_file, ['XXX5.A'], 'XXX5')

@pytest.fixture(scope='module')
def dssp_data_pdb_05(test_data_folder):
    dssp_file = join(test_data_folder, 'dssp_07.dssp')
    return Parser().dssp_parse(dssp_file, ['XXX5.A'], 'XXX5')

@pytest.fixture(scope='module')
def dssp_data_pdb_06(test_data_folder):
    dssp_file = join(test_data_folder, 'dssp_08.dssp')
    return Parser().dssp_parse(dssp_file, ['XXX5.A'], 'XXX5')

def test_define_ss_01(dssp_data_pdb_01):
    ss = DsspSecondaryStructure()
    ss.define_ss(dssp_data_pdb_01)
    # Helices from ss should have data for one chain
    assert len(ss.get_helices()) == 1
    # Helices from ss should have 2 elements for chain 'XXX5.A'
    assert len(ss.get_helices()['XXX5.A']) == 2
    assert ss.get_helices()['XXX5.A'][0].start == 1003
    assert ss.get_helices()['XXX5.A'][0].end == 1011
    assert len(ss.get_helices()['XXX5.A'][0]) == 9
    assert ss.get_helices()['XXX5.A'][1].start == 1013
    assert ss.get_helices()['XXX5.A'][1].end == 1023
    assert len(ss.get_helices()['XXX5.A'][1]) == 11

    ss2 = ss.get_secondary_structure()
    assert ss2['XXX5.A']['ss'] == '-HHHHHHHHH-HHHHHHHHHHH-HHHH'
    assert len(ss2['XXX5.A']['ss_detailed']) == 3
    assert len(ss2['XXX5.A']['ss_detailed'][0]) == 9
    assert len(ss2['XXX5.A']['ss_detailed'][1]) == 11
    assert len(ss2['XXX5.A']['ss_detailed'][2]) == 4
    assert ss2['XXX5.A']['ss_length'] == 'undefined'
    assert ss2['XXX5.A']['ss_type'] == 'Multi-partite'

def test_define_ss_02(dssp_data_pdb_02):
    ss = DsspSecondaryStructure()
    ss.define_ss(dssp_data_pdb_02)
    # Helices from ss should have no data
    assert len(ss.get_helices()) == 1
    assert not ss.get_helices()['XXX5.A']

    ss2 = ss.get_secondary_structure()
    assert ss2['XXX5.A']['ss'] == '-EEEEEEEEE---EEEEEEE-------'
    assert len(ss2['XXX5.A']['ss_detailed']) == 2
    assert len(ss2['XXX5.A']['ss_detailed'][0]) == 9
    assert len(ss2['XXX5.A']['ss_detailed'][1]) == 7
    assert ss2['XXX5.A']['ss_length'] == 'undefined'
    assert ss2['XXX5.A']['ss_type'] == 'Multi-partite'

def test_define_ss_03(dssp_data_pdb_03):
    ss = DsspSecondaryStructure()
    ss.define_ss(dssp_data_pdb_03)
    # Helices from ss should have no data
    assert len(ss.get_helices()) == 1
    assert not ss.get_helices()['XXX5.A']

    ss2 = ss.get_secondary_structure()
    assert ss2['XXX5.A']['ss'] == '-EEEEEEEEE-----------------'
    assert len(ss2['XXX5.A']['ss_detailed']) == 1
    assert len(ss2['XXX5.A']['ss_detailed'][0]) == 9
    assert ss2['XXX5.A']['ss_length'] == 9
    assert ss2['XXX5.A']['ss_type'] == 'Strand'

def test_define_ss_04(dssp_data_pdb_04):
    ss = DsspSecondaryStructure()
    ss.define_ss(dssp_data_pdb_04)
    # Helices from ss should have no data
    assert len(ss.get_helices()) == 1
    assert not ss.get_helices()['XXX5.A']

    # get_secondary_structure should ignore small regions of secondary structure
    ss2 = ss.get_secondary_structure()
    assert ss2['XXX5.A']['ss'] == '-EEEEEEEEE--------TT-------'
    assert len(ss2['XXX5.A']['ss_detailed']) == 1
    assert len(ss2['XXX5.A']['ss_detailed'][0]) == 9
    assert ss2['XXX5.A']['ss_length'] == 9
    assert ss2['XXX5.A']['ss_type'] == 'Strand'


def test_define_ss_05(dssp_data_pdb_05):
    ss = DsspSecondaryStructure()
    ss.define_ss(dssp_data_pdb_05)
    # Helices from ss should have no data
    assert len(ss.get_helices()) == 1
    assert not ss.get_helices()['XXX5.A']

    # get_secondary_structure should assign Turn to a chain if no other
    # secondary structure element are present even if the turn is small (at
    # least 2 continuos residues)
    ss2 = ss.get_secondary_structure()
    assert ss2['XXX5.A']['ss'] == '------------------TT-------'
    assert not ss2['XXX5.A']['ss_detailed']
    assert ss2['XXX5.A']['ss_length'] == 'undefined'
    assert ss2['XXX5.A']['ss_type'] == 'Turn'

def test_define_ss_06(dssp_data_pdb_06):
    ss = DsspSecondaryStructure()
    ss.define_ss(dssp_data_pdb_06)
    # This test checks that the beginning of the helix is correct.
    # See issue [#30]
    assert len(ss.get_helices()) == 1
    assert len(ss.get_helices()['XXX5.A']) == 1
    assert ss.get_helices()['XXX5.A'][0].start == 1007

    ss2 = ss.get_secondary_structure()
    assert ss2['XXX5.A']['ss'] == '--E--HHHHHHHHHHHH-------'
    assert len(ss2['XXX5.A']['ss_detailed']) == 1
    assert ss2['XXX5.A']['ss_length'] == 12
    assert ss2['XXX5.A']['ss_type'] == 'Alpha-helix'
