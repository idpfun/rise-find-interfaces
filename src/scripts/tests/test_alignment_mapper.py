"""
Test for the AlignmentMapper class
"""
import pytest
from pytest import raises
from Bio.SubsMat import MatrixInfo as matlist
from structure.pdb_utils.alignment_mapper import AlignmentMapper

@pytest.fixture(scope="module")
def mapper():
    """
    AlignmentMapper fixture
    """
    return AlignmentMapper()

def test_class_init(mapper):
    """
    mapper default values should be blosum62 matrix from biopyton for matrix,
    -10 for gap_open and -0.5 for gap_extend
        :param mapper:
    """
    assert mapper.matrix == matlist.blosum62
    assert mapper.gap_open == -10
    assert mapper.gap_extend == -0.5

def test_map_01(mapper):
    """
        :param mapper: fixture
    """
    pdb_residues = {
        0:{'position':1001, 'residue':'Q'},
        1:{'position':1002, 'residue':'W'},
        2:{'position':1003, 'residue':'T'},
        3:{'position':1004, 'residue':'Y'}
    }
    mapping = mapper.map('ACCESSION', 'QWTY', 'QWERTY', pdb_residues, True)

    # mapping should return these values:
    assert mapping['is_construct_aligned']
    assert mapping['pdb_alignment'] == 'QW--TY'
    assert mapping['residue_mapping']['max_count_protein'] == 6
    assert mapping['residue_mapping']['min_count_protein'] == 1
    assert mapping['residue_mapping']['n_terminal_overhang'] == 0
    assert mapping['residue_mapping']['pdb_alignment'] == 'QW--TY'
    assert mapping['residue_mapping']['protein'] == 'ACCESSION'
    assert mapping['residue_mapping']['uniprot_alignment'] == 'QWERTY'
    assert mapping['residue_mapping']['residues'][1001]['aligned']
    assert mapping['residue_mapping']['residues'][1001]['construct_offset'] == 0
    assert mapping['residue_mapping']['residues'][1001]['construct_residue'] == 'Q'
    assert mapping['residue_mapping']['residues'][1001]['pdb_offset'] == 1001
    assert mapping['residue_mapping']['residues'][1001]['pdb_residue'] == 'Q'
    assert mapping['residue_mapping']['residues'][1001]['protein_offset'] == 0
    assert mapping['residue_mapping']['residues'][1001]['protein_residue'] == 'Q'
    assert mapping['residue_mapping']['residues'][1004]['aligned']
    assert mapping['residue_mapping']['residues'][1004]['construct_offset'] == 3
    assert mapping['residue_mapping']['residues'][1004]['construct_residue'] == 'Y'
    assert mapping['residue_mapping']['residues'][1004]['pdb_offset'] == 1004
    assert mapping['residue_mapping']['residues'][1004]['pdb_residue'] == 'Y'
    assert mapping['residue_mapping']['residues'][1004]['protein_offset'] == 5
    assert mapping['residue_mapping']['residues'][1004]['protein_residue'] == 'Y'

def test_chose_best_alignment(mapper):
    """
        :param mapper: fixture
    """
    alignments = {
        ('AAAAA', 'A---A'),
        ('AAAAA', '-A--A'),
        ('AAAAA', '--A-A'),
        ('AAAAA', '---AA'),
        }
    assert mapper.chose_best_alignment(alignments)[1] == '---AA'

def test_map_02(mapper):
    """
        :param mapper: fixture
    """
    pdb_residues = {
        0:{'position':1001, 'residue':'Q'},
        1:{'position':1002, 'residue':'W'},
        2:{'position':1003, 'residue':'A'},
    }
    mapping = mapper.map('ACCESSION', 'QWA', 'QWQWA', pdb_residues, True)
    # pdb alignment should be '--QWA'
    assert mapping['pdb_alignment'] == '--QWA'
    assert mapping['residue_mapping']['pdb_alignment'] == '--QWA'

def test_map_03(mapper):
    """
        :param mapper: fixture
    """
    pdb_residues = {
        0:{'position':1001, 'residue':'Q'},
        1:{'position':1002, 'residue':'W'},
    }
    mapping = mapper.map('ACCESSION', 'QW', 'QWQW', pdb_residues, True)
    # pdb alignment should be any of '--QW' or 'QW--'
    assert mapping['is_construct_aligned']
    assert mapping['pdb_alignment'] == '--QW' or mapping['pdb_alignment'] == 'QW--'

def test_map_missing_sequences(mapper):
    """
        :param mapper: fixture
    """
    pdb_residues = {
        0:{'position':1001, 'residue':'Q'},
    }
    # map should raise a ValueError when any of the two sequences are None or
    # empty strings.
    with raises(ValueError) as error:
        mapper.map('ACCESSION', 'Q', None, pdb_residues, True)
        assert "ValueError" in  str(error)

    with raises(ValueError) as error:
        mapper.map('ACCESSION', None, 'Q', pdb_residues, True)
        assert "ValueError" in  str(error)

    with raises(ValueError) as error:
        mapper.map('ACCESSION', 'Q', '', pdb_residues, True)
        assert "ValueError" in  str(error)

    with raises(ValueError) as error:
        mapper.map('ACCESSION', '', 'Q', pdb_residues, True)
        assert "ValueError" in  str(error)

def test_map_unaligned_sequences(mapper):
    """
        :param mapper: fixture
    """
    pdb_residues = {
        0:{'position':1001, 'residue':'Q'},
        1:{'position':1001, 'residue':'W'},
        2:{'position':1001, 'residue':'E'},
    }
    mapping = mapper.map('ACCESSION', 'QWE', 'NFD', pdb_residues, True)
    assert not mapping['is_construct_aligned']

    pdb_residues = {
        0:{'position':1001, 'residue':'Q'},
        1:{'position':1001, 'residue':'W'},
        2:{'position':1001, 'residue':'E'},
    }
    mapping = mapper.map('ACCESSION', 'NFD', 'QWE', pdb_residues, True)
    assert not mapping['is_construct_aligned']

    pdb_residues = {
        0:{'position':1001, 'residue':'Q'},
        1:{'position':1001, 'residue':'W'},
        2:{'position':1001, 'residue':'E'},
    }
    mapping = mapper.map('ACCESSION', 'QFD', 'QWE', pdb_residues, True)
    assert not mapping['is_construct_aligned']

    pdb_residues = {
        0:{'position':1001, 'residue':'Q'},
        1:{'position':1001, 'residue':'W'},
        2:{'position':1001, 'residue':'E'},
    }
    mapping = mapper.map('ACCESSION', 'QWD', 'QWE', pdb_residues, True)
    assert mapping['is_construct_aligned']
