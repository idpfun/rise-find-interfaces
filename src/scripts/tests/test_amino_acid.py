"""
Tests for the structure.iupac.amino_acids.AminoAcids class
"""
from structure.iupac.amino_acids import AminoAcids

def test_amino_acid():
    """
    Tests for the structure.iupac.amino_acids.AminoAcids class
    """
    amino = AminoAcids()
    # Amino acid "5SQ" doesn;t have a parent in json input data.
    non_mon = [k for k, v in amino.items() if len(v["mon_nstd_parent_comp_id"]) == 0]
    assert len(non_mon) == 1
    assert non_mon[0] == "5SQ"

    # AminoAcids should return three parent names for "GYC"
    assert all([x in amino.get_parents_names("GYC") for x in ["CYS", "TYR", "GLY"]])
    # AminoAcids should return three one-letter-codes for "GYC"
    assert all([x in amino["GYC"]["one_letter_code"] for x in ["C", "Y", "G"]])

    parents = amino.get_parents("GYC")
    assert parents["CYS"]["one_letter_code"] == ["C"]
    assert parents["CYS"]["name"] == "CYSTEINE"
    assert parents["CYS"]["mon_nstd_parent_comp_id"] == ["?"]
    assert parents["TYR"]["one_letter_code"] == ["Y"]
    assert parents["TYR"]["name"] == "TYROSINE"
    assert parents["TYR"]["mon_nstd_parent_comp_id"] == ["?"]
    assert parents["GLY"]["one_letter_code"] == ["G"]
    assert parents["GLY"]["name"] == "GLYCINE"
    assert parents["GLY"]["mon_nstd_parent_comp_id"] == ["?"]

    # All retrieved parents should be in amino acids dict
    all_parents = [a for k, v in amino.items()
                   if "mon_nstd_parent_comp_id" in v and v["mon_nstd_parent_comp_id"] != ["?"]
                   for a in amino.get_parents_names(k)]
    assert all([x in amino for x in all_parents])

    # All 20 standard amino acids should have no parent
    std_aa = ['GLN', 'TRP', 'GLU', 'ARG', 'THR', 'TYR', 'ILE', 'PRO', 'ALA', 'SER',
              'ASP', 'PHE', 'GLY', 'HIS', 'LYS', 'LEU', 'CYS', 'VAL', 'ASN', 'MET']
    assert all(x in amino for x in std_aa)

    # The parent one-letter-code for B2I should be I
    assert amino.get_parent_one_letter_code("B2I") == "I"

    # The parent one-letter-code for NYG should be N
    assert amino.get_parent_one_letter_code("NYG") == "N"

    # The parent one-letter-code for AGQ should be Y
    assert amino.get_parent_one_letter_code("AGQ") == "Y"

    # All amino acids should have a name
    assert all(["name" in amino[x] for x in amino.three_letter_codes()])

    # items methods should return a dictionary with all data.
    assert all([len(k) <= 3 for k, v in amino.items()])
    assert all([isinstance(k, (str, unicode)) for k, v in amino.items()])
    assert all([all([x in v for x in  ["one_letter_code", "mon_nstd_parent_comp_id", "name"]])
                for k, v in amino.items()])
