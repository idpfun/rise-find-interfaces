from os.path import join
from os.path import exists
from os import remove
from os import makedirs
from os import rmdir
import pytest
from pytest import fixture
from storage.elm import ElmStorage

@fixture(scope="module")
def elm_folder(test_data_folder):
    elm_folder = join(test_data_folder, 'elm')
    makedirs(elm_folder)
    yield elm_folder
    rmdir(elm_folder)

def test_elm_storage_instances_from_pdb(elm_folder):
    pdb_id = "3N5U"
    sto = ElmStorage(elm_folder)
    elm_file = join(elm_folder, "{}.elm".format(pdb_id))
    if exists(elm_file) :
        remove(elm_file)
    
    assert not exists(elm_file)
    r = sto.instances_from_pdb(pdb_id)
    assert len(r) == 1
    assert r[0].accession == "ELMI002287"
    assert r[0].pdb[0] == pdb_id
    assert r[0].primary_acc == "P06400"
    assert exists(elm_file)

    r = sto.instances_from_pdb(pdb_id)
    assert r[0].accession == "ELMI002287"
    assert r[0].pdb[0] == pdb_id
    assert r[0].primary_acc == "P06400"

    remove(elm_file)

def test_elm_storage_all_pdbs(elm_folder):
    sto = ElmStorage(elm_folder)
    elm_file = join(elm_folder, "pdbs.tsv")
    if exists(elm_file) :
        remove(elm_file)
    assert not exists(elm_file)
    r = sto.all_pdbs()
    assert len(r) == 448
    assert all(['elm' in d and 'uniprot' in d for _, ds in r.items() for d in ds])
    assert len(r["2YJE"]) == 3

    assert exists(elm_file)
    r = sto.all_pdbs()
    assert len(r) == 448
    assert all(['elm' in d and 'uniprot' in d for _, ds in r.items() for d in ds])
    assert len(r["2YJE"]) == 3
    remove(elm_file)
    