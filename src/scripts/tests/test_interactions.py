from os.path import dirname
from os.path import join
from os.path import exists
from os import remove
import inspect
import pytest
from pytest import approx
import Bio.PDB
from structure.pdb_utils.interactions import InteractionFinder

@pytest.fixture(scope="module")
def structure_XXX4(test_data_folder):
    pdb_file = join(test_data_folder, 'pdb', 'XXX4.pdb')
    return Bio.PDB.PDBParser().get_structure('XXX4', pdb_file)

@pytest.fixture(scope="module")
def structure_XXX5(test_data_folder):
    pdb_file = join(test_data_folder, 'pdb', 'XXX5.pdb')
    return Bio.PDB.PDBParser().get_structure('XXX5', pdb_file)

def test_interactor_residues(structure_XXX4):
    ifind = InteractionFinder(None)
    interactions = ifind.interactor_residues(structure_XXX4[0].child_list[0], 10)
    interactions_reseq = {k.get_id()[1]:v for k, v in interactions.items()}
    # All residue with number 1002 to 1006 should have 10 interactions
    assert all([len(interactions_reseq[r]) == 10 for r in [1002, 1003, 1004, 1005, 1006]])
    # All residue with number 1002 to 1006 should have 5 unique interactions
    assert all([len(set(interactions_reseq[r])) == 5 for r in [1002, 1003, 1004, 1005, 1006]])
    # All residue with number 1007 should have 2 interactions
    assert len(interactions_reseq[1007]) == 2
    # All residue with number 1007 should have 1 unique interactions
    assert len(set(interactions_reseq[1007])) == 1

@pytest.mark.xfail()
def test_interactors_per_residue(structure_XXX4):
    ifind = InteractionFinder(None)
    interactions = ifind.interactor_residues(structure_XXX4[0].child_list[0], 10)
    ipr = ifind.interactors_per_residue(interactions)
    ipr.sort()
    assert ipr == [0, 2, 2, 2, 2, 2]

@pytest.mark.xfail()
def test_interactions_no_cache(structure_XXX4):
    ifind = InteractionFinder(None)
    result = ifind.intramolecular_in_chain(structure_XXX4[0].child_list[0], 10)
    assert  result == approx(float(9)/6)

def test_interactions_with_cache_read(structure_XXX4, test_data_folder):
    """
    intramolecular_in_chain should read a cache file when provided, and the
    value should be 99 for this test.
        :param structure_XXX4:
    """
    ifind = InteractionFinder(join(test_data_folder, 'interaction_test_cache'))
    result = ifind.intramolecular_in_chain(structure_XXX4[0].child_list[0], 10)
    assert  result == approx(99)

@pytest.fixture(scope="module")
def no_interaction_test_cache(test_data_folder):
    cache_file = join(test_data_folder, 'no_interaction_test_cache')
    if exists(cache_file):
        remove(cache_file)
    yield cache_file
    if exists(cache_file):
        remove(cache_file)

def test_interactions_with_cache_write(structure_XXX4, no_interaction_test_cache):
    """
    no_interaction_test_cache should create a cache file, if it not exists and
    the result is not the special value 100.
        :param structure_XXX4:
        :param no_interaction_test_cache:
    """
    ifind = InteractionFinder(no_interaction_test_cache)
    result = ifind.intramolecular_in_chain(structure_XXX4[0].child_list[0], 10)
    assert  not result == approx(100)
    assert  exists(no_interaction_test_cache)

@pytest.mark.skip(reason="""Can not be tested.
Bugged n_interactors is not zero when there are no interactions""")
def test_interactions_with_cache_write_XXX5(structure_XXX5, test_data_folder):
    """
    intramolecular_in_chain shouldn't write to cache when there are no interactor
    residues (as expected in test pdb XXX5).
        :param structure_XXX5:
    """
    cache_file = join(test_data_folder, 'no_interaction_test_cache')
    ifind = InteractionFinder(cache_file)
    result = ifind.intramolecular_in_chain(structure_XXX5[0].child_list[0], 10)
    assert  result == approx(100)
    assert  not exists(cache_file)
