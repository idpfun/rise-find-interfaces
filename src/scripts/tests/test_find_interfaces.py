from os.path import dirname
from os.path import join
import inspect
import json
import io
import pytest
from pytest import raises
from structure.dssp.dssp_data import DsspDataPdb
from structure.dssp.dssp_data import DsspPdbChain
from structure.dssp.dssp_data import DsspPdbSummaryChain
from structure.custom_json_encoder import CjsonEncoder
from structure.find_interfaces import findInterfaces

@pytest.fixture(scope="module")
def findInterfacesObj():
    return findInterfaces()

def integration_pdb(pdb):
    findInterfacesObj = findInterfaces()

    findInterfacesObj.options = {
        'skip_large_multimers': True,
        'url_root': 'http://localhost:6543',
        'coiled_coil_helix_helix_contact_proportion_cutoff': 0.75,
        'intramolecular_cutoff': 10.0,
        'data_path': '/home/data/',
        'remake_age': 180.0,
        'disorder_domain_length': 15.0,
        'max_peptide_length': 25.0,
        'pdb_id': pdb,
        'max_intramolecular_interactions': 10.0,
        'chain_id': '',
        'wait_time': 0.1,
        'peptide_length': 30.0,
        'inter_chain_distance': 8,
        'blast_path': '/home/tools/blast/bin/',
        'clustalo_path': '/home/tools/clustalo/clustalo',
        'surface_accessibility_cutoff': 0.35,
        'domain_contacts_cutoff': 3.0,
        'clustalw_path': '/home/tools/clustalw/clustalw2',
        'task': 'find_motifs',
        'dssp_path': '/usr/local/bin/dssp',
        'skip_homomultimers': True,
        'resources_path': '/home/data/',
        'slimsuite_path': '/home/scripts/slimsuite/',
        'sidechain_interactions_only': 'SideToAny',
        'remake': True,
        'debug': False,
        'outfmt': 'tdt',
        'coiled_coil_helix_helix_contact_length_cutoff': 7.0,
        'user_email': 'normandavey@gmail.com'}

    return findInterfacesObj.find_motifs()    



def test_integration_4DX9(test_data_folder):
    motifs = integration_pdb("4DX9")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    # Uncomment to generate an output json file to compare with the original file
    with open(join(test_data_folder, 'find_motifs_4DX9_new.json'), 'w') as fh:
       fh.write(motifs_string)

    motif_expected_file = join(test_data_folder, 'find_motifs_4DX9.json')
    motif_expected = io.open(motif_expected_file, 'r', encoding="utf8").read()

    motifs_string_lines = motifs_string.split("\n")
    motif_expected_lines = motif_expected.split("\n")

    # json data should have the same number of lines
    assert len(motifs_string_lines) == len(motif_expected_lines)

    # json data should be identical
    assert  motifs_string == motif_expected

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
def test_integration_1Q1J(test_data_folder):
    motifs = integration_pdb("1Q1J")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    # Uncomment to generate an output json file to compare with the original file
    with open(join(test_data_folder, 'find_motifs_1Q1J_new.json'), 'w') as fh:
       fh.write(motifs_string)

    motif_expected_file = join(test_data_folder, 'find_motifs_1Q1J.json')
    motif_expected = io.open(motif_expected_file, 'r', encoding="utf8").read()

    motifs_string_lines = motifs_string.split("\n")
    motif_expected_lines = motif_expected.split("\n")

    # json data should have the same number of lines
    assert len(motifs_string_lines) == len(motif_expected_lines)

    # json data should be identical
    assert  motifs_string == motif_expected

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
def test_integration_5M5G(test_data_folder):
    motifs = integration_pdb("5M5G")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    # Uncomment to generate an output json file to compare with the original file
    with open(join(test_data_folder, 'find_motifs_5M5G_new.json'), 'w') as fh:
       fh.write(motifs_string)

    motif_expected_file = join(test_data_folder, 'find_motifs_5M5G.json')
    motif_expected = io.open(motif_expected_file, 'r', encoding="utf8").read()

    motifs_string_lines = motifs_string.split("\n")
    motif_expected_lines = motif_expected.split("\n")

    # json data should have the same number of lines
    assert len(motifs_string_lines) == len(motif_expected_lines)

    # json data should be identical
    assert  motifs_string == motif_expected

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
def test_integration_2AST(test_data_folder):
    motifs = integration_pdb("2AST")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    # Uncomment to generate an output json file to compare with the original file
    with open(join(test_data_folder, 'find_motifs_2AST_new.json'), 'w') as fh:
       fh.write(motifs_string)

    motif_expected_file = join(test_data_folder, 'find_motifs_2AST.json')
    motif_expected = io.open(motif_expected_file, 'r', encoding="utf8").read()

    motifs_string_lines = motifs_string.split("\n")
    motif_expected_lines = motif_expected.split("\n")

    # json data should have the same number of lines
    assert len(motifs_string_lines) == len(motif_expected_lines)

    # json data should be identical
    assert  motifs_string == motif_expected

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
def test_integration_2ast(test_data_folder):
    motifs = integration_pdb("2ast")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    # Uncomment to generate an output json file to compare with the original file
    # with open(join(test_data_folder, 'find_motifs_2ast_new.json'), 'w') as fh:
    #    fh.write(motifs_string)

    motif_expected_file = join(test_data_folder, 'find_motifs_2AST.json')
    motif_expected = io.open(motif_expected_file, 'r', encoding="utf8").read()

    motifs_string_lines = motifs_string.split("\n")
    motif_expected_lines = motif_expected.split("\n")

    # json data should have the same number of lines
    assert len(motifs_string_lines) == len(motif_expected_lines)

    # json data should be identical
    assert  motifs_string == motif_expected

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
def test_integration_5JEM(test_data_folder):
    motifs = integration_pdb("5JEM")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    # Uncomment to generate an output json file to compare with the original file
    with open(join(test_data_folder, 'find_motifs_5JEM_new.json'), 'w') as fh:
       fh.write(motifs_string)

    motif_expected_file = join(test_data_folder, 'find_motifs_5JEM.json')
    motif_expected = io.open(motif_expected_file, 'r', encoding="utf8").read()

    motifs_string_lines = motifs_string.split("\n")
    motif_expected_lines = motif_expected.split("\n")

    # json data should have the same number of lines
    assert len(motifs_string_lines) == len(motif_expected_lines)

    # json data should be identical
    assert  motifs_string == motif_expected

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
def test_integration_4J2X(test_data_folder):
    motifs = integration_pdb("4J2X")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    # Uncomment to generate an output json file to compare with the original file
    with open(join(test_data_folder, 'find_motifs_4J2X_new.json'), 'w') as fh:
       fh.write(motifs_string)

    motif_expected_file = join(test_data_folder, 'find_motifs_4J2X.json')
    motif_expected = io.open(motif_expected_file, 'r', encoding="utf8").read()

    motifs_string_lines = motifs_string.split("\n")
    motif_expected_lines = motif_expected.split("\n")

    # json data should have the same number of lines
    assert len(motifs_string_lines) == len(motif_expected_lines)

    # json data should be identical
    assert  motifs_string == motif_expected    

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
def test_integration_4LG6(test_data_folder):
    motifs = integration_pdb("4LG6")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    # Uncomment to generate an output json file to compare with the original file
    with open(join(test_data_folder, 'find_motifs_4LG6_new.json'), 'w') as fh:
       fh.write(motifs_string)

    motif_expected_file = join(test_data_folder, 'find_motifs_4LG6.json')
    motif_expected = io.open(motif_expected_file, 'r', encoding="utf8").read()

    motifs_string_lines = motifs_string.split("\n")
    motif_expected_lines = motif_expected.split("\n")

    # json data should have the same number of lines
    assert len(motifs_string_lines) == len(motif_expected_lines)

    # json data should be identical
    assert  motifs_string == motif_expected    

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
def test_integration_3VI4(test_data_folder):
    motifs = integration_pdb("3VI4")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    # Uncomment to generate an output json file to compare with the original file
    with open(join(test_data_folder, 'find_motifs_3VI4_new.json'), 'w') as fh:
       fh.write(motifs_string)

    motif_expected_file = join(test_data_folder, 'find_motifs_3VI4.json')
    motif_expected = io.open(motif_expected_file, 'r', encoding="utf8").read()

    motifs_string_lines = motifs_string.split("\n")
    motif_expected_lines = motif_expected.split("\n")

    # json data should have the same number of lines
    assert len(motifs_string_lines) == len(motif_expected_lines)

    # json data should be identical
    assert  motifs_string == motif_expected 

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
def test_integration_1B72(test_data_folder):
    motifs = integration_pdb("1B72")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    # Uncomment to generate an output json file to compare with the original file
    with open(join(test_data_folder, 'find_motifs_1B72_new.json'), 'w') as fh:
       fh.write(motifs_string)

    motif_expected_file = join(test_data_folder, 'find_motifs_1B72.json')
    motif_expected = io.open(motif_expected_file, 'r', encoding="utf8").read()

    motifs_string_lines = motifs_string.split("\n")
    motif_expected_lines = motif_expected.split("\n")

    # json data should have the same number of lines
    assert len(motifs_string_lines) == len(motif_expected_lines)

    # json data should be identical
    assert  motifs_string == motif_expected 


def test_integration_1A9A(test_data_folder):
    motifs = integration_pdb("1A9A")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    # Uncomment to generate an output json file to compare with the original file
    with open(join(test_data_folder, 'find_motifs_1A9A_new.json'), 'w') as fh:
       fh.write(motifs_string)

    motif_expected_file = join(test_data_folder, 'find_motifs_1A9A.json')
    motif_expected = io.open(motif_expected_file, 'r', encoding="utf8").read()

    motifs_string_lines = motifs_string.split("\n")
    motif_expected_lines = motif_expected.split("\n")

    # json data should have the same number of lines
    assert len(motifs_string_lines) == len(motif_expected_lines)

    # json data should be identical
    assert  motifs_string == motif_expected 

def test_integration_2Y6E(test_data_folder):
    motifs = integration_pdb("2Y6E")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)
    # Should be error: Multimer but only one entity in structure [skip_homomultimers set].
    assert motifs["status"] == "Error"
    assert "Multimer but only one entity in structure" in motifs["error_type"]
    
def test_integration_2PZO(test_data_folder):
    motifs = integration_pdb("2PZO")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)
    # Should be error: Entry is marked as obsolete.
    assert motifs["status"] == "Error"
    assert "Entry is marked as obsolete" in motifs["error_type"]
    
def test_integration_2F1Y(test_data_folder):
    motifs = integration_pdb("2F1Y")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)
    # Should be error: Only one entity in structure. Not a complex.
    assert motifs["status"] == "Error"
    assert "Only one entity in structure" in motifs["error_type"]

@pytest.mark.filterwarnings('ignore::Bio.PDB.PDBExceptions.PDBConstructionWarning')
def test_integration_5DE5(test_data_folder):
    motifs = integration_pdb("5DE5")
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    # Uncomment to generate an output json file to compare with the original file
    with open(join(test_data_folder, 'find_motifs_5DE5_new.json'), 'w') as fh:
       fh.write(motifs_string)

    motif_expected_file = join(test_data_folder, 'find_motifs_5DE5.json')
    motif_expected = io.open(motif_expected_file, 'r', encoding="utf8").read()

    motifs_string_lines = motifs_string.split("\n")
    motif_expected_lines = motif_expected.split("\n")

    # json data should have the same number of lines
    assert len(motifs_string_lines) == len(motif_expected_lines)

    # json data should be identical
    assert  motifs_string == motif_expected 

@pytest.fixture(scope="module")
def fi_mapping_01():
    obj = findInterfaces()
    # Set up to test get_construct_for_mapping
    obj.data['construct_sequences'] = {}
    obj.data['construct_sequences']['chain'] = "AAAC--DDD"
    obj.data['dssp'] = DsspDataPdb()
    obj.data['dssp'].add_chain('chain', DsspPdbChain('chain'))
    obj.data['dssp'].chain('chain').add_summary(
        DsspPdbSummaryChain("GGGM--EEE", None, None, None, None, None, None))
    # Set up to test get_protein_for_mapping
    obj.data['chain_details'] = {}
    obj.data['chain_details']['chain'] = {}
    obj.data['chain_details']['chain2'] = {}
    obj.data['chain_details']['chain']['accession'] = 'uniprot_accession'
    obj.data['chain_details']['chain2']['accession'] = 'uniprot_accession2'
    obj.data["protein_info"] = {}
    obj.data["protein_info"]['uniprot_accession'] = {}
    obj.data["protein_info"]['uniprot_accession']['sequence'] = "AAAD--DDD"
    obj.data["protein_info"]['uniprot_accession2'] = {}
    obj.data["protein_info"]['uniprot_accession2']['sequence'] = ""
    return obj

@pytest.fixture(scope="module")
def fi_mapping_02():
    obj = findInterfaces()
    obj.data['construct_sequences'] = {}
    obj.data['construct_sequences']['chain'] = "AAAC--DDD"
    obj.data['dssp'] = {}
    obj.data['dssp']['summary'] = {}
    return obj

def test_get_protein_for_mapping(fi_mapping_01):
    # get_protein_for_mapping should return "AAADDDD" for fi_mapping_01
    # Sequence shouldn't have gaps
    assert fi_mapping_01.get_protein_for_mapping('chain', "GGGEEEE") == 'AAADDDD'
    # Sequence should have the default value "GGGEEEE" for a chain with protein
    # sequence equals to "".
    assert fi_mapping_01.get_protein_for_mapping('chain2', "GGGEE--") == 'GGGEE--'
    # get_protein_for_mapping should raise a KeyError if chain is non existent.
    # 'chain3' doesn't exists.
    with raises(KeyError) as e:
        fi_mapping_01.get_protein_for_mapping('chain3', "GGGEE--")
    assert  "KeyError" in str(e)

def test_get_construct_for_mapping_01(fi_mapping_01):
    # The result from get_construct_for_mapping should equal to the
    # sequence of dssp summary if both, sequence for dssp and sequence from
    # construct_sequences are available.
    # Result sequence should't have gaps
    assert fi_mapping_01.get_construct_for_mapping('chain') == "GGGMEEE"
    # get_construct_for_mapping should raise a KeyError when a non-existent
    # chain is given.
    with raises(KeyError) as e:
        fi_mapping_01.get_construct_for_mapping('random_chain')
    assert "KeyError" in str(e)

def test_get_construct_for_mapping_02(fi_mapping_02):
    # The result from get_construct_for_mapping should equal to the
    # construct_sequences if sequence of dssp summary is not available.
    # Result sequence should't have gaps
    assert fi_mapping_02.get_construct_for_mapping('chain') == "AAACDDD"

def test_read_pfam_xml_file_full_collapse(findInterfacesObj, test_data_folder):
    collapsed = findInterfacesObj.load_collapsed_pfams()
    pfam_xml_file = join(test_data_folder, 'XXX3.pfam.xml')
    domain, names, pfam = findInterfacesObj.read_pfam_xml_file(pfam_xml_file, collapsed)
    # domain should have four elements
    assert len(domain) == 4
    # Domain values should be PFT0001, PFT0002, PFT0003 and PFT0004
    assert  all([n in domain for n in ['PFT0001', 'PF00036', 'PF00400', 'PFT0004']])
    # Domain values shouldn't have version numbers
    assert  all([not '.' in n for n in domain])
    # names should have 4 elements
    assert len(names) == 2
    # names keys should be PFT0001, PFT0002, PFT0003 and PFT0004
    assert  all([n in names for n in ['PFT0001', 'PFT0004']])
    # names values should be:
    assert names['PFT0001'] == 'Test_pfam_01, description'
    assert names['PFT0004'] == 'Test_pfam_04, description'
    # pfam should have three keys
    assert  len(pfam) == 3
    # pfam keys should be "A", "B" and "C"
    assert  all([n in pfam for n in ['A', 'B', 'C']])
    # pfam key "A" should have two elements, "B" and "C" only one
    assert  len(pfam["A"]) == 2
    assert  len(pfam["B"]) == 1
    assert  len(pfam["C"]) == 1
    # Accession for pfams chain "A" should be PFT0001 and PF15911,
    # for chain "B" should be PF13202 and for chain "C" should be "PFT0004"
    assert all([pf in [d["pfamAcc"] for d in pfam["A"]] for pf in ['PFT0001', 'PF15911']])
    assert all([pf in [d["pfamAcc"] for d in pfam["B"]] for pf in ['PF13202']])
    assert all([pf in [d["pfamAcc"] for d in pfam["C"]] for pf in ['PFT0004']])

def test_read_pfam_xml_file_empty_collapse(findInterfacesObj, test_data_folder):
    collapsed = {}
    pfam_xml_file = join(test_data_folder, 'XXX2.pfam.xml')
    domain, names, pfam = findInterfacesObj.read_pfam_xml_file(pfam_xml_file, collapsed)
    # domain should have four elements
    assert len(domain) == 4
    # Domain values should be PFT0001, PFT0002, PFT0003 and PFT0004
    assert  all([n in domain for n in ['PFT0001', 'PFT0002', 'PFT0003', 'PFT0004']])
    # Domain values shouldn't have version numbers
    assert  all([not '.' in n for n in domain])
    # names should have 4 elements
    assert len(names) == 4
    # names keys should be PFT0001, PFT0002, PFT0003 and PFT0004
    assert  all([n in names for n in ['PFT0001', 'PFT0002', 'PFT0003', 'PFT0004']])
    # names values should be:
    assert names['PFT0001'] == 'Test_pfam_01, description'
    assert names['PFT0002'] == 'Test_pfam_02, description'
    assert names['PFT0003'] == 'Test_pfam_03, description'
    assert names['PFT0004'] == 'Test_pfam_04, description'
    # pfam should have three keys
    assert  len(pfam) == 3
    # pfam keys should be "A", "B" and "C"
    assert  all([n in pfam for n in ['A', 'B', 'C']])
    # pfam key "A" should have two elements, "B" and "C" only one
    assert  len(pfam["A"]) == 2
    assert  len(pfam["B"]) == 1
    assert  len(pfam["C"]) == 1
    # Accession for pfams chain "A" shoudb be PFT0001 and PFT0002
    assert all([pf in [d["pfamAcc"] for d in pfam["A"]] for pf in ['PFT0001', 'PFT0002']])

def test_load_collapsed_pfams(findInterfacesObj):
    collapsed = findInterfacesObj.load_collapsed_pfams()

    # Collapsed should have 68 elements
    assert len(collapsed) == 68

    # Collapsed should have 9 different values
    assert len(set(collapsed.values())) == 9

    # Pfam PF00515 should have 22 children, etc
    assert  len([k for k, v in collapsed.items() if v == "PF00515"]) == 22
    assert  len([k for k, v in collapsed.items() if v == "PF00023"]) == 5
    assert  len([k for k, v in collapsed.items() if v == "PF00036"]) == 12
    assert  len([k for k, v in collapsed.items() if v == "PF00076"]) == 6
    assert  len([k for k, v in collapsed.items() if v == "PF00400"]) == 4
    assert  len([k for k, v in collapsed.items() if v == "PF00514"]) == 5
    assert  len([k for k, v in collapsed.items() if v == "PF00515"]) == 22
    assert  len([k for k, v in collapsed.items() if v == "PF00560"]) == 9
    assert  len([k for k, v in collapsed.items() if v == "PF00595"]) == 3
    assert  len([k for k, v in collapsed.items() if v == "PF02172"]) == 2

def test_parse_general_data_with_bad_formed_xml_file(findInterfacesObj, test_data_folder):
    findInterfacesObj.paths["structure_xml"] = join(test_data_folder, "bad_formed.xml")
    status = findInterfacesObj.parse_general_data()
    assert status['status'] == "Error"
    assert "is not well formed" in status['error_type']
