from os.path import join
from os import remove
from os.path import exists
import pytest
from structure.dssp.dssp import Dssp
from pprint import pprint

@pytest.fixture(scope="module")
def pdb_file(test_data_folder):
    return join(test_data_folder, "pdb", "XXX6.pdb")

@pytest.fixture(scope="module")
def pdb_file_2(test_data_folder):
    return join(test_data_folder, "pdb", "XXX7.pdb")

@pytest.fixture(scope="module")
def runner():
    return Dssp()

@pytest.fixture()
def remove_dssp_files(test_data_folder):
    yield
    files_to_remove = [
        join(test_data_folder, "pdb", "XXX6.dssp"),
        join(test_data_folder, "pdb", "XXX7.dssp"),
        join(test_data_folder, "pdb", "XXX6.A_Upper.dssp"),
        join(test_data_folder, "pdb", "XXX6.B_Upper.dssp"),
        join(test_data_folder, "pdb", "XXX7.A_Upper.dssp"),
        join(test_data_folder, "pdb", "XXX7.B_Upper.dssp")]
    for f in files_to_remove:
        if exists(f): 
            remove(f)

def test_split(pdb_file, runner, remove_dssp_files):
    """
    Test the names of the saved files
    """
    results, saved_files = runner.run(pdb_file, "XXX6", parse=False)
    assert len(results) == 2
    assert all([v.startswith(pdb_file[0:-4]) for v in saved_files.values()])
    assert all([v.endswith("{}_Upper.dssp".format(c)) for c, v in saved_files.items()])

def test_dssp(pdb_file, runner):
    """
    Test dssp output with no parsing.
    """
    r, _ = runner.run(pdb_file, "XXX6", parse=False)
    assert(r['A'].startswith("==== Secondary Structure Definition by the program DSSP, CMBI version by M.L. Hekkelman/2010-10-21 ===="))
    assert(len(r['A'].split("\n")) == 30)


def test_run_with_parse_option(pdb_file, runner, remove_dssp_files):
    """
    Test the integrity of dssp results.
    """
    result, saved_files = runner.run(pdb_file, "XXX6")
    assert sorted(result.chains()) == ['XXX6.A', 'XXX6.B']
    assert sorted(result.chain("XXX6.A").positions.keys()) == [177, 178, 179, 180]
    assert sorted(result.chain("XXX6.B").positions.keys()) == [235, 236, 237, 238]
    assert sorted(saved_files.keys()) == ['A', 'B']
    assert all([exists(f) for f in saved_files.values()])

def test_run_solvent_access_difference(pdb_file_2, runner, remove_dssp_files):
    '''
    Solvent accesibility should be lower in joined dssp result compared to splitted dssp result.
    '''
    result_splitted, _ = runner.run(pdb_file_2, "XXX7")
    result_joined, _ = runner.run(pdb_file_2, "XXX7", splitted=False)
    assert sorted(result_splitted.chains()) == ['XXX7.A', 'XXX7.B']
    assert sorted(result_joined.chains()) == ['XXX7.A', 'XXX7.B']
    assert sorted(result_splitted.chain("XXX7.A").positions.keys()) == [177, 178, 179, 180]
    assert sorted(result_splitted.chain("XXX7.B").positions.keys()) == [235, 236, 237, 238]
    assert sorted(result_joined.chain("XXX7.A").positions.keys()) == [177, 178, 179, 180]
    assert sorted(result_joined.chain("XXX7.B").positions.keys()) == [235, 236, 237, 238]

    # Solvent accessibility from splitted dssp run should be larger or equal than joined dssp run.
    splitted_acc = [result_splitted.chain("XXX7.A").get_by_name(i, "solventAccessibility") 
        for i in [177, 178, 179, 180]]
    joined_acc = [result_joined.chain("XXX7.A").get_by_name(i, "solventAccessibility") 
        for i in [177, 178, 179, 180]]
    assert all([x<=y for x,y in zip(joined_acc, splitted_acc)])
    assert any([x<y for x,y in zip(joined_acc, splitted_acc)])

    # Solvent accessibility differences between splitted and joined should be larger or equal to 
    # zero. Solvent accessibility difference is stored as a new attribute "saDifference" for every
    # position of everu chain.
    result_diff = result_splitted.solvent_accesibility_difference(result_joined)
    diff_acc = [result_diff.chain("XXX7.A").get_by_name(i, "saDifference") 
        for i in [177, 178, 179, 180]]
    assert all([x>=0 for x in diff_acc])
    # Original DsspDataPdb object are not affected by the difference operation.
    with pytest.raises(KeyError) as e1:
        result_splitted.chain("XXX7.A").get_by_name(178, "saDifference")
    assert "KeyError" in str(e1)
    with pytest.raises(KeyError) as e2:
        result_joined.chain("XXX7.A").get_by_name(178, "saDifference")
    assert "KeyError" in str(e2)