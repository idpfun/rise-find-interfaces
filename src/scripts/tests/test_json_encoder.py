import json
from pytest import fixture
from Bio.PDB.Residue import Residue
from Bio.PDB.Atom import Atom
from structure.interfaces import Contact
from structure.custom_json_encoder import CjsonEncoder

@fixture(scope='module')
def simple_contact():
    r1 = Residue((' ', 123, ' '), "GLY", "    ")
    r1a1 = Atom("CA", [1.0, 1.0, 1.0], 0.5, 0.5, "CA", "CA", 101, "C")
    r1.add(r1a1)
    r2 = Residue((' ', 501, ' '), "HIS", "    ")
    r2a1 = Atom("CA", [0.0, 0.0, 0.0], 0.5, 0.5, "CA", "CA", 201, "C")
    r2.add(r2a1)
    return Contact(4.5,
                   r1, r1a1, None,
                   r2, r2a1, None)

def test_custom_json_encoder(simple_contact):
    json_o = json.dumps(simple_contact, cls=CjsonEncoder)
    expected = '{"contact_atom": "CA", "contact_backbone": null, "contact_position": ' + \
               '501, "backbone": null, "contact_residue": "H", "contact_residue_code": "HIS", ' + \
               '"atom": "CA", "residue": "G", "distance": 4.5, ' + \
               '"contact_type": "sidechain-sidechain", "position": 123}'
    assert json_o == expected
