from os.path import dirname
from os.path import join
import inspect
import pytest
from pytest import approx
from structure.dssp.parser import Parser

def test_read_a_non_existent_dssp_file():
    """
    Parsing a dssp file result shold be None when a non existent file is given
    """
    dssp_file = join("test_data", "non_existent_file")
    p = Parser()

    with pytest.raises(IOError) as e:
        _ = p.dssp_parse(dssp_file, ["PDB1.A", "PDB1.B"], "PDB1")
    assert "IOError" in str(e)

def test_read_a_dssp_file_with_two_chains(test_data_folder):
    # pylint: disable=too-many-statements
    dssp_file = join(test_data_folder, "dssp_01.dssp")
    p = Parser()
    dssp = p.dssp_parse(dssp_file, ["PDB1.A", "PDB1.B"], "PDB1")

    # chains in test dssp should have two chains: "PDB1.A", "PDB1.B"
    assert len(dssp.chains()) == 2
    assert all([x in dssp.chains() for x in  ["PDB1.A", "PDB1.B"]])

    # chain "PDB1.A" should have AA with integers 1002, 1003, 1006, 1007
    assert len(dssp.chain("PDB1.A")) == 4
    assert all([x in dssp.chain("PDB1.A") for x in [1002, 1003, 1006, 1007]])

    # chain "PDB1.B" should have AA with integers 1008, 1009
    assert len(dssp.chain("PDB1.B")) == 2
    assert all([x in dssp.chain("PDB1.B") for x in [1008, 1009]])
    assert all([x in dssp.chain("PDB1.B") for x in [1008, 1009]])

    # values in residue 1006 should be:
    assert dssp.chain("PDB1.A").get_by_name(1006, "count") == "4"
    assert dssp.chain("PDB1.A").get_by_name(1006, "offset") == "1006"
    assert dssp.chain("PDB1.A").get_by_name(1006, "chain") == "A"
    assert dssp.chain("PDB1.A").get_by_name(1006, "aa") == "L"
    assert dssp.chain("PDB1.A").get_by_name(1006, "SecondaryStructure") == "E"
    assert dssp.chain("PDB1.A").get_by_name(1006, "3-turns/helix") == "q"
    assert dssp.chain("PDB1.A").get_by_name(1006, "4-turns/helix") == "w"
    assert dssp.chain("PDB1.A").get_by_name(1006, "5-turns/helix") == "e"
    assert dssp.chain("PDB1.A").get_by_name(1006, "geometricalBend") == "r"
    assert dssp.chain("PDB1.A").get_by_name(1006, "chirality") == "-"
    assert dssp.chain("PDB1.A").get_by_name(1006, "betaBridgeLabel1") == "A"
    assert dssp.chain("PDB1.A").get_by_name(1006, "betaBridgeLabel2") == "b"
    assert dssp.chain("PDB1.A").get_by_name(1006, "betaBridgePartnerResnum") == "13"
    assert dssp.chain("PDB1.A").get_by_name(1006, "betaSheetLabel") == "38"
    assert dssp.chain("PDB1.A").get_by_name(1006, "solventAccessibility") == "3"
    assert dssp.chain("PDB1.A").get_by_name(1006, "N-H-->O_1") == "8,-2.6"
    assert dssp.chain("PDB1.A").get_by_name(1006, "O-->H-N_1") == "8,-2.9"
    assert dssp.chain("PDB1.A").get_by_name(1006, "N-H-->O_2") == "-2,-0.4"
    assert dssp.chain("PDB1.A").get_by_name(1006, "O-->H-N_2") == "2,-0.5"
    assert dssp.chain("PDB1.A").get_by_name(1006, "TCO") == "-0.983"
    assert dssp.chain("PDB1.A").get_by_name(1006, "KAPPA") == "-119.7"
    assert dssp.chain("PDB1.A").get_by_name(1006, "ALPHA") == "-150.3"
    assert dssp.chain("PDB1.A").get_by_name(1006, "PHI") == "-127.2"
    assert dssp.chain("PDB1.A").get_by_name(1006, "PSI") == "-132.8"
    assert dssp.chain("PDB1.A").get_by_name(1006, "X-CA") == "-1118.1"
    assert dssp.chain("PDB1.A").get_by_name(1006, "Y-CA") == "-1142.9"
    assert dssp.chain("PDB1.A").get_by_name(1006, "Z-CA") == "-1111.2"

    # summary in dssp for chain "PDB1.A" should have:
    assert dssp.chain("PDB1.A").get_summary().sequence == "AS--LQ"
    assert "".join(dssp.chain("PDB1.A").get_summary().accessible_residues) == "AS--xQ"
    assert "".join(dssp.chain("PDB1.A").get_summary().buried_residues) == "xx--Lx"
    assert dssp.chain("PDB1.A").get_summary().mean_surface_accessibility == approx((107+61+3+65)/4)
    assert dssp.chain("PDB1.A").get_summary().secondary_structure == "-E--EE"
    assert len(dssp.chain("PDB1.A").get_summary().surface_accessibility) == 4
    assert dssp.chain("PDB1.A").get_summary().surface_accessibility[1002] == 107
    assert dssp.chain("PDB1.A").get_summary().surface_accessibility[1003] == 61
    assert dssp.chain("PDB1.A").get_summary().surface_accessibility[1006] == 3
    assert dssp.chain("PDB1.A").get_summary().surface_accessibility[1007] == 65
    assert len(dssp.chain("PDB1.A").get_summary().surface_accessibility_normalised) == 4
    assert dssp.chain("PDB1.A").get_summary().surface_accessibility_normalised[1002] ==\
        approx(107.0/124)
    assert dssp.chain("PDB1.A").get_summary().surface_accessibility_normalised[1003] ==\
        approx(61.0/113)
    assert dssp.chain("PDB1.A").get_summary().surface_accessibility_normalised[1006] ==\
        approx(3.0/199)
    assert dssp.chain("PDB1.A").get_summary().surface_accessibility_normalised[1007] ==\
        approx(65.0/192)
