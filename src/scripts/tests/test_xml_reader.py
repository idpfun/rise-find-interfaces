from os.path import dirname
from os.path import join
import inspect
import pytest
from pytest import raises
from structure.pdb_utils.xml_reader import XmlReader

def test_parse_general_data_with_non_existent_file(test_data_folder):
    """
    Reader should raise IOError when a non existent file is given
    to parse_general_data.
    """
    reader = XmlReader()
    structure_xml_file = join(test_data_folder, 'no_existent_pdb.xml')
    with raises(IOError) as error:
        reader.parse_general_data(structure_xml_file)
    assert "IOError" in str(error)

def test_parse_general_data(test_data_folder):
    """
    Details retrieved with parse_general_data should contain these values for
    the example file test_data/pdb.xml
    """
    reader = XmlReader()
    structure_xml_file = join(test_data_folder, 'pdb.xml')
    details = reader.parse_general_data(structure_xml_file)
    assert len(details) == 6
    assert details["experimental_method"] == 'method'
    assert details["number_entities"] == 10
    assert details["pmid"] == '000001'
    assert details["resolution"] == '1.00'
    assert details["status"] == 'CURRENT'
    assert details["title"] == 'This is the title'

def test_parse_general_data_missing_values(test_data_folder):
    """
    Details should have default values for missing 'title', 'pubmedId',
    'expMethod', 'resolution', 'nr_entities' and 'status'  fields:
    """
    reader = XmlReader()
    structure_xml_file = join(test_data_folder, 'pdb.missing.xml')
    details = reader.parse_general_data(structure_xml_file)
    assert len(details) == 6
    assert details["experimental_method"] == ''
    assert details["number_entities"] == 0
    assert details["pmid"] is None
    assert details["resolution"] == ''
    assert details["title"] == ''
    assert details["status"] == 'OBSOLETE'

def test_find_chain_details(test_data_folder):
    #pylint: disable=too-many-statements
    """
    Tests values of chain_datails, protein_info and entity_mapping after
    parsing a sample xml molecule file.
    """
    reader = XmlReader()
    mol_xml_file = join(test_data_folder, 'pdb.molecule.xml')
    chain_details,\
    entity_mapping,\
    protein_info = reader.find_chain_details(mol_xml_file,
                                             'PDB1',
                                             test_data_folder)

    # chain_details should contain 2 chains
    assert len(chain_details) == 2
    # chain_details should contain chains 'PDB1.X' and 'PDB1.Y'
    assert  all(c in chain_details for c in ['PDB1.X', 'PDB1.Y'])
    # Accession for 'PDB1.X' should be "P00001" and for 'PDB1.Y' should be "Q00001"
    assert chain_details['PDB1.X']['accession'] == "PX0001"
    assert chain_details['PDB1.Y']['accession'] == "QX0001"
    # Accession for 'PDB1.X' in chain_details should these values
    assert chain_details['PDB1.X']['end'] == 100
    assert chain_details['PDB1.X']['entityNr'] == 1
    assert chain_details['PDB1.X']['fragment_description'] == 'Unknown'
    assert chain_details['PDB1.X']['mean_intramolecular_interactions'] == -1
    assert chain_details['PDB1.X']['mean_surface_accessibility'] == -1
    assert chain_details['PDB1.X']['pdb_end'] == -1
    assert chain_details['PDB1.X']['pdb_start'] == -1
    assert chain_details['PDB1.X']['polymer_chains'] == ['X']
    assert chain_details['PDB1.X']['polymer_description'] == 'protein1 description'
    assert chain_details['PDB1.X']['polymer_length'] == 100
    assert chain_details['PDB1.X']['polymer_type'] == 'protein'
    assert chain_details['PDB1.X']['start'] == 1
    assert chain_details['PDB1.X']['taxon_id'] == '0001'
    assert chain_details['PDB1.X']['taxon_name'] == 'organism1'

    # Accession for 'PDB1.Y' in chain_details should these values
    assert chain_details['PDB1.Y']['end'] == 200
    assert chain_details['PDB1.Y']['entityNr'] == 2
    assert chain_details['PDB1.Y']['fragment_description'] == 'A fragment of protein2'
    assert chain_details['PDB1.Y']['mean_intramolecular_interactions'] == -1
    assert chain_details['PDB1.Y']['mean_surface_accessibility'] == -1
    assert chain_details['PDB1.Y']['pdb_end'] == -1
    assert chain_details['PDB1.Y']['pdb_start'] == -1
    assert chain_details['PDB1.Y']['polymer_chains'] == ['Y']
    assert chain_details['PDB1.Y']['polymer_description'] == 'protein2 description'
    assert chain_details['PDB1.Y']['polymer_length'] == 200
    assert chain_details['PDB1.Y']['polymer_type'] == 'protein'
    assert chain_details['PDB1.Y']['start'] == 101
    assert chain_details['PDB1.Y']['taxon_id'] == '0001'
    assert chain_details['PDB1.Y']['taxon_name'] == 'organism1'

    # protein_info should have two keys:
    assert len(protein_info) == 2
    # protein_info should contain proteins 'PX0001' and 'QX0001'
    assert all(c in chain_details for c in ['PDB1.X', 'PDB1.Y'])
    # protein_info should contain these values for 'PX0001'
    assert len(protein_info['PX0001']['GO']['C']) == 2
    assert protein_info['PX0001']['GO']['C'][0]['class'] == 'C'
    assert protein_info['PX0001']['GO']['C'][0]['evidence'] == 'ECO:0000001'
    assert protein_info['PX0001']['GO']['C'][0]['id'] == 'GO:00000001'
    assert protein_info['PX0001']['GO']['C'][0]['project'] == 'project1'
    assert protein_info['PX0001']['GO']['C'][0]['term'] == 'cellular_location_1'
    assert protein_info['PX0001']['GO']['C'][1]['class'] == 'C'
    assert protein_info['PX0001']['GO']['C'][1]['evidence'] == 'ECO:0000001'
    assert protein_info['PX0001']['GO']['C'][1]['id'] == 'GO:00000002'
    assert protein_info['PX0001']['GO']['C'][1]['project'] == 'project2'
    assert protein_info['PX0001']['GO']['C'][1]['term'] == 'cellular_location_2'
    assert protein_info['PX0001']['GeneTree'] == 'ENSGT00000000000001'
    assert len(protein_info['PX0001']['PDB']) == 2
    assert protein_info['PX0001']['PDB']['PDB1']['X']['start'] == 1
    assert protein_info['PX0001']['PDB']['PDB1']['X']['end'] == 100
    assert protein_info['PX0001']['PDB']['PDB2']['Y']['start'] == 101
    assert protein_info['PX0001']['PDB']['PDB2']['Y']['end'] == 200
    assert len(protein_info['PX0001']['Pfam']) == 1
    assert protein_info['PX0001']['Pfam'][0] == 'PF00001'
    assert protein_info['PX0001']['accession'] == 'PX0001'
    assert protein_info['PX0001']['description'] == 'Function description'
    assert protein_info['PX0001']['family'] == 'XXX family'
    assert protein_info['PX0001']['gene_name'] == 'Prot1'
    assert protein_info['PX0001']['id'] == 'FAKEPX_ID'
    assert not protein_info['PX0001']['isoform']
    assert protein_info['PX0001']['protein_name'] == 'Protein1'
    assert protein_info['PX0001']['secondary_accessions'] == ['PX0001', 'QX0001']
    assert protein_info['PX0001']['sequence'] == 'QWERTYIPASQWERTYIPASQWERTYIPASQWERTYIPASQWERT' + \
                                                 'YIPASQWERTYIPASQWERTYIPASQWERTYIPASQWERTYIPAS' + \
                                                 'QWERTYIPAS'
    assert protein_info['PX0001']['species_common'] == 'Human'
    assert protein_info['PX0001']['species_scientific'] == 'Homo sapiens'
    assert protein_info['PX0001']['taxon_id'] == '9606'
    assert protein_info['PX0001']['taxonomy'] == 'TaxaA|TaxaB|TaxaC'
    assert protein_info['PX0001']['version'] == '201'

    # protein_info should contain these values for 'QX0001'
    assert len(protein_info['QX0001']['GO']['C']) == 2
    assert protein_info['QX0001']['GO']['C'][0]['class'] == 'C'
    assert protein_info['QX0001']['GO']['C'][0]['evidence'] == 'ECO:0000001'
    assert protein_info['QX0001']['GO']['C'][0]['id'] == 'GO:00000001'
    assert protein_info['QX0001']['GO']['C'][0]['project'] == 'project1'
    assert protein_info['QX0001']['GO']['C'][0]['term'] == 'cellular_location_1'
    assert protein_info['QX0001']['GO']['C'][1]['class'] == 'C'
    assert protein_info['QX0001']['GO']['C'][1]['evidence'] == 'ECO:0000001'
    assert protein_info['QX0001']['GO']['C'][1]['id'] == 'GO:00000002'
    assert protein_info['QX0001']['GO']['C'][1]['project'] == 'project2'
    assert protein_info['QX0001']['GO']['C'][1]['term'] == 'cellular_location_2'
    assert protein_info['QX0001']['GeneTree'] == 'ENSGT00000000000001'
    assert len(protein_info['QX0001']['PDB']) == 2
    assert protein_info['QX0001']['PDB']['PDB1']['Y']['start'] == 101
    assert protein_info['QX0001']['PDB']['PDB1']['Y']['end'] == 200
    assert protein_info['QX0001']['PDB']['PDB4']['Z']['start'] == 101
    assert protein_info['QX0001']['PDB']['PDB4']['Z']['end'] == 200
    assert len(protein_info['QX0001']['Pfam']) == 1
    assert protein_info['QX0001']['Pfam'][0] == 'PF00001'
    assert protein_info['QX0001']['accession'] == 'QX0001'
    assert protein_info['QX0001']['description'] == 'Function description'
    assert protein_info['QX0001']['family'] == 'YYY family'
    assert protein_info['QX0001']['gene_name'] == 'Prot2'
    assert protein_info['QX0001']['id'] == 'FAKEQX_ID'
    assert not protein_info['QX0001']['isoform']
    assert protein_info['QX0001']['protein_name'] == 'Protein2'
    assert protein_info['QX0001']['secondary_accessions'] == ['QX0001', 'PX0001']
    assert protein_info['QX0001']['sequence'] == 'QWERTYIPASQWERTYIPASQWERTYIPASQWERTYIPASQWER' + \
                                                 'TYIPASQWERTYIPASQWERTYIPASQWERTYIPASQWERTYIP' + \
                                                 'ASQWERTYIPAS'
    assert protein_info['QX0001']['species_common'] == 'Human'
    assert protein_info['QX0001']['species_scientific'] == 'Homo sapiens'
    assert protein_info['QX0001']['taxon_id'] == '9606'
    assert protein_info['QX0001']['taxonomy'] == 'TaxaA|TaxaB|TaxaC'
    assert protein_info['QX0001']['version'] == '201'

    # entity_mapping should contain two elements
    assert len(entity_mapping) == 2
    # entity_mapping should contain 'forward' and 'reverse' keys
    assert all([k in entity_mapping for k in ['forward', 'reverse']])
    # entity_mapping should contain these values:
    assert entity_mapping['forward'][1][0] == 'PDB1.X'
    assert entity_mapping['forward'][2][0] == 'PDB1.Y'
    assert entity_mapping['reverse']['PDB1.X'] == 1
    assert entity_mapping['reverse']['PDB1.Y'] == 2
