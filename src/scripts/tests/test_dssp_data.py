from pytest import raises
from structure.dssp.dssp_data import DsspDataPdb
from structure.dssp.dssp_data import DsspPdbSummaryChain
from structure.dssp.dssp_data import DsspPdbChain

def test_dssp_pdb_summary_chain():
    summary = DsspPdbSummaryChain(1, 2, 3, 4, 5, 6, 7)
    assert summary.sequence == 1
    assert summary.secondary_structure == 2
    assert summary.surface_accessibility == 3
    assert summary.surface_accessibility_normalised == 4
    assert summary.mean_surface_accessibility == 5
    assert summary.buried_residues == 6
    assert summary.accessible_residues == 7

    summ_dict = summary.to_dict()

    assert summ_dict["sequence"] == 1
    assert summ_dict["secondary_structure"] == 2
    assert summ_dict["surface_accessibility"] == 3
    assert summ_dict["surface_accessibility_normalised"] == 4
    assert summ_dict["mean_surface_accessibility"] == 5
    assert summ_dict["buried_residues"] == 6
    assert summ_dict["accessible_residues"] == 7

def test_dssp_pdb_chain():
    chain = DsspPdbChain('XXX1.A')
    # a newly created DsspPdbChain should have zero length a have None summary
    # DsspPdbChain should implement __len__ method.
    assert len(chain) == 0 # pylint: disable=len-as-condition
    assert chain.pdb_chain == 'XXX1.A'
    assert chain.get_summary() is None

    chain.add_residue(5, {'data': 1, 'name': 'res_five'})
    chain.add_residue(6, {'data': 2, 'name': 'res_six'})

    # len of chain should be two, after adding two elements
    assert len(chain) == 2
    assert 5 in chain
    assert 6 in chain
    assert 4 not in chain
    assert chain.get_by_name(5, 'data') == 1
    assert chain.get_by_name(6, 'name') == 'res_six'
    assert chain.get(5) == {'name': 'res_five', 'data': 1}

    # get method should raise a KeyError for a key not in chain
    with raises(KeyError) as e:
        chain.get(7)
    assert 'KeyError' in str(e)

    # get_summary should retrieve a previously added summary
    chain.add_summary('A summary')
    assert chain.get_summary() == 'A summary'

def test_dssp_data_pdb():
    d = DsspDataPdb()
    d.add_chain("XXX1.A", "value1")
    d.add_chain("XXX1.B", "value2")
    d.add_chain("XXX1.C", "value3")

    # a DsspDataPdb is an interator
    a = [x for x in d]
    b = [x for x in d]
    assert  all(x in a for x in ['value1', 'value2', 'value3'])
    assert len(a) == 3
    assert  all(x in b for x in ['value1', 'value2', 'value3'])
    assert len(b) == 3

    # a DsspDataPdb implements __contains__
    assert "XXX1.A" in d
    assert "XXX1.B" in d
    assert "XXX1.C" in d
