import os,inspect

from ConfigParser import SafeConfigParser

def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False
        
def load_configeration_options(sections=[]):
    options = {}
    config = SafeConfigParser()
    
    config_path = os.path.abspath(os.path.join(os.path.dirname(inspect.stack()[0][1]),'./config.ini'))
    if os.path.exists(config_path):
        config.read(os.path.join(os.path.dirname(inspect.stack()[0][1]),'./config.ini'))
        
        for each_section in config.sections():
            if len(sections) == 0 or each_section in sections:
                for (each_key, each_val) in config.items(each_section):
                    each_val = each_val.strip()
                    if each_val == "None" or each_val == "none":
                        options[each_key] = None
                    elif each_val == "True" or each_val == "true":
                        options[each_key] = True
                    elif each_val == "False" or each_val == "false":
                        options[each_key] = False
                    elif isfloat(each_val):
                        if str(each_val).count(".") > 0 or str(each_val).count("e") > 0:
                            try:
                                options[each_key] =  float(each_val)
                            except:
                                options[each_key] =  each_val
                        else:
                            options[each_key] = int(each_val)
                    else:
                        options[each_key] = each_val
        
        for option in options:
            if option[-4:] == 'path':
                if not os.path.exists(options[option]):
                    print "In config.ini - " + option+ "=" + options[option],"- path does not exist."
    else:
        print "Config File Path not found:",config_path
    
    return options
    