import inspect
import json
import os
import re
from os.path import dirname
from os.path import join
from os import listdir
import pytest
from structure.find_interfaces import findInterfaces
from structure.custom_json_encoder import CjsonEncoder
from storage.elm import ElmStorage

from pprint import pprint

@pytest.fixture(scope="module")
def elm_pdb_list():
    folder = dirname(inspect.getfile(inspect.currentframe()))
    elm_file = join(folder, "elm_pdb.txt")
    with open(elm_file) as fh:
        lines = [s.strip() for s in fh.readlines() if not s.startswith("#")]
        return lines

@pytest.fixture(scope="module")
def elm_pdb_iterations():
    def read_lines(f):
        with open(f) as fh:
            lines = [s.strip() for s in fh.readlines() if not s.startswith("#")]
            return lines
    folder = dirname(inspect.getfile(inspect.currentframe()))
    result_folder = join(folder, "results")
    pattern = re.compile("errors.iteration.(\d+)")
    result_files = {int(m.group(1)): read_lines(join(result_folder, m.group(0))) for f in listdir(result_folder) for m in [re.match(pattern, f)] if m}
    return result_files

def run_findinterfaces(pdb_id):
    findInterfacesObj = findInterfaces()

    findInterfacesObj.options = {
        'skip_large_multimers': True,
        'url_root': 'http://localhost:6543',
        'coiled_coil_helix_helix_contact_proportion_cutoff': 0.75,
        'intramolecular_cutoff': 10.0,
        'data_path': '/home/data/',
        'remake_age': 180.0,
        'disorder_domain_length': 15.0,
        'max_peptide_length': 25.0,
        'pdb_id': pdb_id,
        'max_intramolecular_interactions': 10.0,
        'chain_id': '',
        'wait_time': 0.1,
        'peptide_length': 30.0,
        'inter_chain_distance': 8,
        'blast_path': '/home/tools/blast/bin/',
        'clustalo_path': '/home/tools/clustalo/clustalo',
        'surface_accessibility_cutoff': 0.35,
        'domain_contacts_cutoff': 3.0,
        'clustalw_path': '/home/tools/clustalw/clustalw2',
        'task': 'find_motifs',
        'dssp_path': '/usr/local/bin/dssp',
        'skip_homomultimers': True,
        'resources_path': '/home/data/',
        'slimsuite_path': '/home/scripts/slimsuite/',
        'sidechain_interactions_only': 'SideToAny',
        'remake': True,
        'debug': 'results',
        'coiled_coil_helix_helix_contact_length_cutoff': 7.0,
        'user_email': 'normandavey@gmail.com'}

    motifs = findInterfacesObj.find_motifs()
    motifs_string = json.dumps(motifs, ensure_ascii=False, indent=4,
                               sort_keys=True, cls=CjsonEncoder)

    benchmark_folder = join(dirname(inspect.getfile(inspect.currentframe())), "results")
    benchmark_file = join(benchmark_folder, '{}.json'.format(pdb_id))
    with open(benchmark_file, 'w') as fh:
        fh.write(motifs_string)

def test_benchmark_elm(elm_pdb_list):
    for pdb in elm_pdb_list:
        try:
            run_findinterfaces(pdb)
        except:
            benchmark_folder = join(dirname(inspect.getfile(inspect.currentframe())), "results")
            benchmark_file = join(benchmark_folder, 'errors')
            with open(benchmark_file, "a+") as fh:
                fh.write(pdb+"\n")

def benchmark_elm_iter(pdb_list, iteration):
    for pdb in pdb_list:
        try:
            run_findinterfaces(pdb)
        except:
            benchmark_folder = join(dirname(inspect.getfile(inspect.currentframe())), "results")
            benchmark_file = join(benchmark_folder, 'errors.iteration.{}'.format(iteration))
            with open(benchmark_file, "a+") as fh:
                fh.write(pdb+"\n")

def test_benchmark_elm_iter_2(elm_pdb_iterations):
    benchmark_elm_iter(elm_pdb_iterations[1],2)

def test_benchmark_elm_iter_3(elm_pdb_iterations):
    benchmark_elm_iter(elm_pdb_iterations[2],3)

def test_benchmark_elm_iter_4(elm_pdb_iterations):
    benchmark_elm_iter(elm_pdb_iterations[3],4)

def test_benchmark_elm_iter_5(elm_pdb_iterations):
    benchmark_elm_iter(elm_pdb_iterations[4],5)

def test_benchmark_elm_iter_6(elm_pdb_iterations):
    benchmark_elm_iter(elm_pdb_iterations[5],6)

def test_benchmark_elm_iter_7(elm_pdb_iterations):
    benchmark_elm_iter(elm_pdb_iterations[6],7)

def test_benchmark_elm_iter_8(elm_pdb_iterations):
    benchmark_elm_iter(elm_pdb_iterations[7],8)
    

def retrive_find_interafaces_results(pdb):
    folder = dirname(inspect.getfile(inspect.currentframe()))
    result_folder = join(folder, "results")
    result_file = join(result_folder, "{}.json".format(pdb))
    return json.load(result_file)

def export_lists(outfile, lists):
    with open(outfile, 'w') as fh:
        fh.write("\n".join(["\t".join(l) for l in lists]))



def test_benchmark_elm_verification():
    # This benchmark requires the results of test_benchmark_elm
    folder = dirname(inspect.getfile(inspect.currentframe()))
    elm_folder = join(folder, "elm")
    elm = ElmStorage(elm_folder)
    current_elm_pdbs = elm.all_pdbs()
    instances = {p: elm.instances_from_pdb(p) for p in current_elm_pdbs}

    result_folder = join(folder, "results")
    json_file = lambda x : join(result_folder, "{}.json".format(x))
    found_interfaces = {p: json.load(open(json_file(p),'r')) for p in instances}

    # 1 - Collect PBDs with errors
    errors = {p: found_interfaces[p]["error_type"] 
              for p in found_interfaces if found_interfaces[p]["status"] == "Error"}
    errors_pdb = set(errors.keys())
    export_lists(join(folder, "errors.txt"), [[p, errors[p]] for p in errors])

    found_interfaces = {p: v for p, v in found_interfaces.items() if v["status"] == "Success"}
    # assert len(found_interfaces) + len(errors) == len(instances)

    # 2 - Collect PDBS with no results
    pdb_with_no_results = [p for p in found_interfaces if not found_interfaces[p]["data"]["motif_details"]]

    with open(join(folder,"no_results.txt"), 'w') as fh:
        for p in pdb_with_no_results:
            fh.write("{}\n".format(p))

    # 3 - Collect motifs with results but no accessions
    def expanded_uniprot_seq(seq, start, end):
        st = max(0, start-10)
        en = min(end+10, len(seq))
        return seq[st:en]
    overlap = lambda start1, end1, start2, end2: max(start1, start2) < (min(end1, end2)+1)
        
    motif_details = [(p, 
          c,
          v["accession"],
          v["uniprot_start"],
          v["uniprot_end"],
          v["uniprot_sequence"],
          expanded_uniprot_seq(v["uniprot_alignment"], v["uniprot_start"], v["uniprot_end"]))
            for p in found_interfaces
            for c, v in found_interfaces[p]['data']["motif_details"].items()]
    pprint("All motifs: {}".format(len(motif_details)))

    motif_details_not_accession = [x for x in motif_details if not x[2]]
    pprint("Motifs with no Accessions: {}".format(len(motif_details_not_accession)))

    not_accessions_pdb = {x[0] for x in motif_details_not_accession}

    with open(join(folder,"no_accession.txt"), 'w')  as fh:
        for pdb, chain, acc, st, en, seq, exp_seq in motif_details_not_accession:
            fh.write("{}\n".format(" ".join([pdb, chain, str(st), str(en), seq])))

    # 4 - For those with a result and an accession try to match to an ELM instance.    
    motif_details = [x for x in motif_details if x[2]]
    pprint("Motifs with accessions: {}".format(len(motif_details)))    

    # def pick_details_for_accession(acc, motif, detail_list):
    #     return [l for l in detail_list if l[2]==acc and l[0]==motif[0]]

    classes = {c.elm_Identifier: c for c in ElmStorage().classes()}

    joined_motif_data_unmatched = [
        (p, i.elm_identifier, i.primary_acc, i.start,  i.end, classes[i.elm_identifier].regex, c, acc, st, end, seq, exp_seq)
        for p, c, acc, st, end, seq, exp_seq in motif_details 
        for i in instances[p] 
        if not acc == i.primary_acc]
    unmatched_pdbs = {x[0] for x in joined_motif_data_unmatched}
    pprint("Motifs unmatched: {}".format(len(joined_motif_data_unmatched)))

    with open(join(folder, "unmatched_acc.txt"), 'w') as fh:
        for i, (p, id, elm_acc, elm_st, elm_end, regex, c, acc, motif_st, motif_end, seq, exp_seq) in enumerate(joined_motif_data_unmatched):
            m = "matches" if classes[id].matches(exp_seq) else "not matches"
            over = "overlaps" if overlap( elm_st, elm_end, motif_st, motif_end) else "not overlaps"
            fh.write("{:3d} {} {} -> {}/{}\n    ({} - {}) {} ({} - {})\n    {} {} {}\n\n".format(i, p, c, elm_acc, acc, elm_st, elm_end, over, motif_st, motif_end, seq, m, regex))

    joined_motif_data = [
        (p, i.elm_identifier, i.start,  i.end, classes[i.elm_identifier].regex, c, acc, st, end, seq, exp_seq) 
        for p, c, acc, st, end, seq, exp_seq in motif_details 
        for i in instances[p] 
        if acc == i.primary_acc]
    pprint("Motifs matched: {}".format(len(joined_motif_data)))        

    # PDB 4WFD is lost. No instance can be retrieved for it, nevertheless it appears in the 
    # pdbs.tsv file of ELM database.
            
    # How many motifs found are similar to elm data.
    similars = [(p, id, elm_st, elm_end, regex, c, acc, motif_st, motif_end, seq, exp_seq) 
                for p, id, elm_st, elm_end, regex, c, acc, motif_st, motif_end, seq, exp_seq in joined_motif_data
                if overlap(elm_st, elm_end, motif_st, motif_end) 
                and classes[id].matches(exp_seq)]
    similars_pdb = {x[0] for x in similars}
    pprint("Motifs similar: {}".format(len(similars)))

    with open(join(folder,"similars.txt"), 'w') as fh:
        for i,(p, id, st, end, regex, c, acc, motif_st, motif_end, seq, exp_seq) in enumerate(similars):
            is_large = "Large" if len(seq) >=50 else "Small"
            seq = seq if len(seq) <=50 else seq[:10] + "..." + seq[-10:]
            fh.write("{:3d} {} -> {} {} ({} - {}) / ({} - {}) {} {}\n".format(i, p, id, is_large, st, end, motif_st, motif_end, regex, seq))
            fh.write("\n")


    not_similars = [x for x in joined_motif_data if x not in similars]
    not_similars_pdbs = {x[0] for x in not_similars} 
    with open(join(folder,"strange.txt"), 'w') as fh:
        for i,(p, id, st, end, regex, c, acc, motif_st, motif_end, seq, exp_seq) in enumerate(not_similars):
            is_large = "Large" if len(seq) >= 50 else "Small"
            seq = seq if len(seq) <=50 else seq[:10] + "..." + seq[-10:]
            fh.write("{:3d} {} -> {} {} ({} - {}) / ({} - {}) {} {}\n".format(i, p, id, is_large, st, end, motif_st, motif_end, regex, seq))
            fh.write("\n")
    pprint("Motifs Strange: {}".format(len(not_similars)))

    pdb_summary = {
        p:{
            'instances': len(instances[p]),
            'error': p in errors_pdb,
            'no_result': p in pdb_with_no_results,
            'no_accession': p in not_accessions_pdb,
            'unmatched': p in unmatched_pdbs,
            'similar' : p in similars_pdb,
            'not_similar': p in not_similars_pdbs
        }
        for p in current_elm_pdbs
    }

    with open(join(folder,"pdb_summary.txt"), 'w') as fh:
        fh.write("# {:4s} {:12s} {:12s} {:12s} {:12s} {:12s} {:12s} {:12s}\n".format("PDB", "instances", "error", "no_result", "no_accession", "unmatched", "similar", "not_similar"))
        for p in sorted(pdb_summary.keys()):
            v = pdb_summary[p]
            fh.write("{:6s} {:12d} {:12s} {:12s} {:12s} {:12s} {:12s} {:12s}\n".format(
                p,
                v['instances'],
                "X" if (v['error']) else "",
                "X" if (v['no_result']) else "",
                "X" if (v['no_accession']) else "",
                "X" if (v['unmatched']) else "",
                "X" if (v['similar']) else "",
                "X" if (v['not_similar']) else "",
            ))

