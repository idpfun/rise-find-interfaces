  0 4CY2 4CY2.D -> Q9H9L4/Q7Z3B3
    (410 - 413) not overlaps (585 - 596)
    DGTCVAARTRPV not matches [ED].{0,3}[VIL]D[VI]

  1 4CY5 4CY5.D -> Q9VAF4/A4V2Z1
    (159 - 162) not overlaps (718 - 726)
    LCSRARPLV not matches [ED].{0,3}[VIL]D[VI]

  2 2LB1 2LB1.A -> Q15797/Q9HCE7
    (224 - 227) not overlaps (305 - 339)
    LGPLPPGWEVRSTVSGRIYFVDHNNRTTQFTDPRL not matches PP.Y

  3 2LB3 2LB3.B -> P84022/Q15796
    (176 - 181) not overlaps (217 - 224)
    IPETPPPG matches ...([ST])P.

  4 2R03 2R03.B -> P04591/P69732
    (483 - 492) not overlaps (456 - 463)
    NLYPDLSE not matches [LM]YP...[LI][^P][^P][LI]

  5 1D8D 1D8D.P -> P01116-2/P01116
    (185 - 188) overlaps (172 - 189)
    SKEEKTPGCVKIKKCIIM matches (C)[^DENQ][LIVMF].$

  6 1XH4 1XH4.B -> P00517/P61925
    (1 - 7) overlaps (6 - 25)
    TTYADFIASGRTGRRNAIHD not matches ^M{0,1}(G)[^EDRKHPFYW]..[STAGCN][^P]

  7 1SMH 1SMH.B -> P00517/P61926
    (1 - 7) overlaps (6 - 25)
    TTYADFIASGRTGRRNAIHD not matches ^M{0,1}(G)[^EDRKHPFYW]..[STAGCN][^P]

  8 2E4H 2E4H.B -> Q71U36/P68363
    (443 - 451) overlaps (444 - 451)
    GEEEGEEY matches [ED].{0,2}[ED].{0,2}[EDQ].{0,1}[YF]$

  9 3KUI 3KUI.B -> Q8IYD1/P15170
    (50 - 62) overlaps (1 - 168)
    MELSEPIVENGETEMSPEESWEHKEEISEAEPGGGSLGDGRPPEESAHEMMEEEEEIPKPKSVVAPPGAPKKEHVNVVFIGHVDAGKSTIGGQIMYLTGMVDKRTLEKYEREAKEKNRETWYLSWALDTNQEERDKGKTVEVGRAYFETEKKHFTILDAPGHKSFVPN not matches ..[LFP][NS][PIVTAFL].A..(([FY].[PYLF])|(W..)).

 10 2C1A 2C1A.I -> P00517/P61925
    (1 - 7) overlaps (6 - 24)
    TTYADFIASGRTGRRNAIH not matches ^M{0,1}(G)[^EDRKHPFYW]..[STAGCN][^P]

 11 1ZW2 1ZW2.B -> P26039/P54939
    (2345 - 2363) overlaps (2344 - 2364)
    ILEAAKSIAAATSALVKAASA matches [VMIL][MILFYPA][^P][TASKHC][AVSC][^P][^P][ILVM][^P][^P][^P][LMTVI][^P][^P][LMV][ILVMA][^P][^P][AIVLMT]

 12 2VJ0 2VJ0.P -> O08838/O43426
    (324 - 328) not overlaps (1478 - 1485)
    PKGWVTFE not matches F.D.F

 13 3EMW 3EMW.B -> Q96IZ0/P09052
    (68 - 73) not overlaps (184 - 190)
    DINNNNN matches [ED][LIV]NNN[^P]

 14 1ATP 1ATP.I -> P05132/P63248
    (348 - 351) not overlaps (6 - 25)
    TTYADFIASGRTGRRNAIHD not matches F..F$

 15 2VNW 2VNW.I -> P00517/P61925
    (1 - 7) overlaps (6 - 25)
    TTYADFIASGRTGRRNAIHD not matches ^M{0,1}(G)[^EDRKHPFYW]..[STAGCN][^P]

 16 1VEB 1VEB.B -> P00517/P61925
    (1 - 7) overlaps (6 - 25)
    TTYADFIASGRTGRRNAIHD not matches ^M{0,1}(G)[^EDRKHPFYW]..[STAGCN][^P]

 17 1G1E 1G1E.A -> Q14582/Q05195
    (6 - 16) overlaps (6 - 21)
    RMNIQMLLEAADYLER matches [LIV]..[LM]L.AA.[FY][LI]

 18 1LF8 1LF8.H -> P08169/P11717
    (2493 - 2499) not overlaps (2484 - 2491)
    SDEDLLHI matches D..LL.{1,2}$

 19 1LF8 1LF8.E -> P08169/P11717
    (2493 - 2499) not overlaps (2484 - 2491)
    SDEDLLHI matches D..LL.{1,2}$

 20 1LF8 1LF8.G -> P08169/P11717
    (2493 - 2499) not overlaps (2484 - 2491)
    SDEDLLHI matches D..LL.{1,2}$

 21 1LF8 1LF8.F -> P08169/P11717
    (2493 - 2499) not overlaps (2484 - 2491)
    SDEDLLHI matches D..LL.{1,2}$

 22 1UKH 1UKH.B -> Q9UQF2/Q9WVI9
    (160 - 166) overlaps (154 - 163)
    PKRPTTLNLF matches [RK]P[^P][^P]L.[LIVMF]

 23 2PG1 2PG1.J -> Q13409/Q62871
    (156 - 162) overlaps (136 - 164)
    IKLGMAKITQVDFPPREIVTYTKETQTPV matches [^P].[KR].TQT

 24 2PG1 2PG1.K -> Q13409/Q62871
    (156 - 162) overlaps (136 - 164)
    IKLGMAKITQVDFPPREIVTYTKETQTPV matches [^P].[KR].TQT

 25 2PG1 2PG1.I -> Q13409/Q62871
    (156 - 162) overlaps (137 - 163)
    KLGMAKITQVDFPPREIVTYTKETQTP matches [^P].[KR].TQT

 26 2PG1 2PG1.L -> Q13409/Q62871
    (156 - 162) overlaps (136 - 164)
    IKLGMAKITQVDFPPREIVTYTKETQTPV matches [^P].[KR].TQT

 27 3GJO 3GJO.D -> Q03001/Q15691
    (7545 - 7558) not overlaps (192 - 249)
    EAAELMQQVNVLKLTVEDLEKERDFYFGKLRNIELICQENEGENDPVLQRIVDILYAT not matches ([KR][^ED]{0,5}[ST].IP[^ED]{5,5})|([^ED]{5,5}[ST].IP[^ED]{0,5}[KR])

 28 3GJO 3GJO.C -> Q03001/Q15691
    (7545 - 7558) not overlaps (192 - 256)
    EAAELMQQVNVLKLTVEDLEKERDFYFGKLRNIELICQENEGENDPVLQRIVDILYATDEGFVIP not matches ([KR][^ED]{0,5}[ST].IP[^ED]{5,5})|([^ED]{5,5}[ST].IP[^ED]{0,5}[KR])

 29 3GJO 3GJO.B -> Q03001/Q15691
    (7545 - 7558) not overlaps (191 - 256)
    DEAAELMQQVNVLKLTVEDLEKERDFYFGKLRNIELICQENEGENDPVLQRIVDILYATDEGFVIP not matches ([KR][^ED]{0,5}[ST].IP[^ED]{5,5})|([^ED]{5,5}[ST].IP[^ED]{0,5}[KR])

 30 3GJO 3GJO.A -> Q03001/Q15691
    (7545 - 7558) not overlaps (192 - 257)
    EAAELMQQVNVLKLTVEDLEKERDFYFGKLRNIELICQENEGENDPVLQRIVDILYATDEGFVIPD not matches ([KR][^ED]{0,5}[ST].IP[^ED]{5,5})|([^ED]{5,5}[ST].IP[^ED]{0,5}[KR])

 31 1PD7 1PD7.B -> Q14582/Q05195
    (6 - 16) overlaps (5 - 28)
    VRMNIQMLLEAADYLERREREAEH matches [LIV]..[LM]L.AA.[FY][LI]

 32 2R02 2R02.B -> P03347/Q9WC62
    (495 - 504) not overlaps (479 - 489)
    ELYPLTSLRSL matches [LM]YP...[LI][^P][^P][LI]

 33 1QJB 1QJB.S -> P03076/P0DOJ8
    (254 - 259) overlaps (254 - 260)
    RSHSYPP matches R[^DE]{0,2}[^DEPG]([ST])(([FWYLMV].)|([^PRIKGN]P)|([^PRIKGN].{2,4}[VILMFWYP]))

 34 1QJB 1QJB.Q -> P03076/P0DOJ8
    (254 - 259) overlaps (253 - 260)
    MRSHSYPP matches R[^DE]{0,2}[^DEPG]([ST])(([FWYLMV].)|([^PRIKGN]P)|([^PRIKGN].{2,4}[VILMFWYP]))

 35 4U4C 4U4C.B -> Q12476/P53632
    (43 - 47) not overlaps (66 - 583)
    TADGILVLEHKSDDDEGFDVYDGHFDNPTDIPSTTEESKTPSLAVHGDEKDLANNDDFISLSASSEDEQAEQEEEREKQELEIKKEKQKEILNTDYPWILNHDHSKQKEISDWLTFEIKDFVAYISPSREEIEIRNQTISTIREAVKQLWPDADLHVFGSYSTDLYLPGSDIDCVVTSELGGKESRNNLYSLASHLKKKNLATEVEVVAKARVPIIKFVEPHSGIHIDVSFERTNGIEAAKLIREWLDDTPGLRELVLIVKQFLHARRLNNVHTGGLGGFSIICLVFSFLHMHPRIITNEIDPKDNLGVLLIEFFELYGKNFGYDDVALGSSDGYPVYFPKSTWSAIQPIKNPFSLAIQDPGDESNNISRGSFNIRDIKKAFAGAFDLLTNRCFELHSATFKDRLGKSILGNVIKYRGKARDFKDERGLVLNKAIIENENYHKKRSRIIHDEDFAEDTVTSTATATTTDDDYEITNPPAKKAKIEEKPESEPAKRNSGETYITVSSEDDDEDGYNPYT not matches GRYFG

 36 2ZJD 2ZJD.B -> Q13501/Q64337
    (335 - 341) overlaps (335 - 344)
    GGDDDWTHLS matches [EDST].{0,2}[WFY]..[ILV]

 37 2ZJD 2ZJD.D -> Q13501/Q64337
    (335 - 341) overlaps (338 - 344)
    DDWTHLS matches [EDST].{0,2}[WFY]..[ILV]

 38 2HSQ 2HSQ.B -> P18010/Q6XVZ2
    (566 - 584) overlaps (565 - 587)
    AIYEKAKEVSSALSKVLSKIDDT matches [VMIL][MILFYPA][^P][TASKHC][AVSC][^P][^P][ILVM][^P][^P][^P][LMTVI][^P][^P][LMV][ILVMA][^P][^P][AIVLMT]

 39 1Q8W 1Q8W.B -> P00517/P61926
    (1 - 7) overlaps (6 - 25)
    TTYADFIASGRTGRRNAIHD not matches ^M{0,1}(G)[^EDRKHPFYW]..[STAGCN][^P]

 40 1PQ1 1PQ1.B -> O54918-2/O54918
    (86 - 102) not overlaps (139 - 171)
    DLRPEIRIAQELRRIGDEFNETYTRRVFANDYR matches ....[LIFVYMTE][ASGC][^P]{2}L[^P]{2}[IVMTL][GACS][D][^P][FVLMI].

 41 2LNH 2LNH.C -> C6UYI3/P0DJ89
    (224 - 232) overlaps (221 - 267)
    LPDVAQRLMQHLAEHGIQPARNMAEHIPPAPNWPAPTPPVQNEQSRP matches [ILV][VA][^P][^P][LI][^P][^P][^P][LM]

 42 2LNH 2LNH.C -> C6UYI3/P0DJ89
    (271 - 279) not overlaps (221 - 267)
    LPDVAQRLMQHLAEHGIQPARNMAEHIPPAPNWPAPTPPVQNEQSRP matches [ILV][VA][^P][^P][LI][^P][^P][^P][LM]

 43 3E1R 3E1R.A -> Q8WUM4/Q53EZ4
    (797 - 807) not overlaps (165 - 210)
    NNIHEMEIQLKDALEKNQQWLVYDQQREVYVKGLLAKIFELEKKTE not matches .A.GPP.{2,3}Y.

 44 3E1R 3E1R.B -> Q8WUM4/Q53EZ4
    (797 - 807) not overlaps (167 - 208)
    IHEMEIQLKDALEKNQQWLVYDQQREVYVKGLLAKIFELEKK not matches .A.GPP.{2,3}Y.

 45 2FO1 2FO1.D -> P14585/Q09260
    (944 - 947) not overlaps (52 - 114)
    EDEPTIGDLNAFHSGEELHRQRSELARANYEKARPEMIANQRAVTAHLFNRYTEDEERKRVEQ not matches [AFILMPTVW]W[FHILMPSTVW]P

 46 2K42 2K42.B -> C6UYI3/A0A0H3JD26
    (224 - 232) not overlaps (35 - 105)
    QHLAEHGIQPARNMAEHIPPAPNWPAPTPPVQNEQSRPLPDVAQRLMQHLAEHGIQPARNMAEHIPPAPNW matches [ILV][VA][^P][^P][LI][^P][^P][^P][LM]

 47 2K42 2K42.B -> C6UYI3/A0A0H3JD26
    (271 - 279) not overlaps (35 - 105)
    QHLAEHGIQPARNMAEHIPPAPNWPAPTPPVQNEQSRPLPDVAQRLMQHLAEHGIQPARNMAEHIPPAPNW matches [ILV][VA][^P][^P][LI][^P][^P][^P][LM]

 48 1O6K 1O6K.C -> Q9WV60/P49841
    (4 - 12) overlaps (3 - 12)
    GRPRTTSFAE matches R.R..([ST])[^P]..

 49 1O6L 1O6L.C -> P31751/P49841
    (470 - 475) not overlaps (3 - 12)
    GRPRTTSFAE not matches F..[FWY][ST][FY]

 50 1YDR 1YDR.I -> P00517/P61925
    (1 - 7) overlaps (6 - 25)
    TTYADFIASGRTGRRNAIHD not matches ^M{0,1}(G)[^EDRKHPFYW]..[STAGCN][^P]

 51 1M4P 1M4P.B -> P04591/Q9IF21
    (454 - 459) not overlaps (27 - 35)
    PEPTAPPEE matches .P[TS]AP.

 52 1Q24 1Q24.I -> P00517/Q71U53
    (1 - 7) overlaps (6 - 25)
    TTYADFIASGRTGRRNAIHD not matches ^M{0,1}(G)[^EDRKHPFYW]..[STAGCN][^P]

 53 2G2L 2G2L.D -> P19490/M0R9A7
    (902 - 907) not overlaps (812 - 812)
    L matches ...[ST].[ACVILF]$

 54 2G2L 2G2L.C -> P19490/M0R9A7
    (902 - 907) not overlaps (809 - 812)
    ATGL matches ...[ST].[ACVILF]$

 55 5JEM 5JEM.C -> Q14653/Q92793
    (391 - 396) not overlaps (2065 - 2106)
    SALQDLLRTLKSPSSPQQQQQVLNILKSNPQLMAAFIKQRTA not matches ..L.I(S)

 56 5JEM 5JEM.H -> Q14653/Q92793
    (391 - 396) not overlaps (2065 - 2106)
    SALQDLLRTLKSPSSPQQQQQVLNILKSNPQLMAAFIKQRTA not matches ..L.I(S)

 57 5JEM 5JEM.F -> Q14653/Q92793
    (391 - 396) not overlaps (2065 - 2106)
    SALQDLLRTLKSPSSPQQQQQVLNILKSNPQLMAAFIKQRTA not matches ..L.I(S)

 58 5JEM 5JEM.D -> Q14653/Q92793
    (391 - 396) not overlaps (2065 - 2106)
    SALQDLLRTLKSPSSPQQQQQVLNILKSNPQLMAAFIKQRTA not matches ..L.I(S)

 59 2F3Y 2F3Y.B -> Q13936/Q810C8
    (1666 - 1685) not overlaps (141 - 158)
    KFYATFLIQEYFRKFKKR matches [ACLIVTM][^P][^P][ILVMFCT]Q[^P][^P][^P][RK][^P]{4,5}[RKQ][^P][^P]

 60 1ELR 1ELR.B -> P07900/Q9H2A1
    (729 - 732) not overlaps (1 - 5)
    MEEVD not matches EEVD$

