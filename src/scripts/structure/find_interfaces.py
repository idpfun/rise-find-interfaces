import os
from os.path import join
import time
import json
import pprint
import sys
import optparse
import copy
import traceback
import inspect
import re
import itertools
import xml
from xml.etree import cElementTree as elementtree
from xml.etree import cElementTree

import Bio.PDB
from Bio.SubsMat import MatrixInfo as matlist

from structure.dssp.parser import Parser
from structure.pdb_utils.xml_reader import XmlReader
from structure.pdb_utils.structureDataGetter import StructureDataGetter
from structure.pdb_utils.construct_sequence_builder import PDBSeqBuilder
from structure.pdb_utils.alignment_mapper import AlignmentMapper
from structure.pdb_utils.interactions import InteractionFinder
from structure.dssp.secondary_structure import DsspSecondaryStructure
from structure.interfaces import ProteinChainsContactSelector
from structure.interfaces import HybridChainsContactSelector
from structure.module_assigner import ModuleAssigner
from structure.iupac.amino_acids import AminoAcids
from structure.custom_json_encoder import CjsonEncoder
from downloaders import pdbDownloader
from downloaders import uniprotDownloader
from structure.dssp.dssp import Dssp
import config_reader

__example__ = """
python find_interfaces.py --pdb_id --debug True --remake True
python find_interfaces.py --pdb_id --debug False --remake True
"""

matrix = matlist.blosum62
gap_open = -10
gap_extend = -0.5

def del_bio_entity(entity):
    try:
        for child in entity:
            del_bio_entity(child)
    except TypeError:
        pass

    if entity != None:
        entity.detach_parent()

class findInterfaces():
    # pylint: disable=too-many-public-methods
    def __init__(self):

        self.options = {}
        self.start_time = 0

        # TODO correct typo
        config_options = config_reader.load_configeration_options(
            sections=["general", "structure_reader"])

        for config_option in config_options:
            self.options[config_option] = config_options[config_option]

        self.settings = {}
        self.settings["script_path"] = os.path.dirname(inspect.stack()[0][1])
        # Deprecated, use AminoAcid singleton class instead.
        with open(os.path.join(self.settings["script_path"], "components_iupac.reduced.json")) as outfile:
            self.settings["aa_codes"] = json.load(outfile)

        # ASK : hardcoded antigen_presenting and antigen_binding
        self.settings["antigen_presenting"] = ["PF11624", "PF00129", "PF14586", 
                                               "PF16497", "PF00993", "PF00969"]
        self.settings["antigen_binding"] = ["PF07686"]

        # Collapsed pfams are collection of pfam domains that fall into
        # another bigger pfam domain.
        self.settings["collapse_pfam"] = self.load_collapsed_pfams()
        self.json = {}
        self.data = {}
        self.paths = {}
        self.structure = {}
        self.init_data()


    def init_data(self):
        self.data["structure_details"] = {}
        self.data["construct_sequences"] = {}
        self.data["construct_pdb_offets"] = {}
        self.data["construct_sequences_offsets"] = {}
        self.data["protein_info"] = {}
        self.data["domain_info"] = {"all": [], "names": {}}
        self.data["entity_mapping"] = {"forward": {}, "reverse": {}}
        self.data["chain_details"] = {}
        self.data["interactors"] = {}
        self.data["interfaces"] = {}
        self.data["contacts"] = {}
        self.data["residue_mapping"] = {}
        self.data["dssp"] = {}
        self.data["intramolecular"] = {}
        self.data["mean_surface_accessibilty"] = {}
        self.data["module_type"] = {}
        self.data["coiled coil"] = {}
        self.data["modifications"] = {}
        self.data["construct_aligned"] = {}

        # ASK: Hard coded domains???
        self.data["domain_info"]["names"] = {
            "PF00515": "Tetratricopeptide repeat (TPR)",
            "PF00514": "Armadillo/beta-catenin-like repeat",
            "PF00595": "PDZ",
            "PF02172": "KIX domain",
            "PF00400": "WD40 repeat",
            "PF00023": "Ankyrin repeat",
            "PF00036": "EF hand",
            "PF00076": "RNA recognition motif (RRM)",
            "PF00560": "Leucine Rich Repeat (LRR)"
        }

        self.structure = None
        self.uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()

    def load_collapsed_pfams(self):
        """
        Creates a dictionary to represent pfam collapsed accessions.
            :param self:
        """
        collapsed = {}
        pfam_file = os.path.join(self.settings["script_path"], "pfam_compression.tdt")
        with open(pfam_file, 'r') as fh:
            for line in fh:
                fields = line.strip().split('\t')
                if len(fields) == 2:
                    collapsed[fields[0]] = fields[1]
        return collapsed

    def setup_paths(self):
        """
        Creates the folder structure for output and intermediate files.
            :param self:
        """
        data_path = self.options["data_path"]
        pdb_id = self.options["pdb_id"]
        self.paths.update({
            "interfaces": os.path.join(data_path, "interfaces"),
            "structures": os.path.join(data_path, "pdb"),
            "intramolecular": os.path.join(data_path, "pdb", "intramolecular"),
            "structure_status": os.path.join(data_path, "pdb", pdb_id + ".status.json"),
            "structure_pdb": os.path.join(data_path, "pdb", pdb_id + ".pdb"),
            "structure_xml": os.path.join(data_path, "pdb", pdb_id + ".xml"),
            "structure_pfam_xml": os.path.join(data_path, "pdb", pdb_id + ".pfam.xml"),
            "structure_molecule_xml": os.path.join(data_path, "pdb", pdb_id + ".molecule.xml"),
            "structure_dssp": os.path.join(data_path, "pdb", pdb_id + ".dssp")
        })

        for path in ["interfaces", "structures", "intramolecular"]:
            if not os.path.exists(self.paths[path]):
                print(path, "does not exist. Making ", self.paths[path])
                os.makedirs(self.paths[path])

        self.uniprotDownloaderObj.options["data_path"] = data_path

    def get_construct_for_mapping(self, chain):
        """
        Retrieve a sequence for a given chain of a pdb construct.
        If a sequence from the dssp summary data is available return this value,
        otherwise returns the sequence from construct_sequences data.
            :param self:
            :param chain:
        """
        construct = self.data['construct_sequences'][chain]
        if chain in self.data["dssp"]:
            construct = self.data["dssp"].chain(chain).get_summary().sequence
        return construct.upper().replace("-", "")

    def get_protein_for_mapping(self, chain, default):
        """
        Retrieve a sequence for a given chain of a protein from uniprot.
        If the protein sequence is equals to an empty string, then returns a
        default sequence given as argument.
            :param self:
            :param chain: a chain_id
            :param default: a str
        """
        accession = self.data['chain_details'][chain]['accession']
        protein = self.data["protein_info"][accession]['sequence']
        protein = protein.upper().replace("-", "")
        return protein if not protein == "" else default

    def is_protein(self, chain):
        chain_details = self.data['chain_details']
        return chain in chain_details and chain_details[chain]['polymer_type'] == "protein"

    def map_construct_sequences(self):
        mapper = AlignmentMapper()
        for chain in self.data['construct_sequences']:
            if self.is_protein(chain):
                accession = self.data['chain_details'][chain]['accession']
                construct = self.get_construct_for_mapping(chain)
                protein = self.get_protein_for_mapping(chain, construct)
                pdb_residues = self.data["construct_sequences_offsets"][chain]
                construct_mapping = True
                alignment_result = mapper.map(accession, construct, protein, pdb_residues, construct_mapping)
                self.data['chain_details'][chain]["pdb_alignment"] = alignment_result["pdb_alignment"]
                self.data['chain_details'][chain]["uniprot_alignment"] = alignment_result["uniprot_alignment"]
                self.data["construct_aligned"][chain] = alignment_result["is_construct_aligned"]
                self.data["residue_mapping"][chain] = alignment_result["residue_mapping"]

    def calculate_intramolecular_interactions(self):
        """
        Estimates the number of intramolecular interactions in every pdb chain.
            :param self:
        """
        for chain in self.structure[0].child_list:
            full_chain_id = self.options["pdb_id"] + "." + chain.id
            intramolecular_file = os.path.join(self.options["data_path"], "pdb", "intramolecular/", full_chain_id + '.txt')
            ifinder = InteractionFinder(intramolecular_file)
            intramolecular = ifinder.intramolecular_in_chain(chain, 10)
            self.data["intramolecular"][full_chain_id] = intramolecular
            if full_chain_id in self.data['chain_details']:
                self.data['chain_details'][full_chain_id]['mean_intramolecular_interactions'] = intramolecular

    def adjust_sequence_for_gaps(self):
        for chain in self.data['chain_details']:
            if self.data["chain_details"][chain]['polymer_type'] not in ['dna', 'rna', 'hybrid'] and chain in self.data["dssp"]:
                try:
                    try:
                        n_terminal_overhang = self.data["residue_mapping"][chain]['n_terminal_overhang']
                    except:
                        n_terminal_overhang = 0

                    if chain in self.data["residue_mapping"]:
                        if 'residues' in self.data["residue_mapping"][chain]:
                            if len(self.data["residue_mapping"][chain]['residues']) != 0:
                                if self.data["residue_mapping"][chain]['n_terminal_overhang'] > 0:
                                    # [self.json['chains'][chain]["start"]]['protein_offset']
                                    self.data['chain_details'][chain]["uniprot_start"] = 1
                                else:
                                    self.data['chain_details'][chain]["uniprot_start"] = self.data["residue_mapping"][chain]['residues'][min(
                                        self.data["residue_mapping"][chain]['residues'])]['protein_offset'] + 1  # [self.json['chains'][chain]["start"]]['protein_offset']

                                # self.data["residue_mapping"][chain]['residues'][self.json['chains'][chain]["end"]]['protein_offset']
                                self.data['chain_details'][chain]["uniprot_end"] = self.data["residue_mapping"][chain]['residues'][max(
                                    self.data["residue_mapping"][chain]['residues'])]['protein_offset'] + 1

                                self.data['chain_details'][chain]["uniprot_sequence"] = self.data["residue_mapping"][chain]['uniprot_alignment'][self.data['chain_details']
                                                                                                                                                 [chain]["uniprot_start"] - 1:self.data['chain_details'][chain]["uniprot_end"] + n_terminal_overhang][n_terminal_overhang:]
                                self.data['chain_details'][chain]["construct_uniprot_sequence"] = self.data[
                                    "dssp"].chain(chain).get_summary().sequence[n_terminal_overhang:]
                    else:
                        self.data['chain_details'][chain]["construct_uniprot_sequence"] = self.data["dssp"].chain(chain).get_summary().sequence

                    try:
                        self.data['chain_details'][chain]["secondary_structure"] = self.data[
                            "dssp"].chain(chain).get_summary().secondary_structure[n_terminal_overhang:]
                        self.data['chain_details'][chain]["buried_residues"] = self.data[
                            "dssp"].chain(chain).get_summary().buried_residues[n_terminal_overhang:]
                        # print("Assigned")
                        self.data['chain_details'][chain]["accessible_residues"] = self.data[
                            "dssp"].chain(chain).get_summary().accessible_residues[n_terminal_overhang:]
                        self.data['chain_details'][chain]["sa_difference"] = self.data[
                            "dssp"].chain(chain).get_summary().surface_accessibility_difference[n_terminal_overhang:]
                    except:
                        # print("Not assigned")
                        pass

                    # lengths = []

                    # for header in ["uniprot_sequence", "construct_uniprot_sequence", "secondary_structure", "buried_residues"]:
                    #     if header in self.data['chain_details'][chain]:
                    #         lengths.append(
                    #             len(self.data['chain_details'][chain][header]))
                    #         #print self.data['chain_details'][chain][header]
                    """
                    if len(set(lengths)) > 1:
                        print "ERROR - MAPPING LENGTH UNEQUAL - adjust_sequence_for_gaps",self.options["pdb_id"]
                        for header in ["uniprot_sequence","construct_uniprot_sequence","secondary_structure","buried_residues"]:
                            if header in self.data['chain_details'][chain]:
                                print self.data['chain_details'][chain][header]
                    """
                except:
                    n_terminal_overhang = 0

    def create_contact_sequence(self):
        """
        Creates a contact sequence for each chain by merging the sequence of every interface for
        that chain.
            :param self: 
        """
        interactors = self.data['interactors']
        chain_details = self.data['chain_details']
        interfaces = self.data['interfaces']

        def merge_seqs(seq_a, seq_b):
            if not seq_a:
                return seq_b
            if not seq_b:
                return seq_a
            return [b if a == "." else a for a, b in zip(seq_a, seq_b)]

        contact_positions = {chain:[] for chain in interactors}
        for interacting_chain in interfaces:
            for chain in interfaces[interacting_chain]:
                seq = interfaces[interacting_chain][chain]['interface_sequence']
                contact_positions[chain] = merge_seqs(contact_positions[chain], list(seq))

        for chain in interactors:
            chain_details[chain]['contact_residues'] = "".join(contact_positions[chain]).replace(".", "x")
            chain_details[chain]['contact_residues_positions'] = {i:v for i, v in enumerate(contact_positions[chain])}

    def parse_DSSP(self):
        pdb_id = self.options["pdb_id"]
        pdb_chains = self.data["chain_details"].keys()
        pdb_file = self.paths["structure_pdb"]
        dssp_runner = Dssp(self.options['dssp_path'])
        
        try:
            dssp_splitted, _ = dssp_runner.run(pdb_file, pdb_id, splitted=True)
            dssp_joined, _ = dssp_runner.run(pdb_file, pdb_id, splitted=False)
        except:
            sys.stderr.write("ERROR: There was a problem runnnig dssp for pdb: {}"
                             .format(pdb_file))
            self.data["dssp"] = {}
            raise
            
        dssp_diff = dssp_joined.solvent_accesibility_difference(dssp_splitted)
        self.data["dssp"] = dssp_diff
        
        for pdb_chain in pdb_chains:
            sa_normalised = dssp_diff.chain(pdb_chain).get_summary().surface_accessibility_normalised if pdb_chain in dssp_diff else {}
            mean_surface_accessibilty = sum(sa_normalised.values())/len(sa_normalised) if pdb_chain in dssp_diff else 0
            self.data["mean_surface_accessibilty"][pdb_chain] = mean_surface_accessibilty
            self.data['chain_details'][pdb_chain]['mean_surface_accessibility'] = mean_surface_accessibilty
            self.data['chain_details'][pdb_chain]['surface_accessibilty_normalised'] = sa_normalised

    def read_pfam_xml_file(self, pfam_xml_file, collapse_pfam):
        """
        Retrieves domain data from a local xml file generated from pfamScan
        program.
        Returns a tuple with domain pfam accession, names (or descriptions) of
        those that are not present in the collapsed pfam domains and a
        collection of relevant data for all domains.
            :param self:
            :param pfam_xml_file: path to a xml file with pfam data
            :param collapse_pfam: A dict with collapsed pfam domains.

        """
        pfam_root = elementtree.parse(pfam_xml_file).getroot()

        pfamHits = [p for p in pfam_root.iter('pfamHit')]
        pfamHitsAcc = [p.attrib["pfamAcc"].split(".")[0] for p in pfam_root.iter('pfamHit')]

        info_collapse = set([collapse_pfam[acc] for acc in pfamHitsAcc
            if acc in collapse_pfam])

        info_not_collapse = set([acc for acc in pfamHitsAcc
            if acc not in collapse_pfam and not acc in info_collapse])

        # ASK: why recover only the names of those that are not in the collapse
        # dict?
        names_not_collapse = {acc:hit.attrib["pfamDesc"] for (acc, hit) in zip(pfamHitsAcc, pfamHits)
            if not acc in collapse_pfam and not acc in info_collapse}

        domain_info = list(info_collapse)
        domain_info.extend(info_not_collapse)

        chain_ids = set([hit.attrib["chainId"] for hit in pfamHits])

        # ASK: Shouldn't return the collapsed accession here?
        pfams= {c:[{
            "pfamAcc": hit.attrib["pfamAcc"].split(".")[0],
            "pfamName": hit.attrib["pfamName"],
            "pfamDesc": hit.attrib["pfamDesc"],
            'pdbResNumStart': int(re.sub('[A-Z]', '', hit.attrib["pdbResNumStart"])),
            'pdbResNumEnd': int(re.sub('[A-Z]', '', hit.attrib["pdbResNumEnd"]))
        } for hit in pfamHits if c == hit.attrib["chainId"]] for c in chain_ids }

        return (domain_info, names_not_collapse, pfams)

    def load_domains(self):
        """
        Retrieves domain data from a local pfam xml file.
            :param self:
        """
        pdb_id = self.options["pdb_id"]

        # Load data from pfam file
        domain_info, names_not_collapse, pfams = self.read_pfam_xml_file(
            self.paths["structure_pfam_xml"], self.settings["collapse_pfam"])

        # Update domain_info data
        self.data["domain_info"]["all"] = domain_info
        self.data["domain_info"]["names"].update(names_not_collapse)

        for chain_id, pfam_list in pfams.items():
            pdb_chain_id = "{}.{}".format(pdb_id, chain_id)
            entity_id = self.data["entity_mapping"]["reverse"][pdb_chain_id]
            for pfam in pfam_list:
                for chain in self.data["entity_mapping"]["forward"][entity_id]:
                    if chain not in self.data["domain_info"]:
                        self.data["domain_info"][chain] = []
                    self.data["domain_info"][chain].append(pfam)

    def parse_general_data(self):
        """
        Reads data from the "structure_xml" file for current pdb.
            :param self:
        """
        structure_xml_file = self.paths["structure_xml"]
        reader = XmlReader()
        try:
            self.data["structure_details"] = reader.parse_general_data(structure_xml_file)
            return {"status": "Success"}
        except cElementTree.ParseError:
            msg = "Xml file {} is not well formed".format(self.paths["structure_xml"])
            return {"status": "Error", "error_type": msg}
            

    def find_chain_details(self):
        """
        Reads a "structure_molecule_xml" for the current pdb .
            :param self:
        """
        reader = XmlReader()
        structure_molecule_xml_file = self.paths["structure_molecule_xml"]
        chain_details, entity_mapping, protein_info = reader.find_chain_details(
            structure_molecule_xml_file,
            self.options["pdb_id"],
            self.options["data_path"])
        self.data["chain_details"] = chain_details
        self.data["protein_info"] = protein_info
        self.data["entity_mapping"] = entity_mapping

    def find_construct_sequences(self):
        seq_builder = PDBSeqBuilder()
        data = seq_builder.build_all_chains(self.structure, self.options["pdb_id"])
        for k, v in data.items():
            self.data[k] = v
        for chain_id, seq in data['construct_sequences'].items():
            self.data['chain_details'][chain_id]['construct_sequences'] = seq
        for chain_id, pdb_range in data['construct_pdb_ranges'].items():
            self.data['chain_details'][chain_id]['pdb_start'] = pdb_range[0]
            self.data['chain_details'][chain_id]['pdb_end'] = pdb_range[1]

    def intramolecular_value(self, chain_id):
        return self.data["intramolecular"][chain_id]

    def get_chain_id(self, chain):
        return "{}.{}".format(
            self.options["pdb_id"],
            chain.id if isinstance(chain, Bio.PDB.Chain.Chain) else str(chain))

    def chain_combinations(self):
        return itertools.permutations(self.structure[0].child_list, 2)

    def any_chain_below_intramolecular_cutoff(self, chains):
        imol_values = [self.intramolecular_value(self.get_chain_id(chain.id))
                        for chain in chains ]
        imol_cutoff = self.options["intramolecular_cutoff"]
        return any([x < imol_cutoff for x in imol_values])

    def is_finding_motifs(self):
        return self.options["task"] == "find_motifs"

    def _get_interfase_contacts(self, sidechain_interactions, residue_mapping):

        interactors = {self.get_chain_id(c):[] for c in self.structure[0].child_list}
        contacts = {self.get_chain_id(c):{} for c in self.structure[0].child_list}
        interfaces = {self.get_chain_id(c):{} for c in self.structure[0].child_list}

        for chain_a, chain_b in self.chain_combinations():
            chain_b_id = self.get_chain_id(chain_b)
            if (not self.is_finding_motifs() or
                self.any_chain_below_intramolecular_cutoff((chain_a, chain_b))):

                interactor, current_contacts, current_interfaces = self.findInteractingResidues(
                    chain_b,
                    chain_a,
                    sidechain_interactions,
                    residue_mapping)
                interactors[chain_b_id].extend(interactor)
                contacts[chain_b_id].update(current_contacts)
                interfaces[chain_b_id].update(current_interfaces)
        return interactors, contacts, interfaces


    def find_interfaces(self):
        """
        Extracts interactor chains, contacts and interfaces between any two chains for every chain
        in a pdb. Contacts are directional and can be asymetrical, ie. Residue A can be in contact 
        with a residue B, but B might not be in contact with A. This is given by the option of 
        looking contacts from, for example, a side chain of a residue to the backbone of other 
        residue.
            :param self: 
        """
        chain_details = self.data['chain_details']
        domain_info = self.data["domain_info"]
        collapse_pfam = self.settings['collapse_pfam']
        domain_contacts_cutoff = self.options["domain_contacts_cutoff"]

        try:

            interactors, contacts, interfaces = self._get_interfase_contacts(
                self.options["sidechain_interactions_only"],
                self.data["residue_mapping"])

            self.data["interactors"] = interactors
            self.data["contacts"] = contacts
            self.data["interfaces"] = interfaces

            chain_pairs_with_domain_info = [(chain, interacting_chain)
                for chain in interactors
                for interacting_chain in interactors[chain]
                if interacting_chain in domain_info]

            for c in interactors:
                chain_details[c]["interacting_domains"] = {}
                chain_details[c]["interacting_pfam"] = []

            for c, ic in chain_pairs_with_domain_info:

                interface_set = set(interfaces[c][ic]['interactor_residues_pdb'])

                domain_ranges = [
                    (domain, range(int(domain['pdbResNumStart']),
                                     int(domain['pdbResNumEnd'])))
                    for domain in domain_info[ic] ]

                interface_intersections = [
                    (d, list(interface_set.intersection(r)))
                    for d, r in domain_ranges]

                for d, i in interface_intersections:
                    d['contacts'] = i

                ii_filtered = [(dom, intersec)
                    for dom, intersec in interface_intersections
                    if len(intersec) > 0]

                ii_pfam = [(dom, intersec, dom['pfamAcc'].split(".")[0])
                    for dom, intersec in ii_filtered ]

                ii_pfam_collapsed = [
                    (dom, ints, collapse_pfam.get(pfam_id, pfam_id))
                    for dom, ints, pfam_id in ii_pfam]

                chain_details[c]["interacting_pfam"].extend(
                    [pfam for _, _, pfam in ii_pfam_collapsed])

                chain_details[c]["interacting_domains"][ic] = {
                    pfam: [dom for dom,_,_ in doms]
                    for pfam, doms in itertools.groupby(
                        ii_pfam_collapsed, lambda x: x[2])}

                domain_contacts = [pfam
                    for _, intersec, pfam in ii_pfam_collapsed
                    if len(intersec) >= domain_contacts_cutoff]

                if len(domain_contacts) > 0:
                    for interacting_chain in interactors[c]:
                        current_interaction = chain_details[c]["interacting_domains"][ic]

                        filtered_interactions = (
                            (pfam, [inst for inst in instances
                                if len(inst['contacts']) >= domain_contacts_cutoff])
                            for pfam, instances in current_interaction.items())

                        filtered_interactions = {pfam : instances
                            for fpam, instances in filtered_interactions
                            if len(instances) > 0}

                        current_interaction.clear()
                        current_interaction.update(filtered_interactions)

            for chain in interactors:
                interface_set = {r
                    for interacting_chain in interfaces[chain]
                    for r in interfaces[chain][interacting_chain]['interactor_residues_pdb']}

                domains = [d for d in domain_info[chain]] if chain in domain_info else []
                domains = [(d, range(int(d['pdbResNumStart']), int(d['pdbResNumEnd'])))
                        for d in domains]
                domains = [(d['pfamAcc'], d) for d, r in domains if len(interface_set.intersection(r)) > 0]

                containing_domains = {}
                containing_domains = {
                    pfam:[ d1 for pfam1, d1 in domains if pfam == pfam1]
                        for pfam, d in domains }

                chain_details[chain]["containing_domains"] = containing_domains
        except:
            raise

    def chain_is_protein(self, chain):
        """
        Checks if a pdb chain is a protein.
            :param self: 
            :param chain: a Bio.PDB.Chain.Chain object
        """
        return self.data['chain_details'][self.get_chain_id(chain.id)]['polymer_type'] == "protein"

    def _chain_is_nucleic_acid_or_hybrid(self, chain):
        """
        Checks if a pdb chain is a nucleic acid or hybrid.
            :param self: 
            :param chain: a Bio.PDB.Chain.Chain object
        """
        return self.data['chain_details'][self.get_chain_id(chain)]['polymer_type'] in ["rna", "dna", "hybrid"]

    def _pick_contact_selector(self, chain_a, chain_b, side_interaction, inter_chain_distance):
        if self.chain_is_protein(chain_a) and self.chain_is_protein(chain_b):
            return ProteinChainsContactSelector(
                side_interaction,
                inter_chain_distance)

        elif self._chain_is_nucleic_acid_or_hybrid(chain_a) and self.chain_is_protein(chain_b):
            return HybridChainsContactSelector(inter_chain_distance, True)

        elif self._chain_is_nucleic_acid_or_hybrid(chain_b) and self.chain_is_protein(chain_a):
            return HybridChainsContactSelector(inter_chain_distance, False)
        else:
            return None

    def findInteractingResidues(self, chain_a, chain_b, side_interaction, residue_mapping):
        chain_b_id = self.get_chain_id(chain_b)
        chain_a_id = self.get_chain_id(chain_a)
        contact_selector = self._pick_contact_selector(chain_a, chain_b, side_interaction,
                                                       self.options['inter_chain_distance'])
        interface_sequence = ""
        if contact_selector:
            for chain_b_res in chain_b.child_list:
                contact = False
                for chain_a_res in chain_a.child_list:
                    contact_selector.update_contacts(chain_a_res, chain_b_res)
                    contact = contact or contact_selector.in_contact()
                res_name = chain_b_res.resname
                next = (AminoAcids().one_letter(res_name) if contact else ".") if res_name in AminoAcids() else ""
                interface_sequence += next 

            interactor_residues_pdb = list(set(contact_selector.interactor_residues))
            interactor_residues_pdb.sort()
            interactor_residues_uniprot = []
            interactor_residues_mapping = {}

            if self.chain_is_protein(chain_b):
                for position in interactor_residues_pdb:
                    if position in residue_mapping[chain_b_id]["residues"]:
                        interactor_residues_mapping[position] = residue_mapping[chain_b_id]["residues"][position]
                        interactor_residues_uniprot.append(residue_mapping[chain_b_id]["residues"][position]['protein_offset'])
                interactor_residues_uniprot.sort()

            if len(interactor_residues_pdb) > 1:
                interactor = [chain_b_id]
                contacts = {chain_b_id: contact_selector.contacts}
                if residue_mapping:
                    contact_res = ((ra, rb) for ra in contacts[chain_b_id]
                                            for rb in contacts[chain_b_id][ra])
                    contact_res = list(contact_res)
                    for ra, rb in contact_res:
                        contacts[chain_b_id][ra][rb].addidional_info(
                            residue_mapping.get(chain_a_id,{}).get('residues',{}).get(rb, {}))
                            # Sometimes there are missing residues in residue_mapping due to dssp
                            # skipping the last residue of a protein

                interfaces = {chain_b_id: {
                        "interactor_residues_pdb": interactor_residues_pdb,
                        "interactor_residues_uniprot": interactor_residues_uniprot,
                        "interactor_residues_mapping": interactor_residues_mapping,
                        "interface_sequence": interface_sequence}}
                return interactor, contacts, interfaces
        return [], {}, {}

    def is_antigen(self, chain):
        """
        Checks if a given pdb chain has a pfam of an antigen.
            :param self: 
            :param chain: A pdb chain like "PDB1.A"
        """   
        if "interacting_pfam" in self.data['chain_details'][chain]:
            return len(set(self.data['chain_details'][chain]["interacting_pfam"]).intersection(set(self.settings["antigen_presenting"] + self.settings["antigen_binding"]))) > 0
        else:
            return False

    def define_ss(self):
        """
        Defines the secondary structure and alpha helices for every pdb chain from dssp data.
            :param self: 
        """
        ss_obj = DsspSecondaryStructure()
        ss_obj.define_ss(self.data["dssp"])
        self.data["helices"] = ss_obj.get_helices()
        self.data["secondary_structure"] = ss_obj.get_secondary_structure()

    def load_structure(self):
    	"""
    	Extracts the Bio.PDB.Structure.Structure from the current PDB.
    	    :param self: 
    	"""
        self.structure = Bio.PDB.PDBParser().get_structure(self.options["pdb_id"], 
                                                           self.paths["structure_pdb"])

    def get_structure_data_complex(self):
        self.get_structure_data(check_complex=True)

    def get_structure_data(self, check_complex=False):
        structure = StructureDataGetter().get(
            os.path.abspath(self.options["data_path"]),
            self.options["pdb_id"],
            self.options['dssp_path'],
            self.paths["structure_pdb"])
        self.structure = structure
        if check_complex and len(structure[0].child_list) <= 1:
            sys.stderr.write("Only one entity in structure. Not a complex.")

    def make_output(self):
        self.json = {"status": "Success"}
        for data_type in ['structure_details', 'module_type', 'intramolecular', 'mean_surface_accessibilty', 'secondary_structure', 'interfaces', 'interactors']:
            self.json[data_type] = self.data[data_type]

    def delete_files(self):
        delete_list = ["structure_pdb", "structure_xml",
                         "structure_pfam_xml", "structure_molecule_xml", "structure_dssp"]
        for delete_file in delete_list:
            if os.path.exists(self.paths[delete_file]):
                os.remove(self.paths[delete_file])
                print "Deleting", self.paths[delete_file]

    def create_status_file(self, status):
        self.delete_files()
        with open(self.paths["structure_status"], "w") as outfile:
            json.dump(status, outfile)
            print "Adding", self.paths["structure_status"]

    def tidy_delete_later(self):
        self.setup_paths()
        if os.path.exists(self.paths["structure_xml"]) and os.path.exists(self.paths["structure_molecule_xml"]):
            self.parse_general_data()
            self.find_chain_details()
            if len(self.data["chain_details"]) <= 1:
                status = {
                    "status": "Error", "error_type": "Only one entity in structure. Not a complex."}
                self.create_status_file(status)
                return {"status": "Error", "error_type": "Only one entity in structure. Not a complex."}
            if self.data["structure_details"]["status"] == "OBSOLETE":
                status = {"status": "Error",
                            "error_type": "Entry is marked as obsolete."}
                print status
                self.create_status_file(status)
                return status

    def run_with_debug(self, func, debug_active, start_time):
        func()
        if debug_active:
            print("{:40}: {}" .format(func.__name__, time.time() - start_time))

    def parse_structure(self):
        """
        Reads a PDB file a associated XML files.
        Download files if required.
            :param self:
        """
        self.setup_paths()
        if os.path.exists(self.paths["structure_status"]) and self.options["remake"] == False:
            return json.loads(open(self.paths["structure_status"]).read())

        pdbDownloaderObj = pdbDownloader.pdbDownloader()
        pdbDownloaderObj.options["data_path"] = os.path.abspath(
            self.options["data_path"])
        pdbDownloaderObj.options["pdbID"] = self.options["pdb_id"]
        pdbDownloaderObj.grabMoleculePDB()
        pdbDownloaderObj.grabPDB()

        if not os.path.exists(self.paths["structure_xml"]):
            return {"status": "Error", 'error_type': "PDB XML file cannot be retrieved"}

        if not os.path.exists(self.paths["structure_molecule_xml"]):
            return {"status": "Error", 'error_type': "Molecule file cannot be retrieved"}

        status = self.parse_general_data()
        if status['status'] == "Error":
            return status

        self.find_chain_details()

        del pdbDownloaderObj

        if len(self.data["chain_details"]) <= 1:
            status = {"status": "Error",
                        "error_type": "Only one entity in structure. Not a complex."}
            self.create_status_file(status)
            return status

        if self.data["structure_details"]["number_entities"] <= 1 and self.options["skip_homomultimers"]:
            status = {"status": "Error",
                        "error_type": "Multimer but only one entity in structure [skip_homomultimers set]."}
            self.create_status_file(status)
            return status

        if self.data["structure_details"]["status"] == "OBSOLETE":
            status = {"status": "Error",
                        "error_type": "Entry is marked as obsolete."}
            self.create_status_file(status)
            return status

        if self.data["structure_details"]['experimental_method'].count('model') > 0:
            status = {"status": "Error", "error_type": "Entry is a model."}
            self.create_status_file(status)
            return status

        if self.data["structure_details"]['experimental_method'] == 'electron microscopy':
            status = {"status": "Error",
                        "error_type": "Entry is electron microscopy structure."}
            self.create_status_file(status)
            return status

        if self.data["structure_details"]["number_entities"] > 8 and self.options["skip_large_multimers"]:
            status = {"status": "Error", "error_type": "Too many entities:" +
                        str(self.data["structure_details"]["number_entities"])}
            self.create_status_file(status)
            return status

        things_to_do = [
            self.get_structure_data_complex,
            self.parse_DSSP,
            self.find_construct_sequences,
            self.load_domains,
            self.map_construct_sequences,
            self.calculate_intramolecular_interactions,
            self.define_ss,
            self.find_interfaces,
            self.find_module_types,
            self.adjust_sequence_for_gaps,
            self.create_contact_sequence]

        start_time = time.time()
        for thing in things_to_do:
            self.run_with_debug(thing, self.options["debug"], start_time )

        return {"status": "Success"}

    def find_module_types(self):
        assigner = ModuleAssigner()
        assigner.set_intramolecular_cutoff(self.options["intramolecular_cutoff"])
        assigner.set_disorder_domain_length(self.options["disorder_domain_length"])
        assigner.set_surface_accessibility_cutoff(self.options["surface_accessibility_cutoff"])
        assigner.set_antigen_presenting(self.settings["antigen_presenting"])
        assigner.set_antigen_binding(self.settings["antigen_binding"] )
        assigner.set_coiled_coil_length_cutoff(self.options["coiled_coil_helix_helix_contact_length_cutoff"])
        assigner.set_coiled_coil_proportion_cutoff(self.options["coiled_coil_helix_helix_contact_proportion_cutoff"])
        assigner.set_contacts(self.data["contacts"])
        assigner.set_helices(self.data["helices"])
        assigner.set_dssp(self.data["dssp"])
        for chain in self.data["intramolecular"]:
            assigner.add_chain_data(
                chain,
                self.data["chain_details"][chain]['polymer_type'],
                self.data["mean_surface_accessibilty"][chain],
                self.data["intramolecular"][chain],
                self.data['interfaces'],
                self.data["secondary_structure"].get(chain,{}).get('ss_type',''),
                self.data['chain_details'][chain]["interacting_pfam"])
        types = assigner.get_types()
        for chain, module_type  in types.items():
            self.data["module_type"][chain] = module_type
        coiled_coils = assigner.coiled_coils
        self.data["coiled coil"] = coiled_coils

        for chain, module_type in types.items():
            if chain in self.data["chain_details"]:
                self.data["chain_details"][chain]['motif_present'] = assigner.is_disordered(module_type)

    def _build_json_output(self):
        result = {}
        result['protein_info'] = self.data['protein_info']
        result['domain_info'] = self.data['domain_info']
        result['structure_details'] = self.data['structure_details']
        result['motif_details'] = {}
        result['pocket_details'] = {}
        result['motif_details'] = {}
        result["chains"] = {}
        
        motif_interface_chains = []

        chains = [chain for chain in self.data['module_type']
        if chain in self.data["chain_details"]]
        for chain in chains:
            result["chains"][chain] = {}
            result["chains"][chain]['chain'] = chain

            if chain in self.data["residue_mapping"]:
                result["chains"][chain]["residue_mapping"] = self.data["residue_mapping"][chain]

            for header in ['accession', 'polymer_length', 'polymer_chains', 'start', 'end', 'fragment_description', 'polymer_description', 'motif_present', 'mean_intramolecular_interactions', 'mean_surface_accessibility',  'polymer_type']:
                result["chains"][chain][header] = self.data["chain_details"][chain][header]

            # ASK 'construct_sequence', etc could be in the headers list above
            result["chains"][chain]['construct_sequence'] = self.data['chain_details'][chain]["construct_sequences"]
            result["chains"][chain]['construct_start'] = self.data['chain_details'][chain]["pdb_start"]
            result["chains"][chain]['construct_end'] = self.data['chain_details'][chain]["pdb_end"]

            if "buried_residues" in self.data['chain_details'][chain]:
                result["chains"][chain]['buried_residues'] = self.data['chain_details'][chain]["buried_residues"]
            if "accessible_residues" in self.data['chain_details'][chain]:
                result["chains"][chain]['accessible_residues'] = self.data['chain_details'][chain]["accessible_residues"]
            if "sa_difference" in self.data['chain_details'][chain]:
                result["chains"][chain]['sa_difference'] = self.data['chain_details'][chain]["sa_difference"]
            if "secondary_structure" in self.data['chain_details'][chain]:
                result["chains"][chain]['ss'] = self.data['chain_details'][chain]["secondary_structure"]
            if "uniprot_sequence" in self.data['chain_details'][chain]:
                result["chains"][chain]['sequence'] = self.data['chain_details'][chain]["uniprot_sequence"]
            if "uniprot_start" in self.data['chain_details'][chain]:
                result["chains"][chain]['start'] = self.data['chain_details'][chain]["uniprot_start"]
            if "uniprot_end" in self.data['chain_details'][chain]:
                result["chains"][chain]['end'] = self.data['chain_details'][chain]["uniprot_end"]

            if self.data['module_type'][chain] not in ['globular domain', 'dna', 'rna', 'hybrid']:
                if 'uniprot_sequence' in self.data['chain_details'][chain]:
                    if len(self.data['chain_details'][chain]["uniprot_sequence"]) == 0:
                        if 'construct_uniprot_sequence' in self.data['chain_details'][chain]:
                            result["chains"][chain]['sequence'] = self.data['chain_details'][chain]["construct_uniprot_sequence"]
                else:
                    result["chains"][chain]['sequence'] = self.data['chain_details'][chain]['construct_sequences']

            else:
                result["chains"][chain]['sequence'] = self.data['chain_details'][chain]['construct_sequences']

            if 'containing_domains' in self.data['chain_details'][chain]:
                result["chains"][chain]['domains'] = self.data['chain_details'][chain]['containing_domains']
            else:
                result["chains"][chain]['domains'] = []

            # and self.data["construct_aligned"][chain]:
            if self.data['module_type'][chain] not in ['globular domain', 'dna', 'rna'] and chain in self.data['interactors']:
                result['motif_details'][chain] = copy.deepcopy(
                    self.data["chain_details"][chain])
                result['motif_details'][chain]["start"] = result['chains'][chain]["start"]
                result['motif_details'][chain]["end"] = result['chains'][chain]["end"]
                result['motif_details'][chain]["residue_mapping"] = self.data["residue_mapping"][chain]

                if result['motif_details'][chain]['accession'] in self.data["protein_info"]:
                    for header in ['protein_name', 'family', 'species_common', 'taxonomy', 'accession', 'version', 'taxon_id', 'species_scientific', 'id', 'gene_name']:
                        if header in self.data["protein_info"][result['motif_details'][chain]['accession']]:
                            result['motif_details'][chain][header] = self.data["protein_info"][
                                result['motif_details'][chain]['accession']][header]
                        else:
                            result['motif_details'][chain][header] = ""

                    if result['chains'][chain]["start"] != None and result['chains'][chain]["end"] != None:
                        result['motif_details'][chain]["n_flank"] = self.data["protein_info"][result['motif_details'][chain]['accession']]['sequence'][max(
                            0, result['chains'][chain]["start"]-6):result['chains'][chain]["start"]-1]
                        result['motif_details'][chain]["c_flank"] = self.data["protein_info"][result['motif_details'][chain]['accession']]['sequence'][result['chains'][chain]["end"]:min(
                            result['chains'][chain]["end"]+5, self.data["protein_info"][result['motif_details'][chain]['accession']]['sequence'])]
                    else:
                        result['motif_details'][chain]["n_flank"] = ""
                        result['motif_details'][chain]["c_flank"] = ""
                else:
                    result['motif_details'][chain]["n_flank"] = ""
                    result['motif_details'][chain]["c_flank"] = ""

                result['motif_details'][chain]["modifications"] = []
                result['motif_details'][chain]["modification_details"] = {}



                for modification in self.data["modifications"][chain]:
                    result['motif_details'][chain]["modifications"].append(
                        self.data["modifications"][chain][modification]['modification'])

                    if modification in self.data["residue_mapping"][chain]['residues']:
                        if self.data["residue_mapping"][chain]['residues'][modification]['protein_residue'] == self.data["modifications"][chain][modification]['aa'].upper():
                            modification_details = {
                                'protein_residue': self.data["residue_mapping"][chain]['residues'][modification]['protein_residue'],
                                'construct_offset': self.data["residue_mapping"][chain]['residues'][modification]['construct_offset'],
                                'pdb_offset': self.data["residue_mapping"][chain]['residues'][modification]['pdb_offset'],
                                'protein_offset': self.data["residue_mapping"][chain]['residues'][modification]['protein_offset'],
                                'modification': self.data["modifications"][chain][modification]['modification']
                            }

                            result['motif_details'][chain]["modification_details"][self.data["residue_mapping"]
                                                                                        [chain]['residues'][modification]['construct_offset']] = modification_details
                        else:
                            result['motif_details'][chain]["modification_details"][modification] = {
                                'protein_residue': self.data["modifications"][chain][modification]['aa'].upper(),
                                'construct_offset': modification,
                                'pdb_offset': modification,
                                'protein_offset': modification,
                                'modification': self.data["modifications"][chain][modification]['modification']
                            }
                    else:
                        print "Error modification @", modification
                        result['motif_details'][chain]["modification_details"][modification] = {
                            'protein_residue': self.data["modifications"][chain][modification]['aa'].upper(),
                            'construct_offset': modification,
                            'pdb_offset': modification,
                            'protein_offset': modification,
                            'modification': self.data["modifications"][chain][modification]['modification']
                        }

                result['motif_details'][chain]['module_type'] = self.data['module_type'][chain]
                result['motif_details'][chain]["interacting_chains"] = self.data['interactors'][chain]
                result['motif_details'][chain]["pocket_interactor_residues_pdb"] = {}
                result['motif_details'][chain]["pocket_interactor_residues_uniprot"] = {}
                result['motif_details'][chain]["pocket_interface_sequence"] = {}
                result['motif_details'][chain]["pocket_interactor_domain"] = {}
                result['motif_details'][chain]["motif_interactor_residues_pdb"] = {}
                result['motif_details'][chain]["motif_interactor_residues_uniprot"] = {}
                result['motif_details'][chain]["interactor_residues_mapping"] = {}
                result['motif_details'][chain]["motif_interface_sequence"] = {}
                result['motif_details'][chain]["motif_contacts"] = {}
                result['motif_details'][chain]["interacting_proteins"] = []
                result['motif_details'][chain]["interacting_domains"] = []

                # motif_contact_sequence = []

                for interacting_chain in self.data['interactors'][chain]:
                    result['motif_details'][chain]["pocket_interactor_residues_pdb"][
                        interacting_chain] = self.data['interfaces'][chain][interacting_chain]['interactor_residues_pdb']
                    result['motif_details'][chain]["pocket_interactor_residues_uniprot"][
                        interacting_chain] = self.data['interfaces'][chain][interacting_chain]['interactor_residues_uniprot']
                    result['motif_details'][chain]["interactor_residues_mapping"][interacting_chain] = self.data[
                        'interfaces'][chain][interacting_chain]['interactor_residues_mapping']
                    result['motif_details'][chain]["pocket_interface_sequence"][
                        interacting_chain] = self.data['interfaces'][chain][interacting_chain]['interface_sequence']

                    if chain in self.data['interfaces'][interacting_chain]:
                        result['motif_details'][chain]["motif_interactor_residues_pdb"][interacting_chain] = self.data[
                            'interfaces'][interacting_chain][chain]['interactor_residues_pdb']
                        result['motif_details'][chain]["motif_interactor_residues_uniprot"][interacting_chain] = self.data[
                            'interfaces'][interacting_chain][chain]['interactor_residues_uniprot']
                        result['motif_details'][chain]["interactor_residues_mapping"][interacting_chain] = self.data[
                            'interfaces'][interacting_chain][chain]['interactor_residues_mapping']
                        result['motif_details'][chain]["motif_interface_sequence"][interacting_chain] = self.data[
                            'interfaces'][interacting_chain][chain]['interface_sequence']
                        result['motif_details'][chain]["motif_contacts"][
                            interacting_chain] = self.data['contacts'][interacting_chain][chain]

                    if interacting_chain not in motif_interface_chains:
                        motif_interface_chains.append(
                            interacting_chain)

                    current_accession = self.data["chain_details"][interacting_chain]['accession']
                    if current_accession:
                        result['motif_details'][chain]["interacting_proteins"].append(current_accession)

                    if "interacting_domains" in self.data['chain_details'][chain]:
                        if interacting_chain in self.data['chain_details'][chain]["interacting_domains"].keys():
                            if len(self.data['chain_details'][chain]["interacting_domains"][interacting_chain]) > 0:
                                c_residues = self.data['interfaces'][chain][interacting_chain]['interactor_residues_pdb']
                                result['motif_details'][chain]["pocket_interactor_domain"][interacting_chain] = c_residues
                                result['motif_details'][chain]["interacting_domains"] += self.data[
                                    'chain_details'][chain]["interacting_domains"][interacting_chain].keys()

                result['motif_details'][chain]["interacting_domains"] = list(
                    set(result['motif_details'][chain]["interacting_domains"]))

                if chain in self.data["residue_mapping"]:
                    result['motif_details'][chain]["pdb_alignment"] = self.data["residue_mapping"][chain]["pdb_alignment"]
                    result['motif_details'][chain]["uniprot_alignment"] = self.data["residue_mapping"][chain]["uniprot_alignment"]

                result['motif_details'][chain]['construct_sequences'] = self.data[
                    'construct_sequences'][chain][self.data["residue_mapping"][chain]['n_terminal_overhang']:]
                result['motif_details'][chain]['sequence'] = result["chains"][chain]['sequence']
                result['motif_details'][chain]['buried_residues'] = self.data['chain_details'][chain]["buried_residues"]
                result['motif_details'][chain]['contact_residues'] = self.data['chain_details'][chain]['contact_residues']

                # -----
                result['motif_details'][chain]["synonyms"] = []
                result['motif_details'][chain]["module_name"] = "Unmapped pocket"

                if result['motif_details'][chain]['module_type'] == "SLiM":
                    interface_type = "motif"
                else:
                    interface_type = "region"


                if "interacting_domains" in self.data['chain_details'][chain]:
                    for interacting_chain in self.data['chain_details'][chain]["interacting_domains"].keys():
                        if self.data['module_type'][interacting_chain] in ['dna', 'rna', 'hybrid']:
                            if self.data['module_type'][interacting_chain] == 'dna':
                                result['motif_details'][chain]["interacting_domains"] += ["111112"]
                                result['domain_info']["all"].append("111112")
                                self.data["domain_info"]["names"]["111112"] = self.data['module_type'][interacting_chain].upper()
                            if self.data['module_type'][interacting_chain] == 'rna':
                                result['motif_details'][chain]["interacting_domains"] += ["111113"]
                                result['domain_info']["all"].append("111113")
                                self.data["domain_info"]["names"]["111113"] = self.data['module_type'][interacting_chain].upper()
                            if self.data['module_type'][interacting_chain] == 'hybrid':
                                result['motif_details'][chain]["interacting_domains"] += ["111114"]
                                result['domain_info']["all"].append("111114")
                                self.data["domain_info"]["names"]["111114"] = "DNA/RNA hybrid"

                if len(result['motif_details'][chain]["interacting_domains"]) > 0:
                    result['motif_details'][chain]["interacting_domains"].sort()
                    result['motif_details'][chain]['module_name'] = "/".join(
                        [self.data["domain_info"]["names"][pfam_id] for pfam_id in result['motif_details'][chain]["interacting_domains"]]) + " binding " + interface_type
                    result['motif_details'][chain]["synonyms"].append(result['motif_details'][chain]['module_name'])
                else:
                    result['motif_details'][chain]["interacting_domains"] = ["111111"]

                if result['motif_details'][chain]['fragment_description'].strip() == "Unknown":
                    pass
                elif result['motif_details'][chain]['fragment_description'].lower().count("unp residues") > 0 and len(result['motif_details'][chain]['fragment_description'].lower().split("unp residues")[0].strip()) == 0:
                    pass
                elif result['motif_details'][chain]['fragment_description'].lower().count("unp residues") > 0 and len(result['motif_details'][chain]['fragment_description'].lower().split("unp residues")[0].strip()) > 0:
                    result['motif_details'][chain]["synonyms"] += [result['motif_details'][chain]['fragment_description'].lower().split("unp residues")[0].strip(" ,(")]
                elif result['motif_details'][chain]['fragment_description'].lower().count("residues") > 0 and len(result['motif_details'][chain]['fragment_description'].lower().split("residues")[0].strip()) == 0:
                    pass
                elif result['motif_details'][chain]['fragment_description'].lower().count("residues") > 0 and len(result['motif_details'][chain]['fragment_description'].lower().split("residues")[0].strip()) > 0:
                    result['motif_details'][chain]["synonyms"] += [result['motif_details'][chain]['fragment_description'].lower().split("residues")[0].strip(" ,(")]
                else:
                    result['motif_details'][chain]["synonyms"] += [result['motif_details'][chain]['fragment_description'].lower()]

                if 'containing_domains' in result['motif_details'][chain]:
                    if len(result['motif_details'][chain]['containing_domains']) > 0:
                        result['motif_details'][chain]['synonyms'] += [
                            result['motif_details'][chain]['containing_domains'].values()[0][0]['pfamDesc']]

                        if result['motif_details'][chain]["module_name"] == "Unmapped pocket":
                            result['motif_details'][chain]['module_name'] = result['motif_details'][chain]['containing_domains'].values()[0][0]['pfamDesc']
                    else:
                        result['motif_details'][chain]['containing_domains'] = []

                if chain in self.data["secondary_structure"]:
                    for header in self.data["secondary_structure"][chain]:
                        result['motif_details'][chain][header] = self.data["secondary_structure"][chain][header]
        return result

    def find_motifs(self):
        """
        Find motifs in a pdb chain.
            :param self:
        """
        self.options["pdb_id"] = self.options["pdb_id"].upper()
        try:
            pdb_id = self.options["pdb_id"]
            self.paths["motif_interfaces"] = join(self.options["data_path"],"interfaces", pdb_id + ".motif.json")
            should_make_computation = not os.path.exists(self.paths["motif_interfaces"]) or self.options["remake"]
            if should_make_computation:
                status = self.parse_structure()
                is_error_status = status["status"] == "Error"
                self.json = status if is_error_status else self._build_json_output()
                with open(self.paths["motif_interfaces"], "w") as outfile:
                    json.dump(self.json, outfile, cls=CjsonEncoder)
                status = status if is_error_status else {"status": "Success", "data": self.json}
            else:
                self.json = json.loads(open(self.paths['motif_interfaces']).read())
                self.tidy_delete_later()
                status = self.json if u'status' in self.json and self.json[u'status'] == "Error" else {"status": "Success", "data": self.json}

            try:
                if self.structure:
                    del_bio_entity(self.structure)
            except:
                pass

            return status

        except Exception, e:
            error_type = sys.exc_info()[0]
            error_value = sys.exc_info()[1]
            error_traceback = traceback.extract_tb(sys.exc_info()[2])

            sys.stderr.write("\n")
            sys.stderr.write('Error in routine ' +
                             self.options["pdb_id"] + '\n')
            sys.stderr.write('Error Type       : ' + str(error_type) + '\n')
            sys.stderr.write('Error Value      : ' + str(error_value) + '\n')
            sys.stderr.write('File             : ' + str(error_traceback[-1][0]) + '\n')
            sys.stderr.write('Method           : ' + str(error_traceback[-1][2]) + '\n')
            sys.stderr.write('Line             : ' + str(error_traceback[-1][1]) + '\n')
            sys.stderr.write('Error            : ' + str(error_traceback[-1][3]) + '\n')
            # raise
            return {"status": "Error", "data": e}

    def get_mimimal_pocket_data(self):
        pocket_details = {}
        interacting_chains = []
        pocket_details['motifs'] = self.json['motif_details'].keys()

        for motif in self.json['motif_details']:
            pocket_details[motif] = {}
            pocket_details[motif]['pocket_interface_sequence'] = self.json['motif_details'][motif]['pocket_interface_sequence']
            pocket_details[motif]['pocket_interactor_residues_uniprot'] = self.json[
                'motif_details'][motif]['pocket_interactor_residues_uniprot']

            interacting_chains.append(motif)
            interacting_chains += self.json['motif_details'][motif]['interacting_chains']

        pocket_details['chains'] = {}
        for interacting_chain in interacting_chains:
            pocket_details['chains'][interacting_chain] = self.json['chains'][interacting_chain]

        return pocket_details

    def get_mimimal_motif_data(self):
        motif_details = {}

        for motif in self.json['motif_details']:
            motif_details[motif] = {}
            for information in ['accessible_residues',
                                'uniprot_sequence',
                                'uniprot_end',
                                'synonyms',
                                'surface_accessibilty_normalised',
                                'ss',
                                'ss_type',
                                'sequence',
                                'residue_mapping',
                                'polymer_chains',
                                'pocket_interface_sequence',
                                'pocket_interactor_residues_uniprot',
                                'motif_interface_sequence',
                                'motif_contacts',
                                'module_type',
                                'modifications',
                                'modification_details',
                                'mean_surface_accessibility',
                                'mean_intramolecular_interactions',
                                'interacting_chains',
                                'contact_residues',
                                'construct_uniprot_sequence']:
                if information in self.json['motif_details'][motif]:
                    motif_details[motif][information] = self.json['motif_details'][motif][information]

        return motif_details

    def get_details(self):
        return self.json['structure_details']

    def get_proteins(self):
        return self.json['protein_info']

    def get_contacts(self, detailed=False):
        contacts = {
            "chains": {},
            "contacts": {}
        }

        chains = []
        for motif in self.json['motif_details']:
            motif_accession = self.json['chains'][motif]['accession']
            chains.append(motif)

            contacts["contacts"][motif] = {motif_accession: {}}

            for interacting_chain in self.json['motif_details'][motif]['motif_contacts'].keys():
                chains.append(interacting_chain)
                interacting_chain_accession = self.json['chains'][interacting_chain]['accession']
                contacts["contacts"][motif][motif_accession][interacting_chain_accession] = {
                }
                for pdb_offset in self.json['motif_details'][motif]['motif_contacts'][interacting_chain]:
                    motif_offset = str(
                        self.json['chains'][motif]["residue_mapping"]['residues'][pdb_offset]['protein_offset'])

                    if detailed == False:
                        contacts["contacts"][motif][motif_accession][interacting_chain_accession][motif_offset] = []
                    else:
                        contacts["contacts"][motif][motif_accession][interacting_chain_accession][motif_offset] = {}

                    for interacting_chain_pdb_offset in self.json['motif_details'][motif]['motif_contacts'][interacting_chain][pdb_offset].keys():
                        interacting_chain_offset = str(
                            self.json['chains'][interacting_chain]["residue_mapping"]['residues'][interacting_chain_pdb_offset]['protein_offset'])

                        if detailed == False:
                            contacts["contacts"][motif][motif_accession][interacting_chain_accession][motif_offset].append(interacting_chain_offset)
                        else:
                            contacts["contacts"][motif][motif_accession][interacting_chain_accession][motif_offset][interacting_chain_offset] = self.json[
                                'motif_details'][motif]['motif_contacts'][interacting_chain][pdb_offset][interacting_chain_pdb_offset]

                            # [interacting_chain_pdb_offset]

        for chain in chains:
            contacts["chains"][chain] = {
                'chain': chain,
                'accession': self.json['chains'][chain]['accession'],
                'start': self.json['chains'][chain]['start'],
                'end': self.json['chains'][chain]['end'],
                'sequence': self.json['chains'][chain]['sequence']
            }
        return contacts

    def get_interactions(self):
        interactions = {
            "motif": {}
        }
        for motif in self.json['motif_details']:
            motif_accession = self.json['chains'][motif]['accession']

            interactions["motif"][motif_accession] = {
                "motif_start": self.data['chain_details'][motif]['start'],
                "motif_end": self.data['chain_details'][motif]['end'],
                "motif_peptide": self.data['chain_details'][motif]['uniprot_sequence'],
                "interactor": {}
            }

            for interacting_chain in self.data['chain_details'][motif]["interacting_domains"]:
                interacting_chain_accession = self.json['chains'][interacting_chain]['accession']
                interactions["motif"][motif_accession]["interactor"] \
                [interacting_chain_accession] = self.data['chain_details'] \
                [motif]["interacting_domains"][interacting_chain]

        return interactions
