from abc import ABCMeta, abstractmethod
import itertools
from structure.iupac.amino_acids import AminoAcids
from structure.pdb_utils.distance_checkers import CarbonAlphaDistanceLessThan
from structure.pdb_utils.distance_checkers import CarbonAlphaToAnyDistance

class Contact(object):
    #pylint: disable=too-many-instance-attributes
    """
    A class to represent a directional contact of two atoms of tow different
    residues. As the contact is directional, there is a source atom and a
    destination atom. The destination atom is referred as the contact atom and
    all their properties are prefixed with 'contact_'.
    and
        :param object:
    """
    def __init__(self, distance, residue, atom, backbone,
                 contact_residue, contact_atom, contact_backbone):
        #pylint: disable=too-many-arguments
        """
        Creates a new Contact.
            :param self:
            :param distance: The distance of the contact in Angstrom.
            :param residue: a Bio.PDB.Residue.Residue object
            :param atom: a Bio.PDB.Atom.Atom object
            :param backbone: a boolean that specifies if the atom is in the
                backbone of a a protein chain
            :param contact_residue:  a Bio.PDB.Residue.Residue object
            :param contact_atom: a Bio.PDB.Atom.Atom object
            :param contact_backbone: a boolean that specifies if the atom is in
                the backbone of a a protein chain
        """
        self.distance = distance
        self.residue = residue
        self.atom = atom
        self.backbone = backbone
        self.contact_residue = contact_residue
        self.contact_atom = contact_atom
        self.contact_backbone = contact_backbone
        self.additional = {}

    def __str__(self):
        """
        Creates a string representation from a contact.
        The string representation is like:
            <A:1.CA->S:11.CB>
            That means a contact from a Carbon alpha from a Ala number 1 residue to
            a Carbon beta from a Ser number 11.
            :param self:
        """
        return "<{}:{}@{}->{}:{}@{}>".format(
            self.one_letter_residue(),
            self.position(),
            self.atom.name,
            self.one_letter_contact_residue(),
            self.contact_position(),
            self.contact_atom.name)

    def __repr__(self):
        """
        Creates a string representation from a contact.
        The string representation is like:
            <A:1.CA->S:11.CB>
            That means a contact from a Carbon alpha from a Ala number 1 residue to
            a Carbon beta from a Ser number 11.
            :param self:
        """
        return str(self)

    def type(self):
        """
        Return the type of contact. This value only makes sense for interprotein
        contacts.
            :param self:
        """
        return "-".join(["backbone" if x else "sidechain"
                         for x in (self.backbone, self.contact_backbone)])

    def one_letter_residue(self):
        """
        Returns the one letter code for the source residue.
        In some uncommon cases c_resname is not in AminoAcids(). For example, for degenerated
        nucleic acid bases. If the length of the unknown residue is just one, it is assumed to
        be a degenerated nucleic acid base or similar and the same string is returned. If not
        a '?' char is returned.        
            :param self:
        """
        c_resname = self.residue.resname.strip()
        if c_resname in AminoAcids():
            return AminoAcids()[c_resname]["one_letter_code"][0]
        else:

            if len(c_resname) == 1:
                return c_resname
            else:
                return "?"

    def one_letter_contact_residue(self):
        """
        Returns the one letter code for the contact residue.
            :param self:
        """
        return AminoAcids()[self.contact_residue.resname.strip()]["one_letter_code"][0]

    def position(self):
        """
        Return the position number of the source residue.
            :param self:
        """
        return self.residue.get_id()[1]

    def contact_position(self):
        """
        Return the position number of the contact residue.
            :param self:
        """
        return self.contact_residue.get_id()[1]

    def addidional_info(self, a_dict):
        """
        Appends a dict for custom annotations of the contact.
            :param self:
            :param a_dict:
        """
        self.additional.update(a_dict)

    def to_dict(self):
        """
        Creates a dict representation of the Contact. It is intended to be used
        to export json data of a contact.
            :param self:
        """
        result = {
            "distance": float(self.distance),
            "residue": self.one_letter_residue(),
            "position": self.position(),
            "atom": self.atom.name,
            "backbone": self.backbone,
            "contact_residue": self.one_letter_contact_residue(),
            "contact_atom": self.contact_atom.name,
            "contact_backbone": self.contact_backbone,
            "contact_position": self.contact_position(),
            "contact_residue_code": self.contact_residue.resname.strip(),
            "contact_type": self.type()
        }
        result.update(self.additional)
        return result

class ContactSelector(object):
    """
    An abstract class for contact selectors. A contact selector decides if two
    residues are in contact. Also accumulates information about the visited
    residue pairs:
    - The closest atoms of each residue contact (as Contact objects).
    - The interactor residues. The complete information of all interacting atoms
        between every residue pair.
        :param object:
    """
    __metaclass__ = ABCMeta

    def __init__(self, inter_chain_distance):
        """
            Creates a new ContactSelector.
            :param self:
            :param inter_chain_distance: The maximum distance between two atoms
                to be considered as a contact.
        """
        self._inter_chain_distance = inter_chain_distance
        self.interactor_residues = []
        self.contacts = {}
        self._in_contact = False

    def in_contact(self):
        return self._in_contact

    def update_contacts(self, res_a, res_b):
        """
        Checks if two residues are in contact, and accumulates their contact
        information.
            :param self:
            :param res_a: a Bio.PDB.Residue.Residue
            :param res_b: a Bio.PDB.Residue.Residue
        """
        is_any_a_water_molecule = self._is_water(res_a) or self._is_water(res_b)
        if not is_any_a_water_molecule and self._are_residues_close(res_a, res_b):
            distance_data = self._get_distance_contacts(res_a, res_b)
            closest = min(distance_data, key=lambda x: x.distance)

            if self._closest_are_in_contact(closest):
                self.interactor_residues.append(res_b.get_id()[1])

                if closest.position() not in self.contacts:
                    self.contacts[closest.position()] = {}

                if self._are_residues_valid(res_a, res_b):
                    self.contacts[closest.position()][closest.contact_position()] = closest
                self._in_contact = True
        else:
            self._in_contact = False

    @abstractmethod
    def _are_residues_valid(self, res_a, res_b):
        pass

    @abstractmethod
    def _closest_are_in_contact(self, closest_pair):
        pass

    @abstractmethod
    def _get_distance_contacts(self, res_a, res_b):
        pass

    @abstractmethod
    def _are_residues_close(self, res_a, res_b):
        pass

    def _below_inter_chain_distance(self, dist):
        return dist < float(self._inter_chain_distance)

    def _is_water(self, a_residue):
        # pylint: disable=no-self-use
        return a_residue.resname.strip() in ["HOH"]

class HybridChainsContactSelector(ContactSelector):
    """
    A selector of contacts between a protein chain and a non-protein chain.
        :param ContactSelector:
    """
    def __init__(self, inter_chain_distance, non_protein_to_protein=True):
        """
        Creates a new HybridChainsContactSelector.
            :param self:
            :param inter_chain_distance: The maximum distance between two atoms
                to be considered as a contact.
            :param non_protein_to_protein=True: As the contact is directional,
                is required to define if the contact has a protein source a non-protein
                destination or vice versa.
        """
        ContactSelector.__init__(self, inter_chain_distance)
        self.non_protein_to_protein = non_protein_to_protein

    def _are_residues_close(self, res_a, res_b):
        return CarbonAlphaToAnyDistance(15).in_contact(res_a, res_b)

    def _get_distance_contacts(self, res_a, res_b):
        return {Contact(atom_b - atom_a,
                        res_b, atom_b, False,
                        res_a, atom_a, False)
                for atom_a, atom_b in itertools.product(
                    res_a.child_list,
                    res_b.child_list)}

    def _closest_are_in_contact(self, closest_pair):
        return self._below_inter_chain_distance(closest_pair.distance)

    def _are_residues_valid(self, res_a, res_b):
        return True

    def update_contacts(self, res_a, res_b):
        if self.non_protein_to_protein:
            res_a, res_b = res_b, res_a
        return super(HybridChainsContactSelector, self).update_contacts(res_a, res_b)


class ProteinChainsContactSelector(ContactSelector):
    """
    A selector of contacts between two protein chains.
        :param ContactSelector:
    """
    def __init__(self, side_interaction, inter_chain_distance):
        """
        Creates a new ProteinChainsContactSelector object.
            :param self:
            :param side_interaction: A string that specifies what type of
                interaction are taken intro account to decide if two residues
                are in contact.
                Valid strings are have the form "(Any,Back,Side)To(Any,Back,Side)".
                For example: "SideToAny". "AnyToAny" is equivalent to "Any"
            :param inter_chain_distance: The maximum distance (in Angstrom) that
                two atoms should have to considered as a contact.
        """
        ContactSelector.__init__(self, inter_chain_distance)
        self.side_interaction = side_interaction

    def _are_residues_close(self, res_a, res_b):
        """
        Check if two residues are somewhat close, so they can be further
        analyzed with more detail. It just check an
        TODO: The distance used to check if two residues are close(15A) is short.
        Many residue pairs could have a distance between alpha carbon larger than
        this value.
        Below there is a list of distances between the most distant atoms in a
        residue for each kind of residue (from 2AST pdb).
        'ALA': 3.6331213  'LEU': 6.088582
        'ARG': 9.480907   'LYS': 8.390167
        'ASN': 5.6662607  'MET': 6.6708655
        'ASP': 5.653207   'PHE': 7.43044
        'CYS': 4.9933205  'PRO': 4.7661743
        'GLN': 6.9966283  'SER': 4.6817517
        'GLU': 6.920483   'THR': 4.746524
        'GLY': 3.6741064  'TRP': 8.183875
        'HIS': 6.722497   'TYR': 8.61099
        'ILE': 5.9041715  'VAL': 4.777367
        """
        return CarbonAlphaDistanceLessThan(15).in_contact(res_a, res_b)

    def _get_distance_contacts(self, res_a, res_b):
        return {Contact(b-a,
                        res_b, b, bb,
                        res_a, a, ba)
                for a, ba in self._get_backbones(res_a)
                for b, bb in self._get_backbones(res_b)}

    def _closest_are_in_contact(self, closest_pair):
        return self._below_inter_chain_distance(closest_pair.distance) \
            and self._should_add_contact(self.side_interaction, closest_pair)

    def _are_residues_valid(self, res_a, res_b):
        return res_b.resname in AminoAcids() and res_a.resname in AminoAcids()

    def _get_backbones(self, a_residue):
        return ((a, self._is_backbone(a))
                for a in self._non_hydrogen_atoms(a_residue))

    def _non_hydrogen_atoms(self, a_residue):
        # pylint: disable=no-self-use
        return [atom for atom in a_residue.child_list
                if atom is not atom.get_name().startswith("H")]

    def _is_backbone(self, an_atom):
        # pylint: disable=no-self-use
        """
        Returns True if the atom is on the backbone of a protein or peptide.
        Looks for Carboxyl-1 carbon, Alpha carbon, Alpha-Amino nitrogen and
        Carboxyl-1
            :param self:
            :param an_atom: a Bio.PDB.Atom.Atom object.
        """
        atom_is_carbon1 = an_atom.get_id() == "C"
        atom_is_oxygen1 = an_atom.get_id() == "O"
        atom_is_alpha_carbon = an_atom.get_id() == "CA"
        atom_is_alpha_amino = an_atom.get_id() == "N"
        return (atom_is_carbon1 or
                atom_is_oxygen1 or
                atom_is_alpha_carbon or
                atom_is_alpha_amino)

    def _should_add_contact(self, side_interaction, closest_pair):
        # pylint: disable=no-self-use
        """
        Decides if a existing contact should be considered depending on the
        location (backbone or sidechain) of the atoms involved.
            :param self:
            :param side_interaction: A string that specifies what type of
                interaction are taken intro account to decide if two residues
                are in contact.
                Valid strings are have the form "(Any,Back,Side)To(Any,Back,Side)".
                For example: "SideToAny". "AnyToAny" is equivalent to "Any"
            :param closest_pair: a Contact object for the closest atoms between
                two residues.
        """
        side_interactions = side_interaction.lower().split("to")
        from_int = side_interactions[0]
        to_int = side_interactions[-1]

        from_ok = (from_int == 'back') == closest_pair.backbone or (from_int == 'any')
        to_ok = (to_int == 'back') == closest_pair.contact_backbone or (to_int == 'any')

        return from_ok and to_ok
