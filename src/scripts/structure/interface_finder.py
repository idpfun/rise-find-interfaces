import sys
import optparse
import json
from pprint import pprint
from structure.find_interfaces import findInterfaces
from structure.custom_json_encoder import CjsonEncoder

def main():
    findInterfacesObj = findInterfaces()
    op = optparse.OptionParser()
    options = {
        "pdb_id": "",
        "chain_id": "",
        "task": "",
        "inter_chain_distance": 8,
        "skip_homomultimers": True,
        "sidechain_interactions_only": "Any",
        "remake": False,
        "debug": True,
        "skip_large_multimers": True,
        "outfmt": "tdt",
    }

    op.add_option("--pdb_id",
                  action="store",
                  dest="pdb_id",
                  default="",
                  help="PDB identifier.")
    op.add_option("--chain_id",
                  action="store",
                  dest="chain_id",
                  default="",
                  help="PDB chain")
    op.add_option("--task",
                  action="store",
                  dest="task",
                  default="find_motifs",
                  help="Task - what to do with the structure [find_interfaces|" + \
                       "find_interface|find_motifs]")
    op.add_option("--inter_chain_distance",
                  action="store",
                  dest="inter_chain_distance",
                  default=8,
                  help="inter_chain_distance")
    op.add_option("--sidechain_interactions_only",
                  action="store",
                  dest="sidechain_interactions_only",
                  default="SideToAny",
                  help="sidechain_interactions_only [SideToSide|SideToBack|BackToSide|" + \
                       "BackToBack|AnyToBack|AnyToSide|BackToAny|SideToAny|Any]")
    op.add_option("--find_homomultimers",
                  action="store",
                  dest="skip_homomultimers",
                  default=True,
                  help="skip_homomultimers")
    op.add_option("--skip_large_multimers",
                  action="store",
                  dest="skip_large_multimers",
                  default=True,
                  help="skip_large_multimers")
    op.add_option("--remake",
                  action="store",
                  dest="remake",
                  default=False,
                  help="remake")
    op.add_option("--debug",
                  action="store",
                  dest="debug",
                  default=False,
                  help="debug [True|False]")
    op.add_option("--outfmt",
                  action="store",
                  dest="outfmt",
                  default="tdt",
                  help="debug [full_json|json|small_json|tdt]")

    opts, args = op.parse_args()

    for option in options:
        try:
            if getattr(opts, option) == "False":
                findInterfacesObj.options[option] = False
            elif getattr(opts, option) == "True":
                findInterfacesObj.options[option] = True
            else:
                findInterfacesObj.options[option] = getattr(opts, option)
        except:
            raise

    if findInterfacesObj.options["pdb_id"] == "":
        print "PDB identifier not set"
        sys.exit()
    else:
        pdb_id = findInterfacesObj.options["pdb_id"].upper()
        findInterfacesObj.options["pdb_id"] = pdb_id

    if findInterfacesObj.options["task"] == "find_interfaces":
        interfaces = findInterfacesObj.parse_structure()

    if findInterfacesObj.options["task"] == "find_interface":
        if findInterfacesObj.options["chain_id"] != "":
            print "PDB chain not set"
            sys.exit()
        interfaces = findInterfacesObj.parse_structure()

    if findInterfacesObj.options["task"] == "find_motifs":
        motifs = findInterfacesObj.find_motifs()
        if motifs["status"] == "Error":
            print motifs
        else:
            headers = ['chain', 'accession', 'start', 'end', 'gene_name', "protein_name", 
                       'module_name', u'module_type', 'fragment_description', 
                       'polymer_description', "sequence", 'contact_residues', "ss", 'ss_type']            
            outfmt = findInterfacesObj.options["outfmt"]
            motif_details = motifs['data']['motif_details']            
            if outfmt == "full_json":
                out = {k: motifs[k] for k in motifs}
                print json.dumps(out, cls=CjsonEncoder, indent=4, sort_keys=True)
            elif outfmt == "json":
                out = {chain: motif_details[chain] for chain in motif_details}
                print json.dumps(out, cls=CjsonEncoder, indent=4, sort_keys=True)
            elif outfmt == "small_json":
                out = {chain:{h: motif_details[chain][h] for h in headers[1:]} 
                       for chain in motif_details}
                print json.dumps(out, cls=CjsonEncoder, indent=4, sort_keys=True)
            elif outfmt == "tdt":
                print "#{}".format("\t".join(headers))
                for chain in motif_details:
                    chain_data = motif_details[chain]
                    fields = [chain] + [str(chain_data[h]) for h in headers[1:]]
                    print "{}".format("\t".join(fields))

if __name__ == "__main__":
    main()
