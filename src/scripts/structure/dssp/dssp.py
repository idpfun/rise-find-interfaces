from os.path import dirname
from os.path import join
from os.path import splitext
from os.path import exists
from os import remove
from Bio.PDB.PDBIO import PDBIO
from Bio.PDB.PDBIO import Select
from Bio.PDB.PDBParser import PDBParser
from subprocess import check_output
from subprocess import PIPE
from subprocess import CalledProcessError
from structure.dssp.parser import Parser

class Dssp(object):
    def __init__(self, path_to_executable="dssp"):
        """
        Creates a new Dssp object, the path to dssp executable can be supplied. If not, it is 
        assumed that the executable name is 'dssp' and is accessible in current enviroment.
            :param self: 
            :param path_to_executable="dssp": 
        """
        self.dssp = path_to_executable

    def _save_dssp(self, save_to_file, dssp_results):
        saved_files = {c: "{}.dssp".format(splitext(pdb)[0]) 
            for c,(pdb, _) in dssp_results.items()} if save_to_file else {}
        for c, dssp_file in saved_files.items():
            with open(dssp_file, 'w') as fh:
                _, dssp_output = dssp_results[c]
                fh.write(dssp_output)
        return saved_files

    def _parse_dssp_results(self, dssp_results, pdb_id, splitted):
        parsed = {c: Parser().dssp_parse(results, [], pdb_id, True, True) 
            for c,(pdb, results) in dssp_results.items()}
        if splitted:
            chain_ids = parsed.keys()
            result = parsed[chain_ids[0]]
            for c in chain_ids[1:]:
                if parsed[c].chains():
                    chain_id = parsed[c].chains()[0]
                    result.add_chain(chain_id, parsed[c].chain(chain_id))
            return result
        else:
            return parsed['all']

    def _rm_tmp_pdb_files(self, plain_results, rm_tmp, splitted):
        if rm_tmp and splitted:
            files_to_remove = [f for _, (f, _) in plain_results.items()]
            for f in files_to_remove:
                if exists(f):
                    remove(f)

    def run(self, pdb_file, pdb_id, splitted=True, save_to_file=True, parse=True, rm_tmp=True):
        """
        Runs dssp on a PDB file. PDB can be used as a whole or can be splitted in each different 
        chain and computed separetally. Results from PDB can be optionally parsed into a 
        DsspDataPdb object.
            :param self: 
            :param pdb_file: The path of the inut pdb file.
            :param pdb_id: The pdb id of the input file.
            :param splitted=True: If true, dssp is run for each chain separetally.
            :param save_to_file=True: Save dssp results to file (or many files if splitted).
            :param parse=True: Parse dssp output into a DsspDataPdb object.
            :param rm_tmp=True: Remove intermideate PDB files generated when splitted option is True.

        Returns a tuple containg the dssp results and a dict with the path of saved files.
        
        Dssp results are plain text or a DsspDataPdb object, if output is plain text, the result is
        a dict where chain ids are keys when option splitted is True or 'all' is the key when
        splitted is False.
        """
        pdbs_to_run = self._split_pdb(pdb_file) if splitted else {'all': pdb_file}
        plain_results = {c:(pdb, self._run(pdb)) for c, pdb in pdbs_to_run.items()}
        saved_files = self._save_dssp(save_to_file, plain_results)
        result = self._parse_dssp_results(plain_results, pdb_id, splitted) if parse else {c: r for c,(_, r) in plain_results.items()}
        self._rm_tmp_pdb_files(plain_results, rm_tmp, splitted)
        return result, saved_files
    
    def _run(self, pdb_file):
        try:
            result = check_output([self.dssp, '-i', pdb_file])
        except CalledProcessError:
            result = ""
        return result

    def _adapt_pdb_file(self, original_file_name, chain):
        m = splitext(original_file_name)
        case = "Upper" if chain.isupper() else "Lower"
        return "{}.{}_{}.pdb".format(m[0], chain.upper(), case)

    def _split_pdb(self, pdb_file):
        parser = PDBParser()
        structure = parser.get_structure("XXXX", pdb_file)
        first_model = structure[0]
        result = {}
        for c in first_model.get_chains():
            io = PDBIO()
            io.set_structure(structure)
            new_pdb_filename = self._adapt_pdb_file(pdb_file, c.id)
            io.save(new_pdb_filename, ChainSelect(c.id))
            result.update({c.id: new_pdb_filename})
        return result


class ChainSelect(Select):
    """
    Custom Bio.PDB.PDBIO.Select class to select a single chain
        :param Select: parent Bio.PDB.PDBIO.Select
    """
    def __init__(self, chain):
        """
        Creates a new ChainSelect class, that selects a single givenc chain
            :param self: 
            :param chain: a character string representing a PDB chain.
        """
        self.accepted = chain
    
    def accept_model(self, model):
        """
        Accepts only the first model of a PDB
            :param self: 
            :param model: 
        """
        return 1 if model.id == 0 else 0

    def accept_chain(self, chain):
        """
        Accepts a single chain
            :param self: 
            :param chain: 
        """
        return 1 if chain.id == self.accepted else 0