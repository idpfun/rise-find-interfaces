import os,sys,time,json,random,itertools,pickle,urllib2,copy,optparse,warnings,inspect,stat,pprint,gzip

from ConfigParser import SafeConfigParser

class cluster_tool():
	def __init__(self):
		self.clusters = {}
		self.cluster_alignments = {}
		
		self.options = {}
		self.options['min_clusters_similarity'] = 10
		
	def print_matrix(self,distance_matrix):
		matrix_str = ""
		sorted = distance_matrix.keys() 
		sorted.sort()
		
		for i in sorted:
			matrix_str += str(i) + "\t" 
			for j in sorted:
				matrix_str += "%1.2f"%distance_matrix[i][j] + "\t"
				
			matrix_str += "\n"

		print matrix_str
		
	def intialise_clusters(self,chains):
		for chain in chains:
			cluster = {}
			cluster["id"] = str(chain)
			cluster["size"] = 1
			cluster["distance"] = 0
			cluster["members"] = [str(chain)]
			self.clusters[str(chain)] = cluster
			
			
	def find_closest(self,distance_matrix):
		closest = ""
		closest_i = 0
		closest_j = 0

		for i in self.clusters:
			for j in self.clusters:
				if i != j:
					tmp = distance_matrix[i][j]
					if closest == "":
						closest = tmp
					if tmp >= closest:
						closest_i = i 
						closest_j = j
						closest = tmp
		
		return (closest_i, closest_j, closest)

	def cluster_peptides(self, distance_matrix):
		if self.clusters == {}:
			self.intialise_clusters(distance_matrix.keys())
		
		i, j, distance_ij = self.find_closest(distance_matrix)
	
		if distance_ij < self.options['min_clusters_similarity']:
			return self.clusters
			
		cluster_i = self.clusters[i]
		cluster_j = self.clusters[j]

		cluster_joined = {}
		cluster_joined["id"] = str(len(self.clusters.keys()) - 1)
		cluster_joined["size"] = cluster_i["size"] + cluster_j["size"]
		cluster_joined["distance"] = distance_matrix[i][j]
		cluster_joined["members"] = cluster_i["members"] + cluster_j["members"]
		
		
		distance_matrix[cluster_joined["id"]] = {}
		
		#----------------------------
		#----------------------------
		
		for l in self.clusters:
			distance_matrix[cluster_joined["id"]][l] = 0
			distance_matrix[l][cluster_joined["id"]] = 0
			distance_matrix[cluster_joined["id"]][cluster_joined["id"]] = 0
			
		for l in self.clusters:
			if l not in [i,j]:
				distance_il = distance_matrix[max(i, l)][min(i, l)]
				distance_jl = distance_matrix[max(j, l)][min(j, l)]
			
				distance_kl = (distance_il * cluster_i["size"] + distance_jl * cluster_j["size"]) / float (cluster_i["size"] + cluster_j["size"])
				
				distance_matrix[l][cluster_joined["id"]] = distance_kl
				distance_matrix[cluster_joined["id"]][l] = distance_kl
			
		distance_matrix[cluster_joined["id"]][cluster_joined["id"]] = (distance_matrix[i][i] + distance_matrix[j][j])/2
		
		self.clusters[cluster_joined["id"]] = cluster_joined
		
		#----------------------------
		
		#print i, j, distance_ij,(distance_matrix[i][i] + distance_matrix[j][j])/2
		
		for l in self.clusters:
			del distance_matrix[l][j]
			del distance_matrix[l][i]
		
		del self.clusters[i]
		del self.clusters[j]
		
		del distance_matrix[j]
		del distance_matrix[i]
		
		#self.print_matrix(distance_matrix)
		#print "#"
		#sys.exit()
		#----------------------------
		
		if len(self.clusters) == 1:
			return self.clusters
		else:
			return self.cluster_peptides(distance_matrix)


class domainAligner():
	def __init__(self):
	
		self.options = {}
		self.options["pfamid"] = ""  
		self.options["download_domain_data"] = False  
		self.options["align_domain_data"] = False  
		self.options["matrix_file"] = os.path.abspath(os.path.join(os.path.dirname(inspect.stack()[0][1]),"./matrices/blosum62.matrix.txt"))
		self.options["production"] = False  
		self.options['min_clusters_similarity'] = 10
		self.options["print_cluster_alignment"] = False
		 
		config = SafeConfigParser()
		config.read(os.path.join(os.path.dirname(inspect.stack()[0][1]),'../../config.ini'))
		
		for each_section in config.sections():
			for (each_key, each_val) in config.items(each_section):
				if each_val == 'True' or each_val == 'False':
					self.options[each_key] = each_val == 'True'
				else:
					self.options[each_key] = each_val
	
	def restRequest(self,urlRequest):
		opener = urllib2.build_opener()
		f = opener.open(urlRequest).read()
		return json.loads(f)

	def grabDomainInterfaceData(self):
		response = self.restRequest(self.options["url_root"] + "/resource/get/domain_interface/" + self.options["pfamid"])
		return response
	
	def read_similarity_matrix(self,mat_file):
		f = open(mat_file)
 
		header = f.next().strip().split('\t')
	
		idx_to_aa = dict(zip(range(0,len(header)), header))
		similarity_matrix = {}
	
	
		max_score = 0
		for line in f:
			fields = line.strip().split('\t')
			from_aa = fields[0]
			similarity_matrix[from_aa] = {}
		
			for idx, score in enumerate(fields):
				if idx == 0:
					continue
				
				to_aa = idx_to_aa[idx]
				similarity_matrix[from_aa][to_aa] = float(score)
			
				if to_aa not in similarity_matrix:
					similarity_matrix[to_aa] = {}
				
				similarity_matrix[to_aa][from_aa] = float(score)
			
				if float(score) > max_score:
					max_score = float(score)
		
			similarity_matrix[from_aa]["-"] = -4
		
		similarity_matrix["-"] = {}
	
		for aa in similarity_matrix:
			if aa == "-":
				similarity_matrix["-"][aa] = -1
			else:
				similarity_matrix["-"][aa] = -4
	
		return similarity_matrix
		
	def align_domain_data(self,domain_interface_data):
		if 'data' in domain_interface_data:
			domain_region_sequence_fasta = ""
		
			added = []
			
			for interface in domain_interface_data['data']:
				if domain_interface_data['data'][interface][u'structure_pocket_chain'] != None:
					for i in range(0,len(domain_interface_data['data'][interface][u'structure_pocket_chain'])):
						if domain_interface_data['data'][interface][u'structure_pocket_chain'][i] + "-" + domain_interface_data['data'][interface][u'structure_motif_chain'] not in added:
							domain_region_sequence = domain_interface_data['data'][interface][u'region_sequence'][i]
							domain_region_sequence_fasta +=  ">" + str(interface) + " " + str(i) + " " +  domain_interface_data['data'][interface][u'structure_pocket_chain'][i] + "\n" + domain_region_sequence + "\n"
							added.append(domain_interface_data['data'][interface][u'structure_pocket_chain'][i] + "-" + domain_interface_data['data'][interface][u'structure_motif_chain'])
				
			self.options['alignment_directory']= os.path.join(self.options['data_path'],"alignments")
			self.options['domain_alignment_directory'] = os.path.join(self.options['alignment_directory'],"domains")
			self.options['domain_fasta_path']  =  os.path.join(self.options['domain_alignment_directory'],self.options["pfamid"] + ".fas")
			self.options['domain_fasta_aligned_path'] = os.path.join(self.options['domain_alignment_directory'],self.options["pfamid"] + ".aln.fas")
			self.options['domain_distance_matrix_path'] = os.path.join(self.options['domain_alignment_directory'],self.options["pfamid"] + ".distmat.json")
		
			paths = [self.options['alignment_directory'],self.options['domain_alignment_directory']]
		
			for path in paths:
				if not os.path.exists(path):
					os.mkdir(path)
			
			domain_id = 1
			domain_fasta = ""
		
			open(self.options['domain_fasta_path'] ,"w").write(domain_region_sequence_fasta)

			cmd = [
			self.options['clustalo_path'],
			"--force",
			"-i " + 	self.options['domain_fasta_path'] ,
			"-o " + self.options['domain_fasta_aligned_path']
			]
		
			os.popen(" ".join(cmd)).read()
		
			aligned_pocket_sequences = {}
			aligned_protein_sequences = {}
			
			
			if os.path.exists(self.options['domain_fasta_aligned_path']):
				print open(self.options['domain_fasta_aligned_path']).read()
				
				for interface in open(self.options['domain_fasta_aligned_path']).read().strip().split(">")[1:]:
					
					try:
						interface_bits = interface.split("\n")
						interface_id = interface_bits[0].split()[0]
						interface_counter = int(interface_bits[0].split()[1])
						aligned_sequence = "".join(interface_bits[1:])
						aligned_pocket_sequence = ""
						offset = 0
						for i in range (0,len(aligned_sequence)):
							if aligned_sequence[i] == "-":
								aligned_pocket_sequence += "-"
							else:
								aligned_pocket_sequence += domain_interface_data['data'][interface_id][u'region_pocket'][interface_counter][offset]
								offset += 1
					
						aligned_protein_sequences[interface_id + "." + str(interface_counter)] = aligned_sequence
						aligned_pocket_sequences[interface_id + "." + str(interface_counter)] = aligned_pocket_sequence
							
							#print motif_chain,aligned_pocket_sequence
					except:
						print "#Interface",interface, "failed to align"
					 	raise
					 	
				order = aligned_pocket_sequences.keys()
				order.sort()
			
				similarity_matrix = self.read_similarity_matrix(self.options["matrix_file"])
			
				distance_matrix = {}
				for pocket_sequence_chain_a in order:
					distance_matrix[pocket_sequence_chain_a] = {}
					for pocket_sequence_chain_b in order:
						summer = 0
						overlap = 0

						for i in range(0,len(aligned_pocket_sequences[pocket_sequence_chain_a])):
							try:
								aa_a = aligned_pocket_sequences[pocket_sequence_chain_a][i]
								aa_b = aligned_pocket_sequences[pocket_sequence_chain_b][i]

								if  (aa_a != "." and aa_b != ".") and (aa_a != "-" and aa_b != "-"): 
									overlap += 1

								if (aa_a in similarity_matrix and aa_b in similarity_matrix) and (aa_a != "-" and aa_b != "-"): 
									summer += similarity_matrix[aa_a][aa_b]
								else:
									pass

							except:
								print "Similarity calculation failed for", pocket_sequence_chain_a,"-",pocket_sequence_chain_b
						
						distance_matrix[pocket_sequence_chain_a][pocket_sequence_chain_b] = summer
		
				with open(self.options['domain_distance_matrix_path'], 'w') as outfile:
					json.dump(distance_matrix, outfile)

				cluster_tool_Obj = cluster_tool()
				cluster_tool_Obj.options['min_clusters_similarity'] = self.options['min_clusters_similarity']
				clusters = cluster_tool_Obj.cluster_peptides(distance_matrix)
				
				cluster_counter = 0
				aligned_interfaces = {}
				
				for cluster in clusters:
					for interface in clusters[cluster]["members"]:
						aligned_interfaces[interface] = {
						"aligned_pocket_sequence":aligned_pocket_sequences[interface],
						"aligned_protein_sequence":aligned_protein_sequences[interface],
						"cluster_id":cluster_counter}
					
					cluster_counter += 1
					
		return aligned_interfaces

if __name__ == "__main__":
	
	op = optparse.OptionParser()
	
	aligner = domainAligner()
	aligner.options["print_cluster_alignment"] = True
	aligner.options["print_fasta_alignment"] = False
	
	op.add_option("--pfamid",
		  action="store", 
		  dest="pfamid",
		  default=aligner.options["pfamid"] ,
		  help="PFam ID.")
	op.add_option("--min_clusters_similarity",
		  action="store", 
		  dest="min_clusters_similarity",
		  default=aligner.options["min_clusters_similarity"],
		  help="PFam ID.")
	op.add_option("--download_domain_data",
		  action="store_true", 
		  dest="download_domain_data",
		  default=aligner.options["download_domain_data"],
		  help="Download domain data")
	op.add_option("--align_domain_data",
		  action="store_true", 
		  dest="align_domain_data",
		  default=aligner.options["align_domain_data"],
		  help="Align domain data")
	op.add_option("--production",
		  action="store_true", 
		  dest="production",
		  default=aligner.options["production"],
		  help="Grab from production server")
	op.add_option("--print_cluster_alignment",
		  action="store_true", 
		  dest="print_cluster_alignment",
		  default=aligner.options["print_cluster_alignment"],
		  help="Print cluster alignment")
	op.add_option("--print_fasta_alignment",
		action="store_true", 
		dest="print_fasta_alignment",
		default=aligner.options["print_fasta_alignment"],
		help="Print fasta alignment")

	if aligner.options["print_fasta_alignment"]:
		aligner.options["print_cluster_alignment"] = False
		
	opts, args = op.parse_args()
	
	##########################################
	
	for option in aligner.options:
		try:
			aligner.options[option] = getattr(opts, option)
		except:
			pass
	
	
	if len(aligner.options["pfamid"]) == 0:
		print "PFam identifier required"
		sys.exit(1)

	if aligner.options["download_domain_data"]:
		domain_interface_data = aligner.grabDomainInterfaceData()
		pprint.pprint(domain_interface_data)

	if aligner.options["align_domain_data"]:
		domain_interface_data = aligner.grabDomainInterfaceData()
		aligned_domain_interface_data = aligner.align_domain_data(domain_interface_data)
		
		
		for interface in aligned_domain_interface_data:
			interface_id = interface.split(".")[0]
			interface_counter = int(interface.split(".")[1])
			try:
				
				print aligned_domain_interface_data[interface]['cluster_id'],"\t",
				print interface,"\t",
				print domain_interface_data['data'][interface_id][u'motif_buried_residues'],"\t",
				print domain_interface_data['data'][interface_id][u'structure_pocket_chain'][interface_counter]
				print aligned_domain_interface_data[interface]['aligned_pocket_sequence']
				

				fasta = False
				if fasta:
					print ">" + domain_interface_data['data'][interface_id][u'protein_acc'][interface_counter] #," ",
					print aligned_protein_sequences[interface]
					print ">" + domain_interface_data['data'][interface_id][u'structure_pocket_chain'][interface_counter]#," ",
					print aligned_pocket_sequences[interface]
			
			except:
				print "-ERROR"
				raise

	##########################################
	
