import Bio.PDB
from structure.pdb_utils.distance_checkers import NitrogenAlphaDistanceLessThan
from withCache import withCache

class InteractionFinder():
    """
    Simple class to find intramolecular interactions in a PDB chain.
    Uses a cache to avoid re compute data if it is already computed.
    """
    def __init__(self, cache_file = None):
        self.__set_up_cache_for_intramolecular_in_chain(cache_file)

    def __set_up_cache_for_intramolecular_in_chain(self, cache_file):
        """
        Sets up the cache stuff:
            - the cache file
            - a default value for error reading the cache
            - a function the decides if is necessary to write the cache based on
              the result of the underlaying function.
            - a function to transform the data before writing the cache.
            - a function to transform the data after reading the cache.
            :param self: 
            :param cache_file: 
        """
        def avoid_writing_cache(result): return result == 100.0
        def transform_cache_write(data): return repr(data)
        def transform_cache_read(data): return float(data)
        self.intramolecular_in_chain = withCache(
            cache_file, 
            100.0,
            avoid_writing_cache,
            transform_cache_write,
            transform_cache_read
            )(self.intramolecular_in_chain)

    def interactor_residues(self, protein_chain, max_distance):
        """
        Find a list of all contacts for every residue in a chain.
        Returns a dict with interacting residues as keys and a list with all 
        their contacts as value.
            :param self: 
            :param protein_chain: 
            :param max_distance: 
        """
        interactor_residues = {}
        dist_calc = NitrogenAlphaDistanceLessThan(max_distance)
        # ASK: The nested loop traverse all pairs twice, one as (A, B) and the
        # second as (B, A), this can be avoid. Also, pairs (A, A) are taken 
        # as interactions
        for query_res in protein_chain.child_list:
            for contact_res in protein_chain.child_list:
                if dist_calc.in_contact(query_res, contact_res):
                    if query_res not in interactor_residues:
                        interactor_residues[query_res] = []

                    # TODO: I think is not required
                    if contact_res not in interactor_residues:
                        interactor_residues[contact_res] = []

                    interactor_residues[query_res].append(contact_res)
                    # TODO: I think is not required
                    interactor_residues[contact_res].append(query_res)          
        return interactor_residues

    def interactors_per_residue(self, interactors):
        """
        Computes de number of interactions for each residue
            :param self: 
            :param interactors: a dict with interactors and contacts
        """
        # ASK: what is the 3?
        # BUG: The number three i think is to remove the the current, the previous and
        # the next amino acid in the chain. I think that this might be a bug for
        # N-terminal and C-terminal residues.
        return [len(set(interactors[r])) - 3 for r in interactors]

    def intramolecular_in_chain(self, protein_chain, max_distance = 10): # pylint: disable=E0202
        """
        Computes the mean number of interactions per interacting residue. 
            :param self: 
            :param protein_chain: 
            :param max_distance=10: 
        Returns the mean number of contacts per interactor.
        """
        interactor_residues = self.interactor_residues(protein_chain, max_distance)
        interactor_residues_summer = self.interactors_per_residue(interactor_residues)

        total_interactors = sum(interactor_residues_summer)
        # BUG: n_interactors can never be zero, every residue interacts with itself
        # in the current implementation
        n_interactors = len(interactor_residues_summer)
        avg_contacts_per_interactor = (float(total_interactors) / n_interactors) if not n_interactors == 0 else 100.0

        return avg_contacts_per_interactor

