from Bio import pairwise2
from Bio.SubsMat import MatrixInfo as matlist
import re
import operator

class AlignmentMapper():
    def __init__(self, matrix=matlist.blosum62, gap_open=-10, gap_extend = -0.5):
        self.matrix = matrix
        self.gap_open = gap_open
        self.gap_extend = gap_extend
        self.gap_pattern = re.compile("-+")
        self._to_standard = self._create_to_standard()

    def _create_to_standard(self):
        std = set("QWERTYIPASDFGHKLXCVNM")
        known_subs = {"B": "D", "O": "K", "U": "C", "Z": "E"}
        known_subs.update({x:x for x in std})
        return known_subs

    def is_construct_aligned(self, aligned_residues):
        """
            Sets a chains as aligned or not against a construct.
            Is aligned if more residues are aligned than missaligned.
            :param aligned_residues: a dict with residue data
        """
        return max(set(aligned_residues), key=aligned_residues.count) if not len(aligned_residues) == 0 else False

    def chose_best_alignment(self, alignments):
        """
        Choose the best aligment from a list of alignments. Alignments has
        only two sequences. The best one is picked as the one that have the
        lesser number of chunks.
            :param alignments: A iterable of alignments
        """
        # ASK: why 1000?
        choser = [1000, (None, None)]
        for alignment in alignments:
            uniprot, pdb = alignment[0], alignment[1]
            bits = reduce( operator.add, [ len([x for x in s.split("-") if not x == ""]) for s in alignment[:2]] )
            if choser[0] > bits:
                choser = [bits, (uniprot, pdb)]
        return choser[1]

    def do_mapping(self, uniprot_alignment, pdb_alignment, pdb_residues, construct_mapping):
        count_construct = -1
        count_protein = -1
        min_count_protein = None
        max_count_protein = None
        aligned_residues = []
        mapping = {}

        for residue in range(0, len(uniprot_alignment)):
            if pdb_alignment[residue] != "-":
                count_construct += 1

            if uniprot_alignment[residue] != "-":
                count_protein += 1

            if construct_mapping:
                if pdb_alignment[residue] != "-" and count_construct in pdb_residues:
                    if min_count_protein == None:
                        min_count_protein = count_protein
                    if max_count_protein != None:
                        max_count_protein = count_protein

                    if count_protein < min_count_protein:
                        min_count_protein = count_protein
                    if count_protein > max_count_protein:
                        max_count_protein = count_protein

                    try:
                        aligned = uniprot_alignment[residue] == pdb_alignment[
                            residue] == pdb_residues[count_construct]['residue']
                    except:
                        aligned = False

                    aligned_residues.append(aligned)

                    mapping[pdb_residues[count_construct]['position']] = {
                        "construct_offset": count_construct,
                        "pdb_offset": pdb_residues[count_construct]['position'],
                        "protein_offset": count_protein,
                        "protein_residue": uniprot_alignment[residue],
                        "pdb_residue": pdb_residues[count_construct]['residue'],
                        "construct_residue": pdb_alignment[residue],
                        "aligned": aligned
                    }
        return (aligned_residues, mapping, min_count_protein, max_count_protein)       

    def _standarize_sequences(self, seq):
        """
        Converts Non standard amino acids in a protein sequence to an alternative amino acid that 
        can be interpreted and aligned by byoPython pairwise2 function.
        The non standard amino acid are taken from:
        http://www.sbcs.qmul.ac.uk/iupac/AminoAcid/A2021.html#AA21
        The conversions performed are:
        B	Asx	        -> D
        O   pyrrolysine -> K
        U 	Sec	        -> C
        Z	Glx	        -> E
        J, etc          -> X
            :param self: 
            :param seq: A sequence as a string
        """
        if seq:
            substitutions = self._to_standard
            return "".join([ substitutions[x] if x in substitutions else "X" for x in list(seq)])
        else:
            return seq


    def map(self, accession, construct, protein, pdb_residues, construct_mapping=True):
        """
            Creates an alignment of a pdb and an uniprot sequence.
            :param self: 
            :param chain:  
            :param construct_mapping=True: 
        """
        alignments = pairwise2.align.globalds(
            self._standarize_sequences(protein), 
            self._standarize_sequences(construct),
            self.matrix, self.gap_open, self.gap_extend)
        n_terminal_overhang = 0

        if len(alignments) > 0:
            uniprot_alignment, pdb_alignment = self.chose_best_alignment(alignments)

            n_terminal_overhang = len(
                uniprot_alignment) - len(uniprot_alignment.lstrip("-"))

            aligned_residues, mapping, min_count_protein, max_count_protein = self.do_mapping(
                uniprot_alignment, pdb_alignment, pdb_residues, construct_mapping)
        else:
            raise ValueError('Alignment generated is empty')

        is_construct_aligned = self.is_construct_aligned(aligned_residues)

        if min_count_protein != None: 
            min_count_protein = min_count_protein + 1
        if max_count_protein != None: 
            max_count_protein = max_count_protein + 1

        residue_mapping = {
            "protein": accession,
            "residues": mapping,
            "n_terminal_overhang": n_terminal_overhang,
            "pdb_alignment": pdb_alignment,
            "uniprot_alignment": uniprot_alignment,
            "min_count_protein": min_count_protein,
            "max_count_protein": max_count_protein}

        return {
            'pdb_alignment' : pdb_alignment,
            'uniprot_alignment': uniprot_alignment,
            'is_construct_aligned': is_construct_aligned,
            'residue_mapping': residue_mapping}
