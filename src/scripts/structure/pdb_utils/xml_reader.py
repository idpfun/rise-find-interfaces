from xml.etree import cElementTree as elementtree
import os
import sys
import inspect

file_path = os.path.dirname(inspect.stack()[0][1])
sys.path.append(os.path.join(file_path, "../../downloaders"))

from downloaders.uniprotDownloader import uniprotDownloader

class XmlReader():
    """
    A simple class to read xml files for pdb entries.
    Can read "pdb.xml" files and "pdb.molecule.xml" files.
    Also, downloads associated uniprot entries.
    """
    def _pdb_chain(self, pdb, chain):
        return "{}.{}".format(pdb, chain.attrib['id'])
    
    def _get_taxon_id(self, polymer):
        taxon_id = ""
        try:
            taxon_id = polymer.find('Taxonomy').attrib['id']
        except Exception:
            pass  # Taxon not present"
        return taxon_id

    def _get_taxon_name(self, polymer):
        taxon_name = ""
        try:
            taxon_name = polymer.find('Taxonomy').attrib['name']
        except Exception:
            pass  # Taxon not present"
        return taxon_name

    def _get_description(self, polymer):
        try:
            polymer_description = polymer.find(
                'polymerDescription').attrib['description']
        except Exception:
            polymer_description = "Unknown"
        return polymer_description

    def _get_fragment_description(self, polymer):
        try:
            fragment_description = polymer.find('fragment').attrib['desc']
        except Exception:
            fragment_description = "Unknown"
        return fragment_description

    def _make_entity_mappings(self, polymer_list, pdb_id):
        entity_mapping = {}
        entity_mapping["forward"] = {
            entityNr: [self._pdb_chain(pdb_id, chain) for chain in chains] 
                for entityNr, chains in [(int(polymer.attrib["entityNr"]), polymer.iter('chain'))
                    for polymer in polymer_list]}
        entity_mapping["reverse"] = {self._pdb_chain(pdb_id, c): int(p.attrib["entityNr"]) 
            for p in polymer_list for c in p.iter('chain') } 
        return entity_mapping

    def _get_accession(self, polymer):
        try:
            return polymer.find('macroMolecule/accession').attrib['id'].strip()
        except:
            return ""

    def _retrieve_accession_data(self, accession, data_path, pdb_id, polymer):
        protein_info = {}
        # This code shouldn't be here, It has nothing to do with xml file reading
        uniprotDownloaderObj = uniprotDownloader()
        uniprotDownloaderObj.options["data_path"] = data_path
        taxon_id = self._get_taxon_id(polymer)
        taxon_name = self._get_taxon_name(polymer)
        if not accession == "":
            try:
                protein_info = uniprotDownloaderObj.parseUniProt(accession)['data']
            except Exception:
               sys.stderr.write("There was an error downloading or reading Uniprot accession: {}".format(accession))

            if len(protein_info['PDB']) == 0:
                protein_info['PDB'] = {pdb_id: []}
        else:
            accession = ""
            protein_info = self._get_default_empty_protein_info(pdb_id, taxon_id, taxon_name)
        return protein_info
        

    def _get_chain_details(self, pdb_id, polymers):
        return { self._pdb_chain(pdb_id, c):{
                "polymer_description" : self._get_description(p),
                "fragment_description" : self._get_fragment_description(p),
                "polymer_length" : int(p.attrib["length"]),
                "polymer_type" : p.attrib["type"],
                "entityNr" : int(p.attrib["entityNr"]),
                "polymer_chains" : [chain.attrib['id'] for chain in p.iter('chain')],
                "accession" : self._get_accession(p),
                "taxon_id" : self._get_taxon_id(p),
                "taxon_name" : self._get_taxon_name(p),
                "pdb_start": -1,
                "pdb_end": -1,
                "start": -1,
                "end": -1,
                "mean_intramolecular_interactions": -1,
                "mean_surface_accessibility": -1
                } for p in polymers for c in p.iter('chain')}

    def _append_start_and_end_to_chain_details(self, chain_details, protein_info, polymers, pdb_id):
        for polymer in polymers:
            accession = self._get_accession(polymer)
            for chain in polymer.iter('chain'):
                current_chain_id = self._pdb_chain(pdb_id, chain)
                try: 
                    if pdb_id in protein_info[accession]["PDB"]:
                        if chain.attrib['id'] in protein_info[accession]["PDB"][pdb_id]:
                            chain_details[current_chain_id]["start"] = protein_info[accession]["PDB"][pdb_id][chain.attrib['id']]['start']
                            chain_details[current_chain_id]["end"] = protein_info[accession]["PDB"][pdb_id][chain.attrib['id']]['end']
                        else:
                            sys.stderr.write("chain:{} not in protein info for accession:{} and pdb:{}\n".format(chain.attrib['id'], accession, pdb_id))
                    else:
                        sys.stderr.write("pdb_id:{} not in protein info for accession:{}\n".format(pdb_id, accession))
                except Exception:
                    sys.stderr.write("There was an error reading {} file\n".format(accession))
        return chain_details        

    def _get_default_empty_protein_info(self, pdb_id, taxon_id, taxon_name):
        return { 'protein_name': "",
                 'sequence': "",
                 'species_common': taxon_name,
                 'taxonomy': taxon_id, 
                 'accession': "", 
                 'PDB': { pdb_id: []}, 
                 'version': "", 
                 'taxon_id': taxon_id, 
                 'species_scientific': taxon_name, 
                 'id': "", 
                 'gene_name': "" }

    # TODO change name to be related with the behaviour of the method
    def parse_general_data(self, structure_xml_file):
        """
        Reads data from the "structure_xml" file for current pdb.
            :param self: 
        """
        
        tree = elementtree.parse(structure_xml_file)
        # [pylint] W0612:Unused variable 'root'
        # root = tree.getroot()

        structure_details = {} 

        try:
            structure_details["title"] = tree.find('./PDB').attrib['title']
        except:
            structure_details["title"] = ""

        try:
            structure_details["number_entities"] = int(
                tree.find('./PDB').attrib['nr_entities'])
        except:
            structure_details["number_entities"] = 0

        try:
            structure_details["status"] = tree.find('./PDB').attrib['status']
        except:
            structure_details["status"] = "OBSOLETE"

        try:
            structure_details["pmid"] = tree.find('./PDB').attrib['pubmedId']
        except:
            structure_details["pmid"] = None

        try:
            structure_details["experimental_method"] = tree.find(
                './PDB').attrib['expMethod'].lower()
        except:
            structure_details["experimental_method"] = ""

        try:
            structure_details["resolution"] = tree.find(
                './PDB').attrib['resolution']
        except:
            structure_details["resolution"] = ""

        return structure_details

    # TODO change the name of the method to something related with their behaviour
    # The Uniprot download and the filling of chain_details with uniprot data 
    # shouldm't be in this method, neither this class.
    # TODO This method returns chain details, entity_mapping and protein_info,
    # This could be done in three separate methods.
    def find_chain_details(self, structure_molecule_xml_file, pdb_id, data_path):
        """
        Reads a "structure_molecule_xml" [sic] for the current pdb. 
            :param self: 
            :param structure_molecule_xml_file: the path to the input xml file.
            :param pdb_id: the pdb_id for the current file.
            :param data_path: a folder path where to keep downloaded files
        """
        # TODO correct typo in docstring

        root = elementtree.parse(structure_molecule_xml_file).getroot()

        polymers = list(root.iter('polymer'))
        entity_mapping = self._make_entity_mappings(polymers, pdb_id)

        accessions = [(self._get_accession(polymer), polymer) for polymer in polymers]
        protein_info = {a:self._retrieve_accession_data(a, data_path, pdb_id, p) for a, p in accessions}

        chain_details = self._get_chain_details(pdb_id, polymers)
        chain_details = self._append_start_and_end_to_chain_details(chain_details,
            protein_info, polymers, pdb_id)

        return chain_details, entity_mapping, protein_info
