import os
import sys
import inspect
import Bio

from downloaders.pdbDownloader import pdbDownloader


class StructureDataGetter():
    """
    Retrieves a pdb file, computes dssp and gets pfam data.
    """
    def get(self, data_path, pdb_id, dssp_path, structure_pdb):
        pdbDownloaderObj = pdbDownloader()
        pdbDownloaderObj.options["data_path"] = data_path
        pdbDownloaderObj.options["pdbID"] = pdb_id
        pdbDownloaderObj.options["dssp_path"] = dssp_path
        pdbDownloaderObj.grabPDBStructure()
        structure = Bio.PDB.PDBParser().get_structure(pdb_id, structure_pdb)
        pdbDownloaderObj.grabPfamPDB()
        # pdbDownloaderObj.grabDSSP()
        return structure

