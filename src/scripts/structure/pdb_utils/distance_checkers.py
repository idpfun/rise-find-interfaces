from abc import ABCMeta, abstractmethod


class distanceChecker(object):
    """
    An abstract class to represent objects that trie to estimate the distance
    between two residues and if they are in contact.
    """
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractmethod
    def in_contact(self, res_a, res_b):
        pass

    @abstractmethod
    def distance(self, res_a, res_b):
        pass


class EquivalentAtomsDistanceLessThan(object):
    """
    A class that estimates the distance between two residues as the distance
    between tow equivalent atoms in them. Also can decide if two are in contact
    if the estimated distace is less than a given cutoff value.
    """
    def __init__(self, atom, max_distance):
        self.max_distance = max_distance
        self.atom = atom

    def __residues_have_N(self, res_a, res_b):
        return self.atom in res_a and self.atom in res_b

    def __distance_unsafe(self, res_a, res_b):
        """
        Compute the distance of alpha-amino nitrogen between two residues.
        Does not check if residues actually have alpha-amino nitrogens.
            :param self:
            :param res_a:
            :param res_b:
        """
        return res_a[self.atom] - res_b[self.atom]

    def distance(self, res_a, res_b):
        """
        Compute the distance of alpha-amino nitrogen between two residues.
            :param self:
            :param res_a:
            :param res_b:
        """
        if self.__residues_have_N(res_a, res_b):
            return self.__distance_unsafe(res_a, res_b)
        return None

    def in_contact(self, res_a, res_b):
        """
        Determines if two residues are in contact.
        To be in contact the distance between de nitrogen alpha of each residue
        must be at less than a given cutoff value.
            :param self:
            :param res_a:
            :param res_b:
            :param max_distance:
        """
        return (self.__residues_have_N(res_a, res_b) and
                self.__distance_unsafe(res_a, res_b) < self.max_distance)


class NitrogenAlphaDistanceLessThan(EquivalentAtomsDistanceLessThan):
    """
    Estimates distance between two nitrogen alpha atoms in two residues.
        :param EquivalentAtomsDistanceLessThan:
    """
    def __init__(self, max_distance):
        EquivalentAtomsDistanceLessThan.__init__(self, "N", max_distance)


class CarbonAlphaDistanceLessThan(EquivalentAtomsDistanceLessThan):
    """
    Estimates distance between two carbon alpha atoms in two residues.
        :param EquivalentAtomsDistanceLessThan:
    """
    def __init__(self, max_distance):
        EquivalentAtomsDistanceLessThan.__init__(self, "CA", max_distance)

class CarbonAlphaToAnyDistance(object):
    """
    A class that estimates the distance between the alpha carbon of a residue
    and any random atom from another residue. Also can decide if two are in
    contact if the estimated distace is less than a given cutoff value.
    """
    def __init__(self, max_distance):
        self.max_distance = max_distance

    def __residue_is_CA(self, res_a):
        # pylint: disable=no-self-use
        return "CA" in res_a

    def __distance_unsafe(self, res_a, res_b):
        # pylint: disable=no-self-use
        """
        Compute the distance of alpha-carbon between a residue an a randomly
        chosen atom of another residue (implemented as the first one).
        Does not check if first residue actually have alpha-carbon.
            :param self:
            :param res_a:
            :param res_b:
        """
        for atom in res_b:
            return res_a["CA"] - atom

    def distance(self, res_a, res_b):
        """
        Compute the distance of alpha-carbon between a residue an a randomly
        chosen atom of another residue (implemented as the first one).
            :param self:
            :param res_a:
            :param res_b:
        """
        if self.__residue_is_CA(res_a):
            return self.__distance_unsafe(res_a, res_b)
        return None

    def in_contact(self, res_a, res_b):
        """
        Determines if two residues are in contact.
            :param self:
            :param res_a:
            :param res_b:
        """
        return (self.__residue_is_CA(res_a) and
                self.__distance_unsafe(res_a, res_b) < self.max_distance)

distanceChecker.register(EquivalentAtomsDistanceLessThan)

distanceChecker.register(CarbonAlphaToAnyDistance)
