import Bio.PDB
from structure.iupac.amino_acids import AminoAcids

class PDBSeqBuilder():
    """
    A simple class to build a amino acid sequence from a Bio.PDB.Structure and
    extract data for mapping positions and modifications.
    """
    def build_all_chains(self, bio_pdb_structure, pdb_id):
        """
        Retrieves the sequence of aminoacids for a all chains from a 
        Bio.PDB.Structure.
        Returns a tuple with (construct_sequences, construct_sequences_offsets, 
        construct_pdb_offsets, construct_pdb_ranges, modifications).
            :param self: 
            :param bio_pdb_structure: Bio.PDB.Structure
            :param pdb_id: a four letter pdb code
        """
        modifications = {}
        construct_sequences = {}
        construct_pdb_offsets = {}
        construct_sequences_offsets = {}
        construct_pdb_ranges = {}

        for chain_a in bio_pdb_structure[0].child_list:
            chain_data = self.build_chain(bio_pdb_structure, chain_a.id, pdb_id)
            if (chain_data):
                c_seqs, c_seq_o, c_pdb_o, c_pdb_r, m = chain_data
                chain_a_id = pdb_id + "." + chain_a.id
                construct_sequences[chain_a_id] = c_seqs
                construct_pdb_offsets[chain_a_id] = c_pdb_o
                construct_sequences_offsets[chain_a_id] = c_seq_o
                construct_pdb_ranges[chain_a_id] = c_pdb_r
                modifications[chain_a_id] = m

        return {
            "construct_sequences": construct_sequences,
            "construct_sequences_offsets": construct_sequences_offsets, 
            "construct_pdb_offsets": construct_pdb_offsets,
            "construct_pdb_ranges": construct_pdb_ranges,
            "modifications": modifications }

    
    def build_chain(self, bio_pdb_structure, chain, pdb_id):
        """
        Retrieves the sequence of aminoacids for a single chain from a 
        Bio.PDB.Structure.
        Returns a tuple with (construct_sequences, construct_sequences_offsets, 
        construct_pdb_offsets, construct_pdb_ranges, modifications).
            :param self: 
            :param bio_pdb_structure: a Bio.PDB.Structure
            :param chain: A single char the defines a chain in a PDB structure
            :param pdb_id: a four letter pdb code
        """
        matching_chains = [c for c in bio_pdb_structure[0].child_list if c.id == chain]

        if len(matching_chains) == 1: 
            amino_acids = AminoAcids()
            chain_a = matching_chains[0]

            # Get a list of all valid residues in a chain.
            # Valid residues are those which three-letter-code is in the IUPAC 
            # list.
            # TODO: May be there is bug, if a residue is not in the list of valid
            # amino acid code then is skipped resulting in a list shorter than
            # expected.
            valid_residues = [r for r in chain_a.child_list if r.resname.strip() in amino_acids.three_letter_codes()]

            # Get the position number of each valid residue.
            all_positions = [r.get_id()[1] for r in valid_residues]
            
            # Get a list of all one-letter-codes for valid residues.
            # Some amino acids in IUPAC can have a parent amino acid if it is
            # mofified. 
            # Each residue is asked for its parent, if has one, then the 
            # one-letter-code of the parent is kept (in lower case), if not the 
            # one-letter-code of the amino acid is kept.
            aa_list = [amino_acids.get_parent_one_letter_code(r).lower()
                if amino_acids.has_a_parent(r) else amino_acids[r]["one_letter_code"][0]
                for r in [res.resname.strip() for res in valid_residues]]

            # Build the amino-acid sequence
            construct_sequences = "".join(aa_list)

            # The the modified amino acids (those that have a parent), keep the
            # full name of the modification.
            modifications = {p:{ "modification": amino_acids[r.resname.strip()]["name"], "aa": a} 
                for a, p, r in zip(aa_list, all_positions, valid_residues) if amino_acids.has_a_parent(r.resname.strip())}

            # Build a mapping from pdb position number to sequence offset number
            construct_pdb_offsets = {
                p:{"offset": offset,
                   "residue": aa} for (p, offset, aa)
                in zip(all_positions, range(len(aa_list)), aa_list)}

            # Build a mapping from sequence offset number to pdb position number
            construct_sequences_offsets = {
                offset:{"position": p,
                        "residue": aa} for (p, offset, aa)
                in zip(all_positions, range(len(aa_list)), aa_list)}

            # Keep the ranges of the pdb positions
            construct_pdb_ranges = [f(all_positions) for f in (min, max)]
            return (construct_sequences,
                    construct_sequences_offsets,
                    construct_pdb_offsets,
                    construct_pdb_ranges,
                    modifications)
        else:
            return None
