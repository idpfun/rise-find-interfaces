import json
from pytest import fail
from json.encoder import JSONEncoder
from structure.interfaces import Contact
from structure.dssp.secondary_structure import SecondaryStructureRegion
from structure.dssp.dssp_data import DsspDataPdb
import pprint

class CjsonEncoder(JSONEncoder):
    def default(self, o): # pylint: disable=E0202
        if isinstance(o, Contact):
            return o.to_dict()
        elif isinstance(o, SecondaryStructureRegion):
            return o.toArray()
        elif isinstance(o, DsspDataPdb):
            return o.to_dict()
        else:
            return JSONEncoder.default(self, o)