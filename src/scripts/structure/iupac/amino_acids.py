import json
import os
import sys
import inspect
from singleton import Singleton


# class Singleton(type):
#     _instances = {}
#     def __call__(cls, *args, **kwargs):
#         if cls not in cls._instances:
#             cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
#         return cls._instances[cls]

class AminoAcids():
    """
    Class to manipulate data related to standard and non standard amino acids.
    Each amino acid has:
    - a three letter code,
    - one or more one letter code,
    - a full name
    Many rare amino acids has a parent amino acid, that is a common amino acid that is related.
    For example '4R-FLUORO-N6-ETHANIMIDOYL-L-LYSINE' has a 'Lysine' as parent.
    """
    __metaclass__ = Singleton
    def __init__(self):
        file_path = os.path.dirname(inspect.stack()[0][1])
        data_file = os.path.join(file_path, "components_iupac.reduced.json")
        tmp = json.load(open(data_file, 'r'))
        self.aa = { k.upper(): {
            "one_letter_code": list(tmp[k]["one_letter_code"]), # Some rare amino acids has more 
                                                                # than one one-letter-code
            "name" : tmp[k]["name"], 
            "mon_nstd_parent_comp_id": [x.upper() 
                                        for x in tmp[k].get("mon_nstd_parent_comp_id", "")
                                                       .replace('"',"")
                                                       .replace(" ","")
                                                       .split(",") if x != ""]  
                                                       # Some rare amino acids has more than one 
                                                       # parent
        } for k in tmp}

    def get_parents_names(self, three_letter_code):
        """
        Returns a list with all parent names of an amino acid.
            :param self:
            :param three_letter_code: a three letter uppercase string representing an amino acid.
        """
        return self.aa[three_letter_code]["mon_nstd_parent_comp_id"]

    def get_parents(self, three_letter_code):
        """
        Returns a list with all parents of an amino acid.
        Each amino acid is represented as a dict object.
            :param self:
            :param three_letter_code: a three letter uppercase string representing an amino acid.
        """
        return {n:self.aa[n] for n in self.get_parents_names(three_letter_code) if n in self.aa}

    def get_parent_one_letter_code(self, three_letter_code):
        """
        Return a one-letter code of the parents of a given amino acid.
        If an amino acid has more than one parent, it only returns one of them.
        If the amino acido has no parent, returns its own one letter code.
            :param self:
            :param three_letter_code: a three letter uppercase string representing an amino acid.
        """
        parents = self.get_parents_names(three_letter_code)
        if len(parents) > 0:
            return self.aa[parents[0]]["one_letter_code"][0]
        else:
            return self.aa[three_letter_code]["one_letter_code"][0]

    def has_a_parent(self, three_letter_code):
        """
        Returns True if a given amino acid has a parent.
            :param self: 
            :param three_letter_code: a three letter uppercase string representing an amino acid.
        """
        parent_list = self.aa[three_letter_code]['mon_nstd_parent_comp_id']
        return parent_list and parent_list[0] != '?'
    
    def __contains__(self, three_letter_code):
        """
        Checks if a given string is one the known amino acids.
            :param self: 
            :param three_letter_code: a three letter uppercase string representing an amino acid.
        """
        return three_letter_code.strip() in self.aa

    def __getitem__(self, three_letter_code):
        return self.aa.get(three_letter_code)
    
    def one_letter(self, three_letter_code):
        three_letter_code = three_letter_code.strip().upper()
        if three_letter_code in self.aa:
            return self.aa[three_letter_code]["one_letter_code"][0]

    def items(self):
        return self.aa.items()

    def three_letter_codes(self):
        return self.aa.keys()
        