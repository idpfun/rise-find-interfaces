import itertools

class ModuleAssigner(object):
    #pylint: disable=too-many-instance-attributes
    """
    Assigns a 'module type' to a chain in the context of a collection of
    interacting chains. The 'module type' describes some structural feature of
    the chain. Possible values of module type are:
    - "antigen"
    - "disordered domain"
    - "SLiM"
    - "uncertain"
    - "globular domain"
    - "coiled coil"
        :param object:
    """

    MODULE_TYPE_ANTIGEN = "antigen"
    MODULE_TYPE_DISORDERED_DOMAIN = "disordered domain"
    MODULE_TYPE_SLIM = "SLiM"
    MODULE_TYPE_UNCERTAIN = "uncertain"
    MODULE_TYPE_GLOBULAR = "globular domain"
    MODULE_TYPE_COILED_COIL = "coiled coil"

    def __init__(self):
        self.intramolecular_cutoff = 10
        self.chains_initial_type = {}
        self.coiled_coils = {}
        self.disorder_domain_length = 0
        self.surface_accessibility_cutoff = 0
        self.antigen_presenting = []
        self.antigen_binding = []
        self.coiled_coil_length_cutoff = 0
        self.coiled_coil_proportion_cutoff = 0
        self.dssp = None
        self.helices = []
        self.contacts = None

    def set_intramolecular_cutoff(self, value):
        self.intramolecular_cutoff = value

    def set_disorder_domain_length(self, value):
        self.disorder_domain_length = value

    def set_surface_accessibility_cutoff(self, value):
        self.surface_accessibility_cutoff = value

    def set_antigen_presenting(self, value):
        self.antigen_presenting = value

    def set_antigen_binding(self, value):
        self.antigen_binding = value

    def set_coiled_coil_length_cutoff(self, value):
        self.coiled_coil_length_cutoff = value

    def set_coiled_coil_proportion_cutoff(self, value):
        self.coiled_coil_proportion_cutoff = value

    def set_contacts(self, value):
        self.contacts = value

    def set_helices(self, value):
        self.helices = value

    def set_dssp(self, value):
        self.dssp = value

    def _is_protein(self, polymer_type):
        #pylint: disable=no-self-use
        return polymer_type in ["protein", "protein_D"]

    def _interface_length(self, chain, interfaces):
        #pylint: disable=no-self-use
        interfaces = interfaces if interfaces else []

        chain_contacts = [residue
                          for other_chain in interfaces
                          for residue in interfaces[other_chain].get(chain,{})
                          .get('interactor_residues_pdb', [])]

        return (max(chain_contacts) - min(chain_contacts)
                if chain_contacts else 0)

    def _is_antigen(self, pfams):
        known_antigen_pfams = set(self.antigen_presenting + self.antigen_binding)
        return pfams and len(set(pfams).intersection(known_antigen_pfams)) > 0

    def add_chain_data(self, chain, polymer_type, mean_surf_acc,
                       intramolecular_contacts, interfaces, ss_type, interacting_pfams):
        """
        Adds data for a chain in order to assign a module type to it later with
        'get_types' method.
            :param self:
            :param chain: A chain id of the form "PDB1.A"
            :param polymer_type: A string describing the nature of the macromolecule.
                i.e. "protein", "dna", etc. Extracted from the PDB annotation.
            :param mean_surf_acc: A float with the mean surface accesibility
                calculated with dssp.
            :param intramolecular_contacts: An float with the mean number of
                intramolecular contacts per residue for the chain.
            :param interfaces: A dict containing detailed information of the
                interfaces in which the chain is involved.
            :param ss_type: A string specifying a secondary structure for the chain.
                i.e. "Extended", "Helix", "Multi-partite"
            :param interacting_pfams: a list of pfam ids associated with the chain
        """
        if not self._is_protein(polymer_type):
            self.chains_initial_type[chain] = polymer_type
        else:
            # ASK: there is any case when a chain doesn't have a computed
            # mean_surf_acc value?
            if mean_surf_acc:
                large_acc = mean_surf_acc > self.surface_accessibility_cutoff
                few_contacts = intramolecular_contacts < self.intramolecular_cutoff

                if self._is_antigen(interacting_pfams):
                    self.chains_initial_type[chain] = self.MODULE_TYPE_ANTIGEN

                elif few_contacts and large_acc:
                    long_interface = (self._interface_length(chain, interfaces) >
                                      self.disorder_domain_length)
                    ss_ok = ss_type in ["Multi-partite", "Extended"]
                    self.chains_initial_type[chain] = (
                        self.MODULE_TYPE_DISORDERED_DOMAIN
                        if long_interface and ss_ok else
                        self.MODULE_TYPE_SLIM)

                elif few_contacts and not large_acc:
                    self.chains_initial_type[chain] = self.MODULE_TYPE_UNCERTAIN

                else:
                    self.chains_initial_type[chain] = self.MODULE_TYPE_GLOBULAR

    def is_disordered(self, module_type):
        """
        Returns True if a chain is disordered or False otherwise. The decision is
        made using the module type value.
            :param self:
            :param module_type:
        """
        return module_type in [
            self.MODULE_TYPE_UNCERTAIN,
            self.MODULE_TYPE_DISORDERED_DOMAIN,
            self.MODULE_TYPE_SLIM,
            self.MODULE_TYPE_COILED_COIL]

    def _update_coiled_coils(self, contacts, helices, dssp):
        chains = self.chains_initial_type.keys()
        coiled_coils = {c:{self._get_helix_id(h):{} for h in helices.get(c,[])}
                        for c in chains}

        for chain_a, chain_b in itertools.permutations(chains, 2):
            if (self.is_disordered(self.chains_initial_type[chain_a])
                    and self.is_disordered(self.chains_initial_type[chain_b])):
                cc_data = self._get_coiled_coil(
                    chain_a, chain_b, contacts, helices, dssp)
                for helix in cc_data:
                    coiled_coils[chain_a][helix].update(cc_data[helix])

        self.coiled_coils = coiled_coils

    def get_types(self):
        """
        Retrieve the module type of the chain whose data was added with
        'add_chain_data' method.
            :param self:
        """
        self._update_coiled_coils(self.contacts, self.helices, self.dssp)
        chains = self.chains_initial_type.keys()
        types = self.chains_initial_type
        for chain in chains:
            helix_contacts = []
            if chain in self.coiled_coils:
                helix_contacts = [v for helix in self.coiled_coils[chain]
                                  for v in self.coiled_coils[chain][helix].values()]
                has_many_contacts = len(helix_contacts) > self.coiled_coil_length_cutoff
                helix_proportion = (float(helix_contacts.count("H")) /
                                    (len(helix_contacts) - helix_contacts.count(""))
                                    if helix_contacts else 0)
                helix_proportion_ok = helix_proportion > self.coiled_coil_proportion_cutoff
                if has_many_contacts and helix_proportion_ok:
                    types[chain] = self.MODULE_TYPE_COILED_COIL
        return types

    def _get_helix_id(self, helix):
        # pylint: disable=no-self-use
        return "{}_{}".format(helix.start, helix.end)

    def _get_coiled_coil(self, chain_a, chain_b, contacts, helices, dssp):
        # pylint: disable=too-many-arguments
        coiled_coil = {}
        for ss_a in helices[chain_a]: # pylint: disable=too-many-nested-blocks
            helix_id = self._get_helix_id(ss_a)
            coiled_coil[helix_id] = {}
            if helices[chain_b] and chain_a in contacts and chain_b in contacts[chain_a]:
                contacts = contacts[chain_a][chain_b]
                for contact_a in contacts:
                    if contact_a in ss_a:
                        for contact_b in contacts[contact_a].keys():
                            contact_id = "{}_{}_{}".format(
                                str(contact_a), str(contact_b), chain_b)
                            if contact_b in dssp.chain(chain_b):
                                coiled_coil[helix_id][contact_id] = dssp.chain(chain_b)\
                                    .get_by_name(contact_b, 'SecondaryStructure')
        return coiled_coil
