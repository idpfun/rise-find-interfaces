# Script to start the production server.
# It is intended to be used as a launch script of a docker container.
cp /home/scripts/rise.conf /etc/nginx/sites-available/rise.conf
ln -s /etc/nginx/sites-available/rise.conf /etc/nginx/sites-enabled/
service nginx restart
uwsgi --ini /home/scripts/rise.ini --logto /home/scripts/uwsgi.rise.log

