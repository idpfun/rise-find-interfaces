from os.path import join
import requests
import re

from singleton import Singleton
from withCache import withCache


class QuoteStripper(object):
    __metaclass__ = Singleton
    FIELD_PATTERN = re.compile('^"?(.+?)"*$')
    def strip(self, text):
        return re.match(self.FIELD_PATTERN, text).group(1)

class ElmClassLineParser(object):
    """
    A parser from TSV files from Elm request to ElmClass objects
        :param object: 
    """
    __metaclass__ = Singleton
    NUMBER_OF_FIELDS = 8
    QUOTE_STRIPPER = QuoteStripper()
    FIELDS_PROBABILITY = 5
    FIELDS_N_INSTANCES = 6
    FIELDS_N_INSTANCES_IN_PDB = 7

    def from_text(self, text):
        fields = text.split("\t")
        if len(fields) == self.NUMBER_OF_FIELDS:
            fields = [self.QUOTE_STRIPPER.strip(x) for x in fields]            
            fields[self.FIELDS_PROBABILITY] = float(fields[self.FIELDS_PROBABILITY])
            fields[self.FIELDS_N_INSTANCES] = int(fields[self.FIELDS_N_INSTANCES])
            fields[self.FIELDS_N_INSTANCES_IN_PDB] = int(fields[self.FIELDS_N_INSTANCES_IN_PDB])
            result = ElmClass(*fields)
            return result
        else:
            return None


class ElmInstanceLineParser(object):
    """
    A parser from TSV files from Elm request to ElmInstance objects
        :param object: 
    """
    __metaclass__ = Singleton
    NUMBER_OF_FIELDS = 13
    QUOTE_STRIPPER = QuoteStripper()
    FIELD_ACCESSIONS = 5
    FIELD_START = 6
    FIELD_END = 7
    FIELD_PDB_LIST = 11
    def from_text(self, text):
        fields = text.split("\t")
        if len(fields) == self.NUMBER_OF_FIELDS:
            fields = [self.QUOTE_STRIPPER.strip(x) for x in fields]
            fields[self.FIELD_ACCESSIONS] = fields[self.FIELD_ACCESSIONS].split(" ")
            fields[self.FIELD_START] = int(fields[self.FIELD_START])
            fields[self.FIELD_END] = int(fields[self.FIELD_END])
            fields[self.FIELD_PDB_LIST] = fields[self.FIELD_PDB_LIST].split(" ")
            result = ElmInstance(*fields)
            return result
        else:
            return None


class ElmClass(object):
    """
    Represents an ELM class
        :param object: 
    """
    DOT_STRIPPER_PATTERN = re.compile("^[.]*([^.]*(.+[^.])*)[.]*$")
    def __init__(self, accession, elm_Identifier, site_name, description, regex, probability, 
                 n_instances, n_instances_pdb):
        self.accession = accession
        self.elm_Identifier = elm_Identifier
        self.site_name = site_name
        self.description = description
        self.regex = regex
        self.compiled_regex = re.compile(re.match(self.DOT_STRIPPER_PATTERN, regex).group(1))
        self.probability = probability
        self.n_instances = n_instances
        self.n_instances_pdb = n_instances_pdb

    def matches(self, other_sequence):
        return re.search(self.compiled_regex, other_sequence) is not None

class ElmInstance(object):
    """
    A simple class to represent data from an instance from ELM database.
        :param object: 
    """
    def __init__(self, accession, elm_type, elm_identifier, protein_name, primary_acc, accessions, 
                 start, end, references, methods, logic, pdb, organism):
        self.accession = accession
        self.elm_type = elm_type
        self.elm_identifier = elm_identifier
        self.protein_name = protein_name
        self.primary_acc = primary_acc
        self.accessions = accessions
        self.start = start
        self.end = end
        self.references = references
        self.methods = methods
        self.logic = logic
        self.pdb = pdb
        self.organism = organism
    
    def __str__(self):
        return "<Elm Instance: {} - {} ({}, {}) PDB:{}>".format(self.accession, self.primary_acc,
                                                                self.start, self.end, self.pdb)

    def __repr__(self):
        return str(self)


class ElmStorage(object):
    """
    A cached storage for ELM data
        :param object: 
    """
    __metaclass__ = Singleton
    ELM_BASE_URL = "http://elm.eu.org/"
    ELM_INSTANCES_TSV = "instances.tsv"
    ELM_PBS = "pdbs.tsv"
    ELM_CLASSES = 'elms/elms_index.tsv'
    def __init__(self, cache_folder):
        self._cache_folder = cache_folder

    def _instances_url(self):
        return self.ELM_BASE_URL + self.ELM_INSTANCES_TSV

    def instances_from_pdb(self, pdb_id):
        """
        Retrieves a list of ElmInstance objects for a given pdb id
            :param self: 
            :param pdb_id: 
        """
        url = self._instances_url()
        payload = {'q': pdb_id}
        cache_file = join(self._cache_folder, "{}.elm".format(pdb_id))
        fetcher = ElmQueryFetcher(cache_file, url, payload)
        r = fetcher.make_request()
        lines = [l.strip() for l in  r.split("\n") if not l.startswith("#") and l][1:]
        instances = [ElmInstanceLineParser().from_text(l) for l in lines]
        instances = [i for i in instances if pdb_id in i.pdb]
        return instances
    
    def _all_pdbs_url(self):
        return self.ELM_BASE_URL + self.ELM_PBS

    def all_pdbs(self):
        """
        Retrieves the list of all pdb files with an instance in Elm
            :param self: 
        """
        url = self._all_pdbs_url()
        cache_file = join(self._cache_folder, "pdbs.tsv")
        fetcher = ElmQueryFetcher(cache_file, url, {})
        qs = QuoteStripper().strip
        r = fetcher.make_request()
        lines = [l.strip() for l in  r.split("\n") if not l.startswith("#") and l][1:]
        pdbs = [(qs(pdb), {'elm':qs(elm_acc), 'uniprot': qs(unip)}) 
                for l in lines 
                for f in [l.split("\t")] 
                if len(f) == 3 
                for (pdb, elm_acc, unip) in [f]]
        pdbs_grouped_by_id = {p:[v for p2, v in pdbs if p == p2] for p, _ in pdbs}
        return pdbs_grouped_by_id

    def _elm_classes(self):
        return  self.ELM_BASE_URL + self.ELM_CLASSES

    def classes(self):
        """
        All classes from elm
            :param self: 
        """
        url = self._elm_classes()
        cache_file = join(self._cache_folder, "classes.tsv")
        fetcher = ElmQueryFetcher(cache_file, url, {})
        qs = QuoteStripper().strip
        r = fetcher.make_request()
        lines = [l.strip() for l in  r.split("\n") if not l.startswith("#") and l][1:]
        classes = [ElmClassLineParser().from_text(l) for l in lines]
        return classes


class ElmQueryFetcher(object):
    """
    Auxiliary class to make requests to ELM database.
        :param object: 
    """
    def __init__(self, cache_file, url, payload):
        self.url = url
        self.payload = payload
        self.make_request = withCache(cache_file, "", lambda x: False)(self._make_request)

    def _make_request(self):
        r = requests.get(self.url, params=self.payload)
        r.raise_for_status()
        return r.text
