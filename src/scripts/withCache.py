from functools import wraps
import os

def withCache(cache_file, default_value, avoid_writing_cache,
              write_transform = lambda x : x, read_transform = lambda x : x):
    """
    Decorator class to use for cache with a text file.
        :param cache_file: a path to a text file used as cache
        :param default_value: A value to be returned when there is a problem
            while reading the data.
        :param avoid_writing_cache Is a function that takes the result of the
            decorated function and decides if it should write the value to the 
            cache.
        :write_transform is a function that transform the data given to the cache into a string.
        :write_transform is a function that transform the string in the cache into a the data.
    Examples:
        # Common use:
        @withCache('my_file.txt', None)
        def compute_complucated_stuff():
            ...
        # when the file needs to be computed dynamically in a class:
        class A():
            def __init__(self):
                def self.compute = withCache(cache_file, default_value)(self.__compute)
    """
    def __should_read_from_cache__():
        return not cache_file is None and os.path.exists(cache_file)

    def __should_write_to_cache__():
        return not cache_file is None and not os.path.exists(cache_file)

    def __write_to_cache(data):
        with open(cache_file, 'w') as fh:
            fh.write(write_transform(data).encode('utf-8'))

    def __retrieve_from_cache():
        try:
            return read_transform(open(cache_file, 'r').read())
        except:
            os.remove(cache_file)
            return default_value

    def decorator(func):
        @wraps(func)
        def wrapper(*arg, **kwargs):
            if __should_read_from_cache__():
                return __retrieve_from_cache()
            result = func(*arg, **kwargs)
            if __should_write_to_cache__() and not avoid_writing_cache(result):
                __write_to_cache(result)
            return result
        return wrapper
    return decorator