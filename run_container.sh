# Runs the RISE container detached,
# publish container's port 80.
if [[ -z "${RISE_PATH}" ]]; then
  export RISE_PATH=$(pwd)
fi
docker run -d -i -p 80:80 \
  --env PYTHONPATH=/home/scripts/ \
  -v ${RISE_PATH}/src/scripts/:/home/scripts/ \
  -v ${RISE_PATH}/data/:/home/data/ \
  rise-find-interfaces /bin/bash | cut -c 1-12 > last_container.id
