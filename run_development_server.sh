# Docker launch script for a development server
if [[ -z "${RISE_PATH}" ]]; then
  export RISE_PATH=$(pwd)
fi

# set default port
PORT=80

# required for getopts to correctly parse arguments with 'source'
export OPTIND=1
# Hides some annoying error messages
export OPTERR=0

while getopts "p:h" opt; do
  case $opt in
    p)
    # Set a new port
      re='^[0-9]+$'
      if ! [[ ${OPTARG} =~ $re ]] ; then
        echo "Port must be a number" >&2
        return 1 2> /dev/null || exit 1
      fi
      if [[ "${OPTARG}" -gt 65535 ]] ; then
        echo "Port number too high" >&2
        return 1 2> /dev/null || exit 1
      fi
      PORT=${OPTARG}
      ;;
    h)
    # Help
      echo "Docker launch script for a production server."
      echo "Options:"
      echo "-h : Help"
      echo "-p : Host port, default=80."
      return 0 2> /dev/null || exit 0
      ;;
    \?)
      echo "Option incorrect." >&2
      return 1 2> /dev/null || exit 1
      ;;
  esac
done

docker run -d -p ${PORT}:80 \
  --env PYTHONPATH=/home/scripts/ \
  --env FLASK_APP=apps:create_app \
  --env FLASK_ENV=development \
  -v ${RISE_PATH}/src/scripts/:/home/scripts/ \
  -v ${RISE_PATH}/data/:/home/data/ \
  rise-find-interfaces \
  flask run --port 80 --host 0.0.0.0 | cut -c 1-12 > last_container.id
